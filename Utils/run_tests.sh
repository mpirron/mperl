#! /bin/sh

echo Running tests
for f in tests/*.py; do
    python3 -m unittest $f
done
