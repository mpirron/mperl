#! /bin/sh

export MYPYPATH=$MYPYPATH:../MyPyStubs/

for f in *.py; do
    echo
    echo Type checking $f
    python3 -m mypy $f
done
