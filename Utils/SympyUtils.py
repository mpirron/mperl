
from sympy.core import Add, Mul, Pow, Symbol, Integer, sympify
from sympy.core.relational import Eq, Ne, Lt, Le, Gt, Ge
from sympy.polys.polytools import poly, degree_list
from sympy import And, Or, QQ, cos, sin, solve


# assumes constraints given as a list of 4D vectors
def flattenMatrixTransformation(cstr, dimensions = 4):
    result = []
    for i in range(0, dimensions - 1):
        result.append(cstr[i] / cstr[dimensions-1])
    return result

def replaceTrig(constraints, prefix = "trig", counter = 0, replacements = []):
    constraints = constraints.subs(replacements)
    coss = { c.args[0] for c in constraints.atoms(cos) }
    sins = { c.args[0] for c in constraints.atoms(sin) }
    exprs = coss | sins
    new_constraints = sympify(True)
    for e in exprs:
        var_cos = Symbol(prefix + str(counter))
        replacements.append((cos(e), var_cos))
        counter += 1
        var_sin = Symbol(prefix + str(counter))
        replacements.append((sin(e), var_sin))
        counter += 1
        new_constraints = And(new_constraints, Eq(var_cos**2 + var_sin**2, 1) )
    return (And(constraints.subs(replacements), new_constraints), replacements)

def cleanSymbolNames(constraint, replacements = []):
    constraint = constraint.subs(replacements)
    variables = constraint.free_symbols
    def not_valid(name):
        return '_' in name or '.' in name or '-' in name
    valid_names = { v for v in variables if not not_valid(v.name) }
    invalid_names = { v for v in variables if not_valid(v.name) }
    def sanitize(name):
        return name.replace('_', '').replace('.', '').replace('-', '')
    for v in invalid_names:
        valid = Symbol(sanitize(v.name))
        assert valid not in valid_names # check for capture
        replacements.append( (v, valid) )
    new_constraint = constraint.subs(replacements)
    return (new_constraint, replacements)

def getConjuncts(expr):
    return list(And.make_args(expr))

def getDisjuncts(expr):
    return list(Or.make_args(expr))

def getNumericalSolution(expr):
    def isTrivial(expr):
        poly = expr.args[0]
        dl = list(degree_list(poly))
        return dl == [1]
    def extractPoly(expr):
        poly = expr.args[0]
        assert( expr == Eq(poly, 0) )
        return poly
    def evaluate(contraints, root):
        for c in contraints:
            g = c.args[0].gens
            asNum = c.subs( g[0], root )
            if asNum == sympify(False):
                return False
        return True
    conjs = getConjuncts(expr)
    nonTrivial = [ x for x in conjs if not isTrivial(x) ]
    trivial = [ x for x in conjs if isTrivial(x) ]
    poly = extractPoly(nonTrivial[0])
    roots = solve(poly)
    inDomain = [ x for x in roots if evaluate(trivial, x) ]
    assert(len(inDomain) == 1)
    return inDomain[0]

def variableFunction(actuator, variable):
    assert(variable.name.startswith(actuator.getName()))
    sub = variable.name[len(actuator.getName()):]
    if not sub[0].isalpha():
        sub = sub[1:]
    return sub

def encapsulationCheck(abstract, concrete):
    # XXX should we make a distinction between actuated and passive for the domain and the variables ? ...
    # XXX do we need name mapping on the input? or do we just assume the names are the same?
    # 1. the interface matches: same number of inputs (actuated parameters).
    # 2. the domain of abstract is included in the domain of concrete. XXX comparing the range of motion and the input constraints ...
    # 3. the trajectories are the same over the domain of abstract.
    pass
