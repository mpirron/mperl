import unittest
from SympyUtils import *
from sympy import symbols, And, Or, Ne, Eq, Symbol

class SympyUtilsTests(unittest.TestCase):

    def test_replaceTrig(self):
        alpha = Symbol('a')
        ca = cos(alpha)
        sa = sin(alpha)
        cstrs0 = And(ca >= 0, sa <= 0)
        cstrs1, replacements = replaceTrig(cstrs0)
        self.assertEqual(replacements, [(cos(alpha), Symbol('trig0')),(sin(alpha), Symbol('trig1'))])
        #print(repr(cstrs1))
        cas = ca.subs(replacements)
        sas = sa.subs(replacements)
        self.assertEqual(cstrs1, And(Eq(cas**2 + sas**2, 1), cas >= 0, sas <= 0))

    def test_cleanSymbolNames(self):
        q_a, q_i, q_j, q_k = symbols('part.q_a part.q_i part.q_j part.q_k')
        cstr0 = Eq(q_a**2 + q_i**2 + q_j**2 + q_k**2, 1)
        cstr1, replacements = cleanSymbolNames(cstr0) 
        self.assertEqual(len(replacements), 4)

if __name__ == '__main__':
    unittest.main()
