import unittest
from QepcadInterface import *
from sympy import symbols, And, Or, Ne, Eq, simplify, poly, QQ, sympify

class QepcadInterfaceTests(unittest.TestCase):

    def test_parsing_only(self):
        out0 = "a > 0"
        out1 = "a >= 0"
        out2 = "- a >= 0"
        out3 = "a > 0 /\ b - a > 0"
        out4 = "b^2 >= 0"
        out5 = "b^2 /= 0"
        out6 = "b^2 - a <= 0 \/ b^2 c^2 + b^4 - a^2 b^2 - b^2 + a^2 <= 0"
        out7 = "[ a = 0 ]"
        out8 = "[ b^2 - a <= 0 \/ b^2 c^2 + b^4 - a^2 b^2 - b^2 + a^2 <= 0 ]"
        out9 = "a > 0 /\ b > 0 /\ c - a + 1 >= 0 /\ c + a - 1 <= 0 /\ [ b^2 - a <= 0 \/ b^2 c^2 + b^4 - a^2 b^2 - b^2 + a^2 <= 0 ]"
        qi = QepcadInterface()
        self.assertNotEqual(qi.parseResult(out0), None)
        self.assertNotEqual(qi.parseResult(out1), None)
        self.assertNotEqual(qi.parseResult(out2), None)
        self.assertNotEqual(qi.parseResult(out3), None)
        self.assertNotEqual(qi.parseResult(out4), None)
        self.assertNotEqual(qi.parseResult(out5), None)
        self.assertNotEqual(qi.parseResult(out6), None)
        self.assertNotEqual(qi.parseResult(out7), None)
        self.assertNotEqual(qi.parseResult(out8), None)
        self.assertNotEqual(qi.parseResult(out9), None)

    def test_parsing(self):
        a, b, c = symbols('a b c')
        out0 = "a > 0"
        out1 = "a >= 0"
        out2 = "- a >= 0"
        out3 = "a > 0 /\ b - a > 0"
        out4 = "b^2 >= 0"
        out5 = "b^2 /= 0"
        out6 = "b^2 - a <= 0 \/ b^2 c^2 + b^4 - a^2 b^2 - b^2 + a^2 <= 0"
        out7 = "[ a = 0 ]"
        out8 = "a > 0 /\ b > 0 /\ c >= 0 /\ [ b^2 - a <= 0 \/ b^2 c^2 + b^4 - a^2 b^2 - b^2 + a^2 <= 0 ]"
        qi = QepcadInterface(debug = False)
        self.assertEqual(qi.parseResult(out0), a > 0)
        self.assertEqual(qi.parseResult(out1), a >= 0)
        self.assertEqual(qi.parseResult(out2), -a >= 0)
        self.assertEqual(qi.parseResult(out3), And(poly(a, domain=QQ) > 0, poly(b -a, domain=QQ) > 0))
        self.assertEqual(qi.parseResult(out4), b**2 >= 0)
        self.assertEqual(qi.parseResult(out5), Ne(b**2, 0))
        self.assertEqual(qi.parseResult(out6), Or(poly(b**2 - a, domain=QQ) <= 0, poly(b**2 * c**2 + b**4 - a**2 * b**2 - b**2 + a**2, domain=QQ) <= 0))
        self.assertEqual(qi.parseResult(out7), Eq(a, 0))
        self.assertEqual(qi.parseResult(out8), And(poly(a, domain=QQ) > 0, poly(b, domain=QQ) > 0, poly(c, domain=QQ) >= 0, Or(poly(b**2 - a, domain=QQ) <= 0, poly(b**2 * c**2 + b**4 - a**2 * b**2 - b**2 + a**2, domain=QQ) <= 0)))

    def test_printing(self):
        a, b, c = symbols('a b c')
        printer = QepcadPrinter()
        self.assertEqual(printer.doprint(a > 0), "a > 0")
        self.assertEqual(printer.doprint(Eq(a, 0)), "a = 0")
        self.assertEqual(printer.doprint(Ne(a, 0)), "a /= 0")
        self.assertEqual(printer.doprint(Le(a, 0)), "a <= 0")
        self.assertEqual(printer.doprint(Ge(a, 0)), "a >= 0")
        self.assertEqual(printer.doprint(Lt(a, 0)), "a < 0")
        self.assertEqual(printer.doprint(Gt(a, 0)), "a > 0")
        self.assertEqual(printer.doprint(-a >= 0), "-a >= 0")
        self.assertEqual(printer.doprint(Ne(b**2, 0)), "b^2 /= 0")
        self.assertEqual(printer.doprint(poly(a, domain=QQ) > 0), "a > 0")
        self.assertEqual(printer.doprint(And(poly(a, domain=QQ) > 0, poly(b -a, domain=QQ) > 0)), "a > 0 /\ b - a > 0")
        self.assertEqual(printer.doprint(Or(poly(b**2 - a, domain=QQ) <= 0, poly(b**2 * c**2 + b**4 - a**2 * b**2 - b**2 + a**2, domain=QQ) <= 0)), "b^2 - a <= 0 \/ a^2 + b^4 - b^2 + b^2 c^2 - a^2 b^2 <= 0")
        self.assertEqual(printer.doprint(And(poly(a, domain=QQ) > 0, poly(b, domain=QQ) > 0, poly(c, domain=QQ) >= 0, Or(poly(b**2 - a, domain=QQ) <= 0, poly(b**2 * c**2 + b**4 - a**2 * b**2 - b**2 + a**2, domain=QQ) <= 0))), "c >= 0 /\ a > 0 /\ b > 0 /\ [ b^2 - a <= 0 \/ a^2 + b^4 - b^2 + b^2 c^2 - a^2 b^2 <= 0 ]")

    def test_run(self):
        qi = QepcadInterface()
        assert(qi.isPresent())

    def test_qepcad(self):
        qi = QepcadInterface(debug = False)
        a, b, c = symbols('a b c')
        formula = qi.run([], [a], a >= 0)
        self.assertEqual(formula, sympify(True))

if __name__ == '__main__':
    unittest.main()
