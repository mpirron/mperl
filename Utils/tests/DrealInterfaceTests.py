import unittest
from DrealInterface import *
from sympy import symbols, And, Or, Ne, Eq, simplify, poly, QQ, sympify, cos, Float

class DrealInterfaceTests(unittest.TestCase):

    def test_01(self):
        dr = DrealInterface(debug = False)
        a, b, c = symbols('a b c')
        result, model = dr.run([a >= 0])
        #print(model)
        self.assertEqual(result, True)
    
    def test_02(self):
        dr = DrealInterface(debug = False)
        a, b, c = symbols('a b c')
        result, model = dr.run([a * b >= 0])
        #print(model)
        self.assertEqual(result, True)
    
    def test_03(self):
        dr = DrealInterface(debug = False)
        a, b, c = symbols('a b c')
        result, model = dr.run([a * a < 0])
        self.assertEqual(result, False)
        self.assertEqual(model, None)
    
    def test_04(self):
        dr = DrealInterface(debug = False)
        a, b, c = symbols('a b c')
        result, model = dr.run([a * a + b * b < 0])
        self.assertEqual(result, False)
        self.assertEqual(model, None)

    def test_05(self):
        dr = DrealInterface(debug = False)
        a, b, c = symbols('a b c')
        result, model = dr.run([-a >= 0])
        #print(model)
        self.assertEqual(result, True)

    def test_06(self):
        dr = DrealInterface(debug = False)
        a, b, c = symbols('a b c')
        result, model = dr.run([a - b >= 0])
        #print(model)
        self.assertEqual(result, True)
    
    def test_07(self):
        dr = DrealInterface(debug = False)
        a, b, c = symbols('a b c')
        result, model = dr.run([cos(a) >= 0])
        #print(model)
        self.assertEqual(result, True)

    def test_mdl(self):
        dr = DrealInterface(debug = False)
        a, b, c = symbols('a b c')
        model = {}
        model[a] = (Float(1.0),Float(2.0))
        model[b] = (Float(-1.0),Float(2.0))
        model[c] = (Float(1.0),Float(1.0))
        m2 = dr.approximateModel(model)
        #print(m2)
        self.assertEqual(m2[a], 1.5)
        self.assertEqual(m2[b], 0.5)
        self.assertEqual(m2[c], 1.0)

if __name__ == '__main__':
    unittest.main()
