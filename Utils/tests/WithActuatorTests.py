import unittest

from QepcadInterface import *
from SympyUtils import *

import sys
import os

script_dir = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
#actuator_tree_path = os.path.normpath(os.path.join(script_dir, '../../Graph'))
actuator_tree_path = os.path.normpath(os.path.join(script_dir, '../../'))
sys.path.append(actuator_tree_path)

import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor

import ActuatorGraph.Algorithms as Algorithms

from ActuatorGraph.ActuatorTree import *
import ActuatorGraph.ActuatorGraph as ActuatorGraph

from  math import pi

import numpy as np
import sympy as sp


class WithActuatorTests(unittest.TestCase):

    def test_0(self):
        print( "Running Test0" )
        A0 = Anchor.Anchor( "A0" )
        A1 = Anchor.Anchor( "A1" )
        A0.setCoordinates( [0,0,0] ) # the origin
        P0 = Prismatic.Prismatic( "P0", minLen=0, maxLen=10 )
        tree = ActuatorTree( "union", ActuatorTree( A0 ), ActuatorTree( "union", ActuatorTree( P0 ), ActuatorTree( A1 ) ) )
        matrix = Algorithms.calculateEndPosition( tree )
        print( matrix )
        coord = matrix.multiply(sp.Matrix([0,0,0,1]))
        constraints = flattenMatrixTransformation(coord)
        x,y,z = sp.symbols( "x y z" )
        target = sp.And( Eq( x, 5), Eq(y, 0), Eq(z, 0) )
        _query = sp.And( sp.Eq(constraints[0], x), sp.Eq(constraints[1], y), sp.Eq(constraints[2], z), tree.getConstraints(tree), target )
        qi = QepcadInterface(debug = False)
        query, replacements = cleanSymbolNames(_query)
        print( "query" )
        print( query )
        lenP0 = sp.Symbol("P0.length").subs(replacements)
        formula = qi.run([lenP0], [x, y, z], query)
        expected = sp.Eq(sp.poly(lenP0 - 5, domain = sp.QQ), 0)
        self.assertEqual(formula, expected)


    def test_1(self):
        print( "Running Test1" )
        # Anchor
        A0 = Anchor.Anchor( "A0" )
        A0.setCoordinates( [0,0,0] ) # the origin
        A1 = Anchor.Anchor( "A1" )
        # Joints
        P0 = Prismatic.Prismatic( "P0", minLen=0, maxLen=10 )
        P1 = Prismatic.Prismatic( "P1", minLen=0, maxLen=10 )
        P2 = Prismatic.Prismatic( "P2", minLen=0, maxLen=20 )
        # 1st part
        tree0 = ActuatorTree( "union", ActuatorTree( A0 ), ActuatorTree( "union", ActuatorTree( P0 ), ActuatorTree( "union", ActuatorTree( P1 ), ActuatorTree( A1 ) ) ) )
        coord0 = Algorithms.calculateEndPosition( tree0 ).multiply(sp.Matrix([0,0,0,1]))
        constraints0 = flattenMatrixTransformation(coord0)
        # 2nd part
        tree1 = ActuatorTree( "union", ActuatorTree( A0 ), ActuatorTree( "union", ActuatorTree( P2 ), ActuatorTree( A1 ) ) )
        coord1 = Algorithms.calculateEndPosition( tree1 ).multiply(sp.Matrix([0,0,0,1]))
        constraints1 = flattenMatrixTransformation(coord1)
        # constaints to checkl equivalence
        x0,y0,z0 = sp.symbols( "x0 y0 z0" )
        x1,y1,z1 = sp.symbols( "x1 y1 z1" )
        _query = sp.And( sp.Eq(constraints0[0], x0), sp.Eq(constraints0[1], y0), sp.Eq(constraints0[2], z0), tree0.getConstraints(tree0),
                         sp.Eq(constraints1[0], x1), sp.Eq(constraints1[1], y1), sp.Eq(constraints1[2], z1), tree1.getConstraints(tree1),
                         sp.Eq(sp.Symbol("P2.length"), sp.Symbol("P0.length") + sp.Symbol("P1.length")),
                         sp.Or(sp.Ne(x0, x1), sp.Ne(y0, y1), sp.Ne(z0, z1)) )
        query, replacements = cleanSymbolNames(_query)
        print( "query" )
        print( query )
        lenP0 = sp.Symbol("P0.length").subs(replacements)
        lenP1 = sp.Symbol("P1.length").subs(replacements)
        lenP2 = sp.Symbol("P2.length").subs(replacements)
        # run
        qi = QepcadInterface(debug = False)
        formula = qi.run([], [x0, y0, z0, x1, y1, z1, lenP0, lenP1, lenP2], query)
        expected = sp.sympify(False)
        self.assertEqual(formula, expected)

    def test_2(self):
        print( "Running Test2" )
        # Anchor
        A0 = Anchor.Anchor( "A0" )
        A0.setCoordinates( [0,0,0] ) # the origin
        A1 = Anchor.Anchor( "A1" )
        # Joints
        P0 = Prismatic.Prismatic( "P0", minLen=0, maxLen=10 )
        R0 = Revolute.Revolute( "R0" ) # rotates around Y
        R0.setAngle( thetaX=0, thetaZ=0 )
        # trees
        tree = ActuatorTree( "union", ActuatorTree( A0 ), ActuatorTree( "union", ActuatorTree( R0 ), ActuatorTree( "union", ActuatorTree( P0 ), ActuatorTree( A1 ) ) ) )
        matrix = Algorithms.calculateEndPosition( tree )
        coord = matrix.multiply(sp.Matrix([0,0,0,1]))
        constraints = flattenMatrixTransformation(coord)
        _query0 = sp.And( sp.Eq(constraints[0], 5),
                          sp.Eq(constraints[1], 0),
                          sp.Eq(constraints[2], 5),
                          tree.getConstraints(tree) )
        _query1, replacementsTrig = replaceTrig(_query0)
        query, replacements = cleanSymbolNames(_query1, replacements = replacementsTrig)
        print( "replacement" )
        print( replacements )
        print( "query" )
        print( query )
        lenP0 = sp.Symbol("P0.length").subs(replacements)
        thetaR0 = sp.symbols("R0.pitch").subs(replacements)
        qi = QepcadInterface(debug = False)
        t0 = cos(sp.symbols("R0.pitch")).subs(replacements)
        t1 = sin(sp.symbols("R0.pitch")).subs(replacements)
        formula0 = qi.run([lenP0], [t0, t1], query)
        formula1 = qi.run([t0], [lenP0, t1], query)
        formula2 = qi.run([t1], [lenP0, t0], query)
        print( "result" )
        print( formula0 ) # not as nice as I would like but seems ok
        print( getNumericalSolution(formula0) )
        print( formula1 ) # not as nice as I would like but seems ok
        print( getNumericalSolution(formula1) )
        print( formula2 ) # not as nice as I would like but seems ok
        print( getNumericalSolution(formula2) )

    def test_scara( self ):
        print( "Running test_scara" )
        A00 = Anchor.Anchor( name = "A00" )
        A01 = Anchor.Anchor( name = "A01" )
        A10 = Anchor.Anchor( name = "A10" )
        A11 = Anchor.Anchor( name = "A11" )
        R00 = Revolute.Revolute( name="R00", actuated=True)
        R01 = Revolute.Revolute( name="R01", actuated=True)
        R10 = Revolute.Revolute( name="R10", actuated=True)
        R11 = Revolute.Revolute( name="R11", actuated=True)
        R20 = Revolute.Revolute( name="R20", actuated=False)
        R21 = Revolute.Revolute( name="R21", actuated=False)
        
        P00 = Prismatic.Prismatic( "Bar00", minLen=1, maxLen=1, actuated=False )
        P01 = Prismatic.Prismatic( "Bar01", minLen=1, maxLen=1, actuated=False )
        P10 = Prismatic.Prismatic( "Bar10", minLen=1, maxLen=1, actuated=False )
        P11 = Prismatic.Prismatic( "Bar11", minLen=1, maxLen=1, actuated=False )
        
        arm1 = ActuatorTree( "union", ActuatorTree( A00 ), ActuatorTree( "union", ActuatorTree( R00 ),ActuatorTree( "union", ActuatorTree( P00 ), ActuatorTree( "union", ActuatorTree(R10), ActuatorTree( "union", ActuatorTree( P10 ), ActuatorTree( "union", ActuatorTree( R20 ), ActuatorTree( A10 )))))))
        arm2 = ActuatorTree( "union", ActuatorTree( A01 ), ActuatorTree( "union", ActuatorTree( R01 ),ActuatorTree( "union", ActuatorTree( P01 ), ActuatorTree( "union", ActuatorTree(R11), ActuatorTree( "union", ActuatorTree( P11 ), ActuatorTree( "union", ActuatorTree( R21 ), ActuatorTree( A11 )))))))
        
        scara_arm = ActuatorGraph.ActuatorGraph( name="Scara Arm")
        scara_arm.addNode( arm1 )
        scara_arm.addNode( arm2 )
        
        scara_arm.addPostConstraint( Eq( A00.coordinates.col(-1), A01.coordinates.col(-1) ) )
        scara_arm.addPostConstraint( Eq( A10.coordinates.col(-1), A11.coordinates.col(-1) ) )
        
        print( "Solving inverse kinematics" )
        
        print( "Solving forward kinematics" )
        R00.setAngle( thetaX=0, thetaY=0, thetaZ=0.25*sp.pi)
        R01.setAngle( thetaX=0, thetaY=0, thetaZ=-0.25*sp.pi)
        R10.setAngle( thetaX=0, thetaY=0, thetaZ=-0.25*sp.pi)
        R11.setAngle( thetaX=0, thetaY=0, thetaZ=0.25*sp.pi)
        x,y,z = sp.symbols( "target.x target.y target.z" )
        target_position = sp.Matrix( [x,y,z,1] )        
        _query = scara_arm.solveInverseKinematics( target_position )
        print( "query>", _query )
        qi = QepcadInterface( debug=True )
        query, replacements = cleanSymbolNames( _query )
    
    
    def test_naming(self):
        print( "Running test_naming" )
        P0 = Prismatic.Prismatic( "P0", minLen=0, maxLen=10, actuated=True )
        length = P0.getInputs()[0]
        self.assertEqual(variableFunction(P0, length), "length")

if __name__ == '__main__':
    unittest.main()
