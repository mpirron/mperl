
from typing import List, Dict, Any, cast, Tuple

from arpeggio import Optional, ZeroOrMore, OneOrMore, EOF
from arpeggio import Parser, RegExMatch, ParserPython, NoMatch
from arpeggio import PTNodeVisitor, visit_parse_tree

from subprocess import Popen, PIPE, run, CalledProcessError, TimeoutExpired

from sympy.core import Expr, Add, Mul, Pow, Symbol, Integer, sympify
from sympy.core.compatibility import default_sort_key, string_types
from sympy.core.function import _coeff_isneg
from sympy.core.mul import _keep_coeff
from sympy.core.relational import Eq, Ne, Lt, Le, Gt, Ge
from sympy.polys.polytools import poly
from sympy.printing.precedence import precedence, PRECEDENCE
from sympy.printing.printer import Printer
from sympy import And, Or, QQ


# TODO flatten matrix operation
# TODO replace trig operation by new variables
# TODO substitution for even exp only variables

# < Enter an informal description  between '[' and ']':
# > [ sample ]
# < Enter a variable list:
# > (a,b,c,x,y)
# < Enter the number of free variables:
# > 3
# < Enter a prenex formula:
# > (A x)(A y)[ a > 0 /\ b > 0 /\ [
# > [ b^2 (x - c)^2 + a^2 y^2 - a^2 b^2 = 0] ==>
# >   x^2 + y^2 - 1 <= 0
# > ] ].
# <
# < =======================================================
# <
# < Before Normalization >
# > finish
# < An equivalent quantifier-free formula:
# <
# > a > 0 /\ b > 0 /\ c - a + 1 >= 0 /\ c + a - 1 <= 0 /\ [ 
# > b^2 - a <= 0 \/ b^2 c^2 + b^4 - a^2 b^2 - b^2 + a^2 <= 0 ]
# >
# > =======================================================

class QepcadPrinter(Printer):
    printmethod = "_qepcad"

    def _print_BooleanTrue(self, expr):
        return "TRUE"

    def _print_BooleanFalse(self, expr):
        return "FALSE"

    def _print_Add(self, expr):
        prec = precedence(expr)
        l = []
        for term in expr.args:
            t = self._print(term)
            if t.startswith('-'):
                sign = "-"
                t = t[1:]
            else:
                sign = "+"
            if precedence(term) < prec:
                l.extend([sign, "(%s)" % t])
            else:
                l.extend([sign, t])
        if l[0] == "+":
            l.pop(0)
        return ' '.join(l)

    def _print_Mul(self, expr):
        prec = precedence(expr)
        c, e = expr.as_coeff_Mul()
        if c < 0:
            expr = _keep_coeff(-c, e)
            sign = "-"
        else:
            sign = ""
        args = Mul.make_args(expr)
        return sign + " ".join([ self.parenthesize(arg, prec) for arg in args ])
    
    def _print_And(self, expr):
        prec = precedence(expr)
        return " /\\ ".join([ self.parenthesize(arg, prec) for arg in expr.args ])
    
    def _print_Relational(self, expr):
        charmap = {
            "==": "=",
            "!=": "/=",
        }
        prec = precedence(expr)
        return '%s %s %s' % (self.parenthesize(expr.lhs, prec),
                            charmap.get(expr.rel_op) or expr.rel_op,
                            self.parenthesize(expr.rhs, prec))

    def _print_Or(self, expr):
        prec = precedence(expr)
        return " \\/ ".join([ self.parenthesize(arg, prec) for arg in expr.args ])
    
    def _print_Pow(self, expr):
        prec = precedence(expr)
        return '%s^%s' % (self.parenthesize(expr.base, prec), self.parenthesize(expr.exp, prec))

    def _print_Poly(self, expr):
        e = expr.as_expr()
        return self._print(e)

    def parenthesize(self, item, level):
        # And / Or have the same precedence in QEQPCAD
        prec = precedence(item)
        if (level == PRECEDENCE["And"] and prec == PRECEDENCE["Or"]) or (level == PRECEDENCE["Or"] and prec == PRECEDENCE["And"]):
            return "[ %s ]" % self._print(item)
        elif (prec < level):
            return "( %s )" % self._print(item)
        else:
            return self._print(item)


class QepcadParseTreeVisitor(PTNodeVisitor):

    def visit_ident(self, node, children):
        return Symbol(node.value)

    def visit_integer(self, node, children):
        return Integer(int(node.value))

    def visit_posRatio(self, node, children):
        if len(children) == 3:
            return Rational(children[0], children[2])
        else:
            assert len(children) == 1
            return children[0]

    def visit_ratio(self, node, children):
        if len(children) == 1:
            return children[0]
        else:
            assert children[0] == '-'
            return Integer(-1) * children[1]

    def visit_variable(self, node, children):
        if len(children) == 1:
            return children[0]
        else:
            assert len(children) == 2, "children: " + str(children)
            return children[0] ** children[1]

    def visit_monomial(self, node, children):
        term = children[0]
        for i in range(1, len(children)):
            term *= children[i]
        return term

    def visit_polynomial(self, node, children):
        expr = Integer(0)
        start = 1
        if children[0] == "-":
            expr = Integer(-1) * children[1]
            start = 2
        else:
            expr = children[0]
        for i in range(start, len(children), 2):
            if children[i] == "+":
                expr = expr + children[i+1]
            else:
                assert children[i] == "-"
                expr = expr - children[i+1]
        return poly(expr, domain = QQ)


    def visit_equation(self, node, children):
        if children[1] == "=":
            return Eq(children[0] , children[2])
        elif children[1] == "/=":
            return Ne(children[0] , children[2])
        elif children[1] == ">":
            return Gt(children[0] , children[2])
        elif children[1] == "<":
            return Lt(children[0] , children[2])
        elif children[1] == ">=":
            return Ge(children[0] , children[2])
        else:
            assert children[1] == "<="
            return Le(children[0] , children[2])

    def visit_system(self, node, children):
        if children[0] == "TRUE":
            return sympify(True)
        elif children[0] == "FALSE":
            return sympify(False)
        elif children[0] == "[":
            return children[1]
        elif len(children) == 1:
            return children[0]
        else:
            expr = children[0]
            conjunction = True
            if children[1] == "/\\":
                conjunction = True
            else:
                conjunction = False
            for i in range(2, len(children), 2):
                assert (children[i-1] == "\\/") == (not conjunction)
                if conjunction:
                    expr = And(expr, children[i])
                else:
                    expr = Or(expr, children[i])
            return expr
    
    def visit_top(self, node, children):
        return children[0]


class QepcadInterface:

    def __init__(self, timeout: int = 120, debug: bool = False, memory: int = 100000000, primeList: int = 2000) -> None:
        self.memory = memory
        self.primeList = primeList
        self.timeout = timeout
        self.debug = debug

    def isPresent(self) -> bool:
        try:
            run(['qepcad', '-h'], stdout=PIPE).check_returncode()
            return True
        except:
            return False

    # parsing using https://github.com/igordejanovic/Arpeggio
    def parser(self) -> Parser:
        def ident():      return RegExMatch(r"\w+")
        def integer():    return RegExMatch(r'\d+')
        def posRatio():   return integer, Optional("/", integer)
        def ratio():      return Optional("-"), posRatio
        def variable():   return ident, Optional("^", ratio)
        def monomial():   return OneOrMore([posRatio, variable])
        def polynomial(): return Optional("-"), monomial, ZeroOrMore(["+","-"], monomial)
        def equation():   return polynomial, ["=", "/=", ">=", "<=", ">", "<", ], ratio
        def system():     return ["TRUE", "FALSE", ("[", system, "]"), (equation, ZeroOrMore(["/\\", "\\/"], system))]
        def top():        return system, EOF
        return ParserPython(top, debug = self.debug)

    def parseResult(self, string):
        try:
            p = self.parser()
            tree = p.parse(string)
            return visit_parse_tree(tree, QepcadParseTreeVisitor())
        except NoMatch as e:
            return None

    def readUntil(self, stream, expected):
        line = stream.readline().strip();
        if self.debug:
            print("< " + line)
        while line != expected:
            line = stream.readline().strip();
            if self.debug:
                print("< " + line)
    
    def write(self, stream, string):
        stream.write(string)
        if self.debug:
            print(string, end='')

    # https://docs.python.org/3.6/library/subprocess.html
    def run(self, freeVariables: List[Symbol], boundVariables: List[Symbol], expr: Expr, assumptions: List[Expr] = [], mcCallum: bool = True):
        printer = QepcadPrinter()
        # p = poly(expr) # FIXME not quite true, only within the boolean structure ...
        stringExpr = printer.doprint(expr)

        command = ["qepcad", "-noecho", "+N" + str(self.memory), "+L" + str(self.primeList)]
        proc = Popen(command, stdin=PIPE, stdout=PIPE, stderr=PIPE, universal_newlines=True)

        self.readUntil(proc.stdout, "Enter an informal description  between '[' and ']':")
        self.write(proc.stdin, "[ query ]\n")
        proc.stdin.flush()

        self.readUntil(proc.stdout, "Enter a variable list:")
        strList = [var.name for var in freeVariables + boundVariables]
        self.write( proc.stdin, "(" + (",".join(strList)) + ")\n")
        proc.stdin.flush()

        self.readUntil(proc.stdout, "Enter the number of free variables:")
        self.write(proc.stdin, str(len(freeVariables)) + "\n")
        proc.stdin.flush()

        self.readUntil(proc.stdout, "Enter a prenex formula:")
        for v in boundVariables:
            self.write(proc.stdin, "(E "+ v.name +")")
        self.write(proc.stdin, "[ ")
        self.write(proc.stdin,stringExpr)
        self.write(proc.stdin, " ].\n")
        proc.stdin.flush()

        self.readUntil(proc.stdout, "Before Normalization >")
        
        for a in assumptions:
            stringExpr = printer.doprint(a)
            self.write(proc.stdin, "assume [" + stringExpr +" ].\n")
            proc.stdin.flush()
            self.readUntil(proc.stdout, "Before Normalization >")

        if (not mcCallum) and len(freeVariables) > 3:
            self.write(proc.stdin, "go\n")
            self.write(proc.stdin, "proj-op (m,m," + ",".join("h" for i in range(3, len(freeVariables) + len(boundVariables))) + ")\n")
     
        self.write(proc.stdin, "finish\n")
        proc.stdin.flush()

        try:
            outs, errs = proc.communicate(timeout=self.timeout)
            exit_code = proc.wait()
            if exit_code == 0:
                if self.debug:
                    print("< " + outs)
                lines = outs.split("\n")
                i = 0
                while lines[i].strip() != "An equivalent quantifier-free formula:":
                    i = i + 1;
                i = i + 1;
                while lines[i].strip() == "":
                    i = i + 1;
                return self.parseResult(lines[i])
            else:
                if self.debug:
                    print("< " + errs)
                return None
        except TimeoutExpired:
            proc.kill()
            return None
