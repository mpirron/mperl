from sympy.utilities.autowrap import ufuncify
from functools import reduce
from operator import concat
from scipy.optimize import least_squares
import sympy as sp
import numpy as np
import logging
from Utils.DrealInterface import DrealInterface
from abc import ABC, abstractmethod
from enum import Enum, unique

@unique
class SolverKind(Enum):
    least_squares = 1
    dreal = 2

    def create(self, tolerance = 1e-3, debug = False):
        if self.value == 1:
          return SolverLeastSquare(tolerance, debug)
        elif self.value == 2:
          return SolverDReal(tolerance, debug)
        else:
          raise Exception("solver kind unkown: " + str(self))


class Solver(ABC):
    """an interface that can either be a Least Square optimizer or an SMT solver (dreal)"""

    def __init__(self, tolerance, debug):
        self.tolerance = tolerance
        self.debug = debug
        self.vars = set()
        self.lbs = dict()
        self.ubs = dict()
        # log to the console
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        ch = logging.StreamHandler()
        if debug:
            ch.setLevel(logging.DEBUG)
        else:
            ch.setLevel(logging.INFO)
        formatter = logging.Formatter('[%(name)s] %(levelname)s: %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    def _logMultiline(self, level, msg):
        for l in msg.split('\n'):
            self.logger.log(level, l)

    def addVar(self, variable, lowerBound, upperBound):
        self.vars.update([variable])
        l = self.lbs.get(variable, -sp.oo)
        u = self.ubs.get(variable,  sp.oo)
        i0 = sp.Interval(l, u)
        i1 = sp.Interval(lowerBound, upperBound)
        ib = i0.intersect(i1)
        self.lbs[variable] = sp.N(ib.start) if ib.start != -sp.oo else -np.inf
        self.ubs[variable] = sp.N(ib.start) if ib.start != -sp.oo else -np.inf

    @abstractmethod
    def addLeq(self, lhs, rhs):
        pass

    @abstractmethod
    def addEq(self, lhs, rhs):
        pass

    @abstractmethod
    def solve(self):
        pass


class SolverLeastSquare(Solver):
    
    def __init__(self, tolerance, debug):
        super().__init__(tolerance, debug)
        self.eqs = []
        self.cnt = 0

    def addLeq(self, lhs, rhs):
        slack = sp.Symbol("slack_" + self.cnt)
        self.cnt += 1
        self.addVar(slack, 0, sp.oo)
        self.addEq(lhs, rhs + slack)

    def addEq(self, lhs, rhs):
        self.eqs.append( (lhs,rhs) )

    def _distance(self, l, r):
        #return (l - r)**2
        return sp.sqrt(sum(map(lambda a: (a[0] - a[1]) ** 2, zip(l, r))))
    
    def solve(self):
        #distance
        distance = 0
        for (l,r) in self.eqs:
            distance += self._distance(l, sp.Matrix(r))
        self._logMultiline(logging.DEBUG, "distance:\n" + sp.pretty(distance))
        #get Jacobian
        matrix = sp.Matrix(1, 1, [distance])
        variables = list(self.vars) 
        jacobian = matrix.jacobian( variables )
        self._logMultiline(logging.DEBUG, "jacobian:\n" + sp.pretty(jacobian))
        #create the optimisation problem
        l_distance = sp.lambdify(tuple(variables), distance)
        def f_distance(x):
            return l_distance(*x)
        l_jacobian_separate = [sp.lambdify(tuple(variables), j) for j in jacobian]
        def f_jacobian(x):
            return [f(*x) for f in l_jacobian_separate]
        x0 = np.array([0] * len(variables))
        verb = 0
        if self.debug:
            verb = 2
        lowerBounds = [ self.lbs[v] for v in variables ]
        upperBounds = [ self.ubs[v] for v in variables ]
        res = least_squares(f_distance, x0, f_jacobian, (lowerBounds, upperBounds), verbose = verb)
        if res.status < 0:
            raise Exception("ill-formed problem (a bug somewhere)")
        elif res.cost < tolerance:
            return zip(variables, res.x)
        else:
            raise Exception("could not find a solution")


class SolverDReal(Solver):
    
    def __init__(self, tolerance, debug):
        super().__init__(tolerance, debug)
        self.f = sp.true
    
    def addEq(self, lhs, rhs):
        self.f = sp.And(self.f, sp.Eq(lhs, rhs))
    
    def addLeq(self, lhs, rhs):
        self.f = sp.And(f, lhs <= rhs)
    
    def solve(self):
        solver = DrealInterface( self.tolerance, debug = self.debug )
        constraints = [ v >= self.lbs[v] for v in self.vars ]
        constraints.extend( v >= self.ubs[v] for v in self.vars )
        constraints.append(self.f)
        result, intervals = solver.run(constraints)
        if result:
            return solver.approximateModel(intervals)
        else:
            raise Exception("dreal returned unsat")
