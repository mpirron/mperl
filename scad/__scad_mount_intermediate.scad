module mount_intermediate(){
    difference(){
        size=8;
        cube( [20, 20, 5.25], center=true );
        cylinder( 15, r1=8, r2=8, center=true );
        translate( [10,0,0] )cube( [size, size+0.1, 5.25], center=true );
        for ( i=[0:90:360] )
            rotate( [0,0,i] ) 
                translate( [8,8,0] ) 
                    cylinder( 5.25, r1=1.5, r2=1.5, center=true );
    }
}