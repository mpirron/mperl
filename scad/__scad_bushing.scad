module bushing(){
    difference(){
        size=4;
        cylinder( 2*size+0.4, r1=3.7, r2=3.7 );
        cylinder( 2*size+0.4, r1=2.5, r2=2.5 );
    }
}