module mount_anchor() {
    translate( [-15,-10,-39] ) cube( [5.25, 20, 41.6] );
    translate( [-15,-10,-39] ) cube( [15, 20, 5.25]);

    difference(){
        cube( [20, 20, 5.25], center=true );
        cylinder( 15, r1=8, r2=8, center=true );    
    }
}