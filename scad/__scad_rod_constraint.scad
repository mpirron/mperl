module arc( resolution, inner_radius, outer_radius, thickness, height, arc_start, arc_end  ){
    difference(){
        cylinder( height, r1=outer_radius, r2=outer_radius );
        cylinder( height, r1=inner_radius+0.1, r2=inner_radius+0.1 );
    }
        
    if (thickness < ( outer_radius-inner_radius )){
        for( i=[arc_start:360/resolution:arc_end] )
            rotate( [0,0,i] )
                translate( [0,inner_radius-0.5,0] ) cube( [0.1,thickness, height], center=false );
    }
    else{
        for( i=[arc_start:360/resolution:arc_end] )
            rotate( [0,0,i] )
                translate( [0,inner_radius-0.5,0] ) cube( [0.1,outer_radius-inner_radius, height], center=false );
    }
}

module rod_constraint( stroke, arc_start, arc_end ){
    length = stroke - 4;

    module mount(){
        difference(){
            cylinder( 4, r1=7.5, r2=7.5 );
            cylinder( 4, r1=4, r2=4 );
        }
    }
    
    arc( 360, 5, 7.5, 4, 4, arc_start, arc_end );
    
    translate( [5,-2,4] ) 
        rotate( [0,90,0]) cube( [4,4, length-5] );
    translate( [2+length+2,0,0] )
        mount();
}

//arc( 360, 4.0, 7.5, 4, 4, 0, 270 ); 

//rod_constraint( 20, 90, 270 );