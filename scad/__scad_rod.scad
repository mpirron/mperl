module rod( stroke, actuated ){
    size=8;
    length = stroke - 4;

    module mount( actuated=actuated ){
        difference(){
            cylinder( size, r1=7.5, r2=7.5 );
            if ( actuated )
                cylinder( size, d1=4, d2=4 );
            else
                cylinder( size, r1=4, r2=4 );
        }
    }
    
    mount();
    
    translate( [size-size/5,-size/2,size] ) 
        rotate( [0,90,0]) cube( [size,size, length-size] );
    translate( [2+length+2,0,0] )
        mount( false );
}