module rod_end( stroke ){
    size=8;
    length = stroke - 4;

    module mount(){
        difference(){
            cylinder( size, r1=7.5, r2=7.5 );
            cylinder( size, r1=4, r2=4 );
        }
    }
    
    mount();
    
    translate( [size-size/5,-size/2,size] ) 
        rotate( [0,90,0]) cube( [size,size, length-size] );
    translate( [2+length+2,0,0] ){
        difference(){
            mount();
            translate( [0,-10,0] ) cube( [10,20,size] );
        }
    }
}
