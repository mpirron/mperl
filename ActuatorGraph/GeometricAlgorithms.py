#!/usr/bin/env python


######################################################################
#                                                                    #
# Geometrical algorithms module                                      #
#                                                                    #
# 2017 marcus@pirron.org                                             #
#                                                                    #
# This module contains algorithms for dealing with geometical        #
# problems like convex hull, point-in-polgon &c.                     #
#                                                                    #
######################################################################


import sympy as sp

import math
import stl
from stl import mesh
import numpy

import os
import sys

# Return 1 if positive, -1 if negativ and 0 otherwise.
def sign( x ):
    if (x<0):
        return -1
    elif (x==1):
        return 0
    else:
        return 1


def distanceBetweenPoints( p1, p2 ):
    l_p1 = len( p1 )
    l_p2 = len( p2 )
    assert( l_p1 == l_p2 )
    sum = 0
    for i in range( l_p1 ):
        sum += (p1[i]-p2[i])**2
    return sp.sqrt( sum )


def sameQuadrant( p1, p2 ):
    return sign( p1[0] ) == sign( p2[0] and sign( p1[1] == p2[1]) )


def segmentLine( v1, v2, numberIntermediaryPoints ):
    f = lambda v1, v2, l: ( v1[0]+l*(v2[0]-v1[0]), v1[1]+l*(v2[1]-v1[1]),v1[2]+l*(v2[2]-v1[2]) )
    s =  lambda v1, v2,k:[f( v1, v2, i/k) if k >=1 else v2 for i in range( 0,k+1 )]
    return s( v1, v2, numberIntermediaryPoints )


def position( A, B, p ):
    '''
    Given a line A+lambda*B and a point (X,Y), this function returns -1 if the point
    lies to the left side of the line, 1 to the right and 0 when on the line.
    '''
    Ax, Ay = A
    Bx, By = B
    X,Y = p
    return sign((Bx - Ax) * (Y - Ay) - (By - Ay) * (X - Ax))


def getConvexHull(l, S):
    '''
    Convex hull algorithm: we start at leftmost point and check for
    points on the left - if there are any, we break and check another
    point.
    '''
    pointOnHull = l
    i=0
    P={}
    while True:
        P[i] = pointOnHull
        endpoint = S[0]
        for j in range( len(S) ):
            if endpoint == pointOnHull or position( P[i], endpoint, S[j] ) == -1:
                endpoint = S[j]
        i=i+1
        pointOnHull=endpoint
        if endpoint == P[0]:
            break
    return P


def collinear( A, B ):
    '''Check for collinearity of two points A and B.'''
    Ax, Ay = A
    Bx, By = B
    return Ax/Ay == Bx/By

def leftmostPoint( listOfPoints ):
    '''Get the leftmost point'''
    mx=float( 'inf' )
    my=float( 'inf' )
    for (x,y) in listOfPoints:
        if x<mx:
            mx=x
            my=y
    return (mx, my)


def intersect( A1, A2, B1, B2 ):
    '''
    Given two lines A1A2 and B1B2, returns true for intersection and false for no intersection.
    Note that first order contact counts as no intersection, as well as collinearity.
    '''
    A1x, A1y = A1
    A2x, A2y = A2
    B1x, B1y = B1
    B2x, B2y = B2
    return position( (A1x, A1y), (A2x, A2y), (B1x, B1y)) * position( (A1x, A1y), (A2x, A2y), (B2x, B2y)) <= 0 \
       and position( (B1x, B1y), (B2x, B2y), (A1x, A1y)) * position( (B1x, B1y), (B2x, B2y), (A2x, A2y)) <= 0


def pointInPolygon( point, listOfEdges ):
    '''
    Creates a line from -infty up to point and counts, how often it intersects
    with the edges of the polygon. If the number is odd, the point lies within the
    polygon, if the number is 0 or even, the point lies outside the polygon.
    '''
    startPoint = (-10e10, -10e10 )
    countIntersect = 0
    for i in range( len(listOfEdges)-1 ):
        if intersect( startPoint, point, listOfEdges[i], listOfEdges[i+1]):
            countIntersect += 1
    if intersect( startPoint, point, listOfEdges[ len(listOfEdges)-1 ], listOfEdges[0] ):
            countIntersect += 1
    return countIntersect%2 != 0


def BezierCurve( start, end, numberIntermediaryPoints=10, controlPoint1=None, controlPoint2=None ):
    g = lambda t: (1-t)**3*start+3*(1-t)*t**2*controlPoint1+3*(1-t)*t**2*controlPoint2+t**3*end
    r = []
    for i in range( 0, numberIntermediaryPoints ):
        r.append( g( i/numberIntermediaryPoints ) )
    return r


def getPositionVector( vector1, vector2 ):
    pass


def angleBetweenVectors( vector1, vector2 ):
    vector1_u = vector1 / vector1.norm()
    vector2_u = vector2 / vector2.norm()
    return sp.acos( vector1_u.dot(vector2_u) )


# From https://pypi.python.org/pypi/numpy-stl
def dimensions_from_stl( obj ):
    '''
    Calculate the dimensions of an stl file. Returns a tuple of lengths in units.
    '''
    minx = maxx = miny = maxy = minz = maxz = None
    for p in obj.points:
        # p contains (x, y, z)
        if minx is None:
            minx = p[stl.Dimension.X]
            maxx = p[stl.Dimension.X]
            miny = p[stl.Dimension.Y]
            maxy = p[stl.Dimension.Y]
            minz = p[stl.Dimension.Z]
            maxz = p[stl.Dimension.Z]
        else:
            maxx = max(p[stl.Dimension.X], maxx)
            minx = min(p[stl.Dimension.X], minx)
            maxy = max(p[stl.Dimension.Y], maxy)
            miny = min(p[stl.Dimension.Y], miny)
            maxz = max(p[stl.Dimension.Z], maxz)
            minz = min(p[stl.Dimension.Z], minz)
    length_x = maxx - minx
    length_y = maxy - miny
    length_z = maxz - minz
    return (length_x, length_y, length_z)


