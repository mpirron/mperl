"""
Actuator Graph


"""

import logging
import sympy as sp

from collections.abc import Mapping

import logging

# import ActuatorGraph.Algorithms as Algorithms

from ActuatorGraph.Utils.NameGenerator import NameGenerator
import ActuatorGraph.GraphDecomposition as GraphDecomposition
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes
import ActuatorGraph.Semantics as Semantics


# class ActuatorGraph( Mapping ):
class ActuatorGraph(Mapping):
    def __init__(self, name=NameGenerator.next(" ActuatorGraph"), origin=sp.Matrix([0, 0, 0, 1]), debug=True):
        self._dictionary = {}  # This holds the graph
        self.name = name

        self.linkages = []
        self.nodeRotation = []
        self.nodePosition = 0

        self.preConstraints = None
        self.postConstraints = None

        self.constraints = []
        self.origin = origin

        self.anchors = []
        self.endeffectors = []

        # log to the console
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        ch = logging.StreamHandler()
        if debug:
            ch.setLevel(logging.DEBUG)
        else:
            ch.setLevel(logging.INFO)
        formatter = logging.Formatter('[%(name)s] %(levelname)s: %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    # Use the actuatorgraph like a dict
    # def __init__(self, *args, **kw):
    #    self._dictionary = dict(*args, **kw)

    def __getitem__(self, key):
        return self._dictionary[key]

    def __iter__(self):
        return iter(self._dictionary)

    def __len__(self):
        return len(self._dictionary)

    def __str__(self):

        return str("ActuatorGraph structure, linkages=%s" % (self.__len_linkages__()))

    def __repr__(self):
        return repr(self._dictionary)

    def __len_linkages__(self):
        """Returns number of linkages in the graph"""
        return len(self.linkages)

    # Add an ActuatorTree to the Graph
    def loadDictionary(self, dictionary):
        """Convert a dictionary into an ActuatorGraph structure"""
        self._dictionary = dictionary

    def partitionLinkages(self):
        listOfAnchors = self.anchors
        endEffector = self.endeffectors
        for anchor in listOfAnchors:
            for effector in endEffector:
                for path in GraphDecomposition.find_all_paths(self, anchor, effector):
                    self.linkages.append(path)
                    # self.logger.debug( "Added Path", [ a.name for a in path ] )

    def setAnchors(self, listOfAnchors):
        if type(listOfAnchors) == list:
            self.anchors = listOfAnchors
        else:
            self.anchors = [listOfAnchors]

    def setEndeffectors(self, listofEndeffectors):
        if type(listofEndeffectors) == list:
            self.endeffectors = listofEndeffectors
        else:
            self.endeffectors = [listofEndeffectors]

    # def setRotation( self, node, angle ):
    #    """
    #    Rotate a node around the z-axis relative to the ActuatorGraph. Angle
    #    is in radians
    #    """
    #    pos = self.linkages.index(tree)
    #    self.nodeRotation[ pos ] = angle

    # def getRotation( self, node ):
    #    """
    #    Get the rotation of a node around the z-axis relative to the
    #    ActuatorGraph. Angle is in radians
    #    """
    #    pos = self.linkages.index(tree)
    #    return self.nodeRotation( pos )

    def setOrigin(self, matrix):
        """Set origin in superordinate frame. Use sympy matrix"""
        self.origin = matrix

    def getOrigin(self):
        """
        Returns origin of the ActuatorGraph structure in the superordinate
        frame.
        """
        return self.origin

    def degree(self):
        """Returns number of attached nodes"""
        return len(self.linkages)

    def countActuatorTypes(self):
        """Returns total number of distinct actuator types in the graph structure"""
        Linkage = []
        for linkage in self.linkages:
            result = {}
            for e in linkage:
                if result.has_key(e.getType()):
                    result[e.getType()] += 1
                else:
                    result[e.getType()] = 1
            Linkage.append(result)
        return Linkage

    # The next functions are used in order to iterate over the graph
    # def __iter__( self ):
    #    """ """
    #    return self

    # def next( self ):
    #    """ """
    #    if self.NodePosition == 0:
    #        self.NodePosition += 1
    #        return self._dictionary[ 0]
    #    elif self.NodePosition < len(self._dictionary):
    #        self.NodePosition += 1
    #        return self._dictionary[self.NodePosition - 1]
    #    else:
    #        raise StopIteration()

    def getListOfNodes(self):
        """ """
        return self.linkages

    def __getListOfActuators__(self):
        """Get a list of all actuators which are represented in the tree"""
        k = list(self._dictionary.keys())
        v = list(self._dictionary.values())
        # k = list(self._dictionary.keys())
        # v = list(self._dictionary.values())
        res = k + v
        return list(set(sp.flatten(k + v)))

    def getListOfActuators(self, actuatorType=None, actuated=None, name=None, isEndEffector=None):
        """
        Return specific Actuators
        E.g. getListOfActuators( actuatorType="Revolute", actuated=True )
        """
        l = self.__getListOfActuators__()
        if isEndEffector is not None:
            return [a for a in l if (actuatorType == None or a.getType() == actuatorType) and
                    (actuated == None or a.isActuated() == actuated) and
                    (name == None or a.getName() == name) and (a.endeffector == isEndEffector)]
        else:
            return [a for a in l if (actuatorType == None or a.getType() == actuatorType) and
                    (actuated == None or a.isActuated() == actuated) and
                    (name == None or a.getName() == name)]


    def getScopedListOfActuators( self, actuatorName=None, scope=2):
        result = set()
        f = lambda l, p, i: l[p-i:p+i+1]
        for linkage in self.linkages:
            linkage = linkage.getListOfActuators()
            pos = linkage.index( actuatorName )
            tmp = f( linkage, pos, scope )
            for e in tmp:
                result.add(e)
        return list(result)

    def getRootAnchor(self):
        """Return the initial anchor point or None."""
        l = list(self._dictionary.keys())[0]
        return l if l.type == ActuatorTypes.Anchor else None

    def getLeafAnchor(self):
        """Returns a list of leaf anchors"""
        pass

    # Compile a list of specific variables
    def getListOfVariables(self, actuatorType=None, actuated=None, name=None):
        """ """
        listOfVariables = []
        for linkage in self.linkages:
            listOfVariables.append(linkage.getListOfVariables(actuatorType, actuated, name))
        return listOfVariables

    def getEndEffector(self):
        return self.endeffectors

    def getLinkages(self):
        return self.linkages

    # Constraint handling
    def __checkConstraints__(self):
        for linkage in self.linkages:
            cond = linkage.__checkConstraints__(linkage)
            if cond != True:
                return False
        return True

    def getPreConstraints(self):
        """ """
        constraints = []
        for linkage in self.linkages:
            constraints.append(linkage.getPreConstraints()) if linkage.getPreConstraints() != None else []
        return constraints

    def getPostConstraints(self):
        """ """
        constraints = []
        for linkage in self.linkages:
            constraints.append(linkage.getPostConstraints()) if linkage.getPostConstraints() != None else []
        return constraints

    def getInvariants(self):
        """ """
        constraints = []
        for linkage in self.linkages:
            constraints.append(linkage.getInvariants()) if linkage.getPostConstraints() != None else []
        return constraints

    def getConstraints(self):
        """ """
        constraints = []
        for linkage in self.linkages:
            constraints.append(linkage.getConstraints())
        return self.getPreConstraints() + self.getPostConstraints() + self.getInvariants() + self.constraints + constraints

    def getNodeDegree(self, node):
        return self.getNodeInDegree(node) + self.getNodeOutDegree(node)

    def getNodeInDegree(self, node):
        cnt = 0
        for k, v in self._dictionary:
            for e in v:
                if e == node:
                    cnt = cnt + 1
        return cnt

    def getNodeOutDegree(self, node):
        return len(self._dictionary[node])

    def attachWeight(self, weight):
        '''
        Attach weight to endeffector (i.e. lifting an object)
        Todo: Needs to be adjusted between the individual end effectors.
        :return:
        '''
        for e in self.endeffectors:
            e.weight = weight

    def updateState(self):
        res = []
        for node in self.linkages:
            res.append(node.getInputParameters())
        return res

    def executeState(self):
        pass

    def dumpState( self ):
        res = {}
        p = [ 'weight', 'force', 'torque' ]
        for k,v in self._dictionary.items():
            res[k.name] = k.__getSubstitution__()
            for e in p:
                if hasattr(k, e):
                    res[k.name] = res[k.name] + [(str(k.name)+"_"+str(e), getattr(k, e))]
            for i in v:
                res[i.name] = i.__getSubstitution__()
                for e in p:
                    if hasattr(i, e):
                        res[i.name] = res[i.name] + [(str(i.name)+"_"+str(e), getattr(i, e))]
        return res
