"""
Export ActuatorTrees and ActuatorGraph structure to openscad structures.
"""


import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor

from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes

import ActuatorGraph.Tools as Tools
import ActuatorGraph.ActuatorGraph as ActuatorGraph

import ActuatorGraph

import os


class OpenScad():
    """
    This class creates printable objects from an actuatorgraph structure.
    Known structures:

    rod
    rod_end
    mount_anchor
    mount_intermediate
    bushing
    Anchor
    """

    def __init__( self, tree=None, graph=None ):
        self.level = 0
        self.structures = { '__scad_rod': [ActuatorGraph.Actuators.Revolute.Revolute, ActuatorGraph.Actuators.Prismatic.Prismatic, ActuatorGraph.Actuators.Revolute.Revolute],
                            '__scad_rod_end': [ActuatorGraph.Actuators.Revolute.Revolute, ActuatorGraph.Actuators.Prismatic.Prismatic],
                            '__scad_mount_anchor': [ActuatorGraph.Actuators.Anchor.Anchor],
                            '__scad_mount_intermediate': [ActuatorGraph.Actuators.Prismatic.Prismatic, ActuatorGraph.Actuators.Revolute.Revolute, ActuatorGraph.Actuators.Prismatic.Prismatic],
                            '__scad_bushing': [ActuatorGraph.Actuators.Revolute.Revolute, ActuatorGraph.Actuators.Revolute.Revolute],
                            '__scad_stacked_prismatic': [ActuatorGraph.Actuators.Prismatic.Prismatic, ActuatorGraph.Actuators.Prismatic.Prismatic],
                            '__scad_module_Anchor__': []
        }

        self.modules = list( self.structures.values() )
        self.scad_modules = {}
        self.found_modules = []
        self.found_modules_parameter = []
        self.found_modules_map = {}
        self.tree = tree
        self.graph = graph
        self.path =  "../scad/"



        for dirName, subdirList, fileList in os.walk( self.path ):
            print('Found directory: %s' % dirName)
            for fname in fileList:
                print('\t%s' % fname)
                f = open( self.path+fname, 'r' )
                c = f.readlines()
                src = []
                for l in c:
                    src.append( l )
                self.scad_modules[ fname.split('.')[0]] = ''.join( src )
                f.close()


    def __rolling_window__( self, actuatorlist, length ):
        sublists = []
        for i in range( len( actuatorlist ) - length ):
            params = []
            sl = actuatorlist[i:i+length]
            print( "sl", sl )
            for actuator in sl:
                print( "component:",actuator )
                if actuator.getType() == ActuatorTypes.Prismatic:
                    params.append( [actuator.getLength()])
                elif actuator.getType() == ActuatorTypes.Revolute:
                    params.append( [actuator.minAngle, actuator.maxAngle] )
                else:
                    params.append( [-1] )
            print( "params:", params)
            sublists.append( list(map( type, sl ) ) )
            self.found_modules_parameter.append( params )
            self.found_modules_map[length] = [ sublists, self.found_modules_parameter ]
        return sublists


    def lookup_module( self, structure ):
        return list( self.structures.keys() )[ self.modules.index( structure ) ]


    #todo: call correct modules ..
    def __find_modules_in_tree__( self, listOfActuators ):
        self.src = []
        for i  in range( len( listOfActuators ), 0, -1):
            res = self.__rolling_window__( listOfActuators, i)
            #print( "res", res)
            for e in res:
                try:
                    #print( "->",e )

                    module = self.lookup_module( e )
                    print("module", module)
                    if module in self.scad_modules:
                        print( "found" + self.lookup_module(e))
                        self.found_modules.append( module )
                        self.src.append( self.scad_modules[ self.lookup_module(e)] )
                except:
                    print( "found nothing" )
        self.src = Tools.flatten( self.src)
        return self.src


    def __place_instantiations__( self ):
        print( self.found_modules )
        self.src.append( "\r\n" )
        for module in self.found_modules:
            self.src.append( str(module).replace("__scad_", "")+"();\r\n" )

        for k,v in self.found_modules_map.items():
            print ( k, v )



    def uniqe( self, list ):
        n = []
        for e in list:
            if e not in n:
                n.append(e)
        return n


    def modify_csg_primitive( self, primitive, modification ):
        src = []
        if primitive == "CUBE":
            src.append( "module cube( size=[width, depth, height], center ){ ")
            src.append( modification )
            src.append( '};')
        return src


    def get_scad_from_tree( self ):
        listOfActuators = self.tree.getListOfActuators()
        listOfUniqueActuatorTypes = []
        for actuator in listOfActuators:
            print( "actuatued", actuator.isActuated() )
            t = str(type( actuator )).split(".")[2]
            if t not in listOfUniqueActuatorTypes:
                listOfUniqueActuatorTypes.append( t )
        self.__find_modules_in_tree__( listOfActuators )
        self.__place_instantiations__()
        print( ''.join(Tools.flatten(self.src)))

        return self.src


    def get_tree_from_scad( self, scad ):
        PASS


    def getBoundingBox( self, part ):
        """
        Returns the max. dimensions of the part
        :param part:
        :return:
        """



