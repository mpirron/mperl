"""
Name Generator
"""

from sys import maxsize

class NameGenerator:

    i = 0
    actuators = {} 

    def next( actuatorType=None ):
        if actuatorType == None:
            if i < maxsize:
                x = i
                i += 1
                return "_" + str(x)
            else:
                raise StopIteration()
        else:
            if not actuatorType in NameGenerator.actuators:
               NameGenerator.actuators[ actuatorType ] = 0
            if NameGenerator.actuators[ actuatorType ] < maxsize:
                x = NameGenerator.actuators[ actuatorType ]
                NameGenerator.actuators[ actuatorType ]  += 1
                return str(actuatorType) + "_" + str(x)
            else:
                raise StopIteration()
