'''
This class implements helper functions for use with Newton and Noisy:
https://github.com/phillipstanleymarbell/Noisy-lang-compiler/
'''

import ActuatorGraph.ActuatorGraph as ActuatorGraph
import os
import ActuatorGraph.Actuators.ActuatorTypes as ActuatorTypes

def exportSensorToNoisy( sensor ):
    '''
    Creates a (skeleton) .nt file for use in Noisy
    :param sensor:
    :return:
    '''
    file = sensor.name

    with open( file ) as fh:
        fh.writeln( "include \"NewtonBaseSignals.nt\"" )
        fh.writeln( "k%sErasureToken    :   constant = 16rFEFE" %( str(sensor.type) ) )
        fh.writeln( "k%sStdDev          :   constant = " %( str(sensor.type) ) )
        fh.writeln( "%s                 :   sensor( %s%s: %s ) = " %( str(sensor.type).upper(), str(sensor.type), str(sensor.dimension_output), sensor.dimension_output ) )
        fh.writeln( "\t{")
        fh.writeln( "\trange %s%s == [ %s, %s ], " %(str(sensor.type), str(sensor.dimension_output), str(sensor.dimension_output)) )
        fh.writeln( "\tinterface %s%s@8*bits == spi" %( str(sensor.type), str(sensor.dimension_output)))
        fh.writeln( "\t{")
        fh.writeln( "\t\tx%s%s = read 16r08;" %( str(sensor.type), str(sensor.dimension_output)))
        fh.writeln( "\t}")
        fh.writeln( "\t#erasureToken %s%s = k%sErasureToken" %( str(sensor.type), str(sensor.dimension_output), str(sensor.type)))
        fh.writeln( "}" )

def importSensorFromNoisy( filename ):
    '''
    Generate a sensor from Noisy description
    :param filename:
    :return:
    '''
    pass


#def getParameters( robotic_system ):
#    res = []
#    l = ActuatorGraph.ActuatorGraph.getListOfActuators( robotic_system )
#    for e in l:
#        print( ">", e, e.type is not ActuatorTypes.ActuatorTypes.Anchor )
#        if e.unit is not None or e.type != ActuatorTypes.ActuatorTypes.Anchor:
#
#    return res

def exportNewton( robotic_system, actuator=None, scope=None, filename=None ):
    if scope==None:
        l = robotic_system.getListOfActuators()
    else:
        l = robotic_system.getScopedActuators( actuator, scope )

    params = robotic_system.dumpState()
    path = os.path.dirname(os.path.abspath(__file__))

    with open( filename, 'w' ) as fh:
        fh.write( "# Description: Invariant export from mperl\n\n" )
        fh.write( "# Parameters: \n")
        for e in l:
            params = e.__getParameters__()
            if str(params[0]).find("Anchor") == -1:
                for m in params[1:]:
                    fh.write( "#\t%s\n" %(params[0]))
        fh.write( "\n\n" )
        fh.write( "include \"NewtonBaseSignals.nt\"\n\n")

        with open( path+"/../External/NewtonExtendedSignals.nt") as newton_import_file:
            newton_import = newton_import_file.readlines()
            for line in newton_import:
                fh.write( line )
        fh.write("\n\n")

        fh.write( "%sForPiGroups: invariant(" %(robotic_system.name))
        for i in range(len(l)):
            e = l[i]
            params = e.__getParameters__()
            if str(params[0]).find("Anchor") == -1:
                for m in params[1:]:
                    if m is not None:
                        if i ==  len(l)-1:
                            fh.write("\n\tparam_%s: %s" % (params[0], m))
                        else:
                            fh.write("\n\tparam_%s: %s," % (params[0], m))
        fh.write( ") = \n")
        fh.write("{\n")
        fh.write("}\n\n")

        fh.write("%s : state = { \n" % (robotic_system.name))
        for k, v in robotic_system.dumpState().items():
            for e in v:
                if type(e[1]) is float or type(e[1]) is int:
                    fh.write("\t%s = %s;\n" % (e[0], e[1]))
        fh.write("}\n")



