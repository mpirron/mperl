'''
Functions for evaluating the performance of robotic systems
'''

from functools import wraps
from time import time as timer

def time_function( f ):
    """
    From the python 3 documentation:

    time.perf_counter()

    Return the value (in fractional seconds) of a performance counter,
    i.e. a clock with the highest available resolution to measure a
    short duration. It does include time elapsed during sleep and is
    system-wide.
    (https://docs.python.org/3/library/time.html#time.perf_counter)
    """
    #@wraps( f )
    def wrapped( *args, **kwargs ):
        start=timer()
        print( "args:", *args, "kwargs:", **kwargs )
        res = f( *args,  **kwargs)
        end=timer()
        r = (end - start), res, f.__name__
        print( "Result of timing: ",r )
        print ((end - start), res, f.__name__)
        return f( *args )
    return wrapped


def withinSpec( source, target, tol=1e-10 ):
    """
    Compare two arrays
    :param source: Source array
    :param target: Target array
    :param tol: Tolerance
    :return: True / False
    """
    f = lambda M: list(map( lambda x: abs(x[0]-x[1])<tol, zip(M[0],M[1]))).count( False ) == 0
    return f( source, target )

