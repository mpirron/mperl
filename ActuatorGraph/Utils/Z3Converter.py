"""
Converts string representation of actuator graph representation into z3
expressions.
"""

from __future__ import unicode_literals, print_function
try:
    text=unicode
except:
    text=str

from ActuatorGraph import Algorithms

from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes

from arpeggio import Optional, ZeroOrMore, OneOrMore, EOF, ParserPython, PTNodeVisitor, visit_parse_tree
from arpeggio import RegExMatch as _
import z3
import re

def number():     return _(r'\d*\.\d*|\d+')
def variable():   return _(r'[AnchorPrismaticRevoluteSpherical]*?_\d*_[yawrolpitchenghxz]*')
def factor():     return Optional(["+","-"]), [number, variable, ("(", expression, ")")]
def term():       return factor, ZeroOrMore(["*","/", "^"], factor)
def expression(): return term, ZeroOrMore(["+", "-"], term)
def equation():   return expression, ZeroOrMore([">", "<", "==", "=", "<=", ">="], expression), EOF  # bug: does not match ">=" and "<=" correctly 


class Z3VisitorTree(PTNodeVisitor):
     
    def __init__( self, defaults=True, **kwargs ):
        super(PTNodeVisitor, self).__init__(**kwargs)
        self.for_second_pass = []
        self.defaults = defaults
        self.solver = z3.Tactic( 'qfnra-nlsat').solver()


    def visit_variable( self, node, children ):
        """
        Add variable to assertions
        """
        if self.debug:
            print( "visit_variable node, children:", node, children )
            print( "visit_variable: Add assertion %s >= 0" %( node.value ) )

        self.solver.add( z3.Real(node.value) >= 0 )



        return z3.Real(node.value) 


    def visit_number(self, node, children):
        """
        Converts node value to z3 reals
        """
        if self.debug:
            print("visit_number node, children:", node, children )
            print("visit_number: Converting {}.".format(node.value))
        return z3.RealVal( node.value )


    def visit_factor(self, node, children):
        """
        Applies a sign to the expression or number.
        """
        if self.debug:
            print( "visit_factor node, children:", children )
            print("visit_factor: Factor {}".format(children))
        if len(children) == 1:
            return children[0]
        sign = -1 if children[0] == '-' else 1
        if self.debug:
            print( str(sign * children[-1]) )
            print( "vf", type( sign*children[-1]))
        return sign * children[-1]

    def visit_term(self, node, children):
        """
        Divides or multiplies factors and variables.
        Factor nodes will be already evaluated.
        """
        if self.debug:
            print( "visit_term children:", children )
            print("Term {}".format(children))
       #     term = children[0]
       #     for i in range(2, len(children), 2):
       #         if children[i-1] == "*":
       #             term = term+'*'+children[i]
       #         elif children[i-1] == "^":
       #             term = "("+term+")"+'**'+children[i]
       #         else:
       #             term = term+'/'+children[i]
        
        term = children[0]
        for i in range(2, len(children), 2):
            print( "visit_term children:", children )
            print( "visit_term: debug:", children, list(map( type, children)) )
            if children[i-1] == "*":
                print( type(term), type(children[i]))
                term = term*children[i]
            elif children[i-1] == "^":
                term = term**children[i]
            else:
                term = term/children[i]
        if self.debug:
            print("visit_term: Term = {}".format(term))
        return term

    def visit_expression(self, node, children):
        """
        Adds or substracts terms.
        Term nodes will be already evaluated.
        """
        if self.debug:
            print( "visit_expression children:", node, children )
            print("visit_expression: Expression {}".format(children))
        expr = children[0]
        for i in range(2, len(children), 2):
            if i and children[i - 1] == "-":
                expr = expr-children[i]
            else:
                expr = expr+children[i]
        if self.debug:
            print("visit_expression: Expression = {}".format(expr))
        return expr
    
    def visit_equation( self, node, children ):
        """
        Equals / Unequals expressions.
        Expressins will already be evaluated.
        """
        print( "visit equation: node, children", node, children, type(node), type(children) )
        print( children[0], type(children[0]) )
        print( children[1], type(children[1]) )
        print( children[2], type(children[2]) )

        if children[1] == "=": self.solver.add( children[0] == children[2] ); self.solver.push()
        if children[1] == ">": self.solver.add( children[0] > children[2] ); self.solver.push()
        if children[1] == "<": self.solver.add( children[0] < children[2] ); self.solver.push()
        if children[1] == "==": self.solver.add( children[0] == children[2] ); self.solver.push()
        if children[1] == ">=": self.solver.add( children[0] > children[2] ); self.solver.push()
        if children[1] == "<=": self.solver.add( children[0] < children[2] ); self.solver.push()

        if children[1] == "=": self.equations = [ children[0], "==", children[2]] 
        if children[1] == ">": self.equations = [ children[0], ">", children[2] ] 
        if children[1] == "<": self.equations = [ children[0], "<", children[2] ] 
        
        return self.solver


def substituteTrigonometrics( equation ):
    """
    Bhaskara-Aryabhata Approximation
    Does not filter recurrent matches ..
    """
    #rules = ["sin", "cos", "**" ]
    #for rule in rules:
    #    if equation.find( rule ) != -1:
    #        equation = re.sub( r'sin\((.*?)\)', r'4*(\1*180/3.1415926535)*(180-(\1*180/3.1415926535))/(40500-(\1*180/3.1415926535)*(180-(\1*180/3.1415926535)))', equation )
    #        equation = re.sub( r'cos\((.*?)\)', r'4*((\1-3.1415926535/2)*180/3.1415926535)*(180-((\1-3.1415926535/2)*180/3.1415926535))/(40500-((\1-3.1415926535/2)*180/3.1415926535)*(180-((\1-3.1415926535/2)*180/3.1415926535)))', equation )
    #        equation = re.sub( r'\*\*', r'^', equation ) # ** vs. 
    #        print( "subs:",equation )
    equation = re.sub( r'sin\((.*?)\)', r'4*(\1*180/3.1415926535)*(180-(\1*180/3.1415926535))/(40500-(\1*180/3.1415926535)*(180-(\1*180/3.1415926535)))', equation )
    equation = re.sub( r'cos\((.*?)\)', r'4*((\1-3.1415926535/2)*180/3.1415926535)*(180-((\1-3.1415926535/2)*180/3.1415926535))/(40500-((\1-3.1415926535/2)*180/3.1415926535)*(180-((\1-3.1415926535/2)*180/3.1415926535)))', equation )
    equation = re.sub( r'\*\*', r'^', equation ) # ** vs.
    print( "subs:",equation )
    return equation


def getSolverFromExpression( input_equation, debug=True ):
    """
    Given an equation / expression, return a z3 Solver for it.
    """
    parser = ParserPython(equation, debug=debug)
    parse_tree = parser.parse(input_equation)
    return visit_parse_tree(parse_tree, Z3VisitorTree(debug=debug))


def getSolverFromTree( tree, targetPosition, tol=0.1, debug=True ):
    """
    Derive kinematic equations from a tree structure and return a z3 solver
    instance. Currently, makes a separate solver for each coordinate and returns
    the union of them.
    """
    Algorithms.calculateEndPosition( tree)
    expressionTree = tree.endeffectors[-1].getGlobalPositionMatrix().col(-1)[0:3]
    equation = []
    for expression in zip( expressionTree, targetPosition ):
        print( "expression:", expression )
        equation.append( [str(expression[0]),str(expression[1])] )
    print( equation )
    if ( tol > 0 ):
        # Allow for some tolerances
        eq = []
        eq.append( equation[0][0]+"<"+equation[0][1]+"+"+str(tol) )
        eq.append( equation[0][0]+">"+equation[0][1]+"-"+str(tol) )
        eq.append( equation[1][0]+"<"+equation[1][1]+"+"+str(tol) )
        eq.append( equation[1][0]+">"+equation[1][1]+"-"+str(tol) )
        print( "SMT: equations", eq)
    else:
        eq = []
        eq.append( equation[0][0]+"="+equation[0][1] )
        eq.append( equation[1][0]+"="+equation[1][1] )
        print( "SMT: equations", eq)

    # Add constraints per actuator
    for actuator in tree.getListOfActuators():
        substitutions = actuator.__getSubstitution__()

        for p, s in substitutions:
            if s != None:
                eq.append( str(p) + "==" +  str(s) )
        
    # Substitute Trigonometrics    
    for i in range( len( eq ) ) :
        eq[i] = substituteTrigonometrics( eq[i] )
        print(" substituted %s" %( eq[i] ) )
    print( "SMT: equations", eq)
    
    s = []
    for e in eq:
        print( "solver from", e )
        s.append( getSolverFromExpression(substituteTrigonometrics(e)) )
    
    s = unifySolver( s )
    
    return s

def unifySolver( listOfSolvers ):
    """
    Returns a z3 solver which satisfies all assertions from all solvers given in
    listofSolvers.
    """
    s = z3.Solver()
    for solver in listOfSolvers:
        for assertions in solver.assertions():
            s.add( assertions ); s.push()
    return s


