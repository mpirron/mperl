'''
Sample mechanism for testing, without hardware interaction
'''

import ActuatorGraph.GeometricAlgorithms as GeometricAlgorithms
import ActuatorGraph.SMT as SMT
import ActuatorGraph.LeastSquare as LS
import sympy as sp
#from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from ActuatorGraph.Mechanisms.Scara import single_scara as single_scara
#from ActuatorGraph.Mechanisms.scara_arm import single_scara_spherical as single_scara



rs, listOfActuators = single_scara( constraint=True )

start = sp.Matrix( [0,0,0] )
end   = sp.Matrix( [sp.sqrt(2), sp.sqrt(2), 0] )



points =  GeometricAlgorithms.BezierCurve( start=start,
                                        end = end,
                                        numberIntermediaryPoints=15,
                                        controlPoint1 = sp.Matrix( [0,0.3,0]),
                                        controlPoint2 = sp.Matrix( [1.0,1.5,0])
                                        )

for p in points:
    print( "Navigating to point %s" %(p))
    try:
        res = SMT.solveInverseKinematics(rs, sp.N(p), 0.001, "dreal")
        for x, v in res.items():
            print(x, " = ", v)
    except:
        print( "Could not navigte to point %s" % p )
    print("------")

res = SMT.solveInverseKinematics(rs, [sp.sqrt(2)/2, sp.sqrt(2)/2,0], 0.001, "dreal")
for x, v in res.items():
    print(x, " = ", v)





#x = []
#y = []
#z = []
#fig = plt.figure()
#for p in points:
#    x.append( p[0] )
#    y.append( p[1] )
#    z.append( p[2] )
#    plt.scatter( x[-1], y[-1] )
#plt.show()



