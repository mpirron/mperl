"""
This class provides basic json support:
 * Im- and Export structures
 * Im- and Export values
"""

import json
import sympy as sp

import ActuatorGraph.ActuatorGraph as ActuatorGraph
import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Spherical as Spherical
import ActuatorGraph.Actuators.Anchor as Anchor
import ActuatorGraph.Actuators.Beam as Beam

import ActuatorGraph.Algorithms as Algorithms

def __prepareStructureForExport__( actuatorgraph ):
    '''Convert Actuatorgraph structures into json-understandable parts'''
    types = {}
    convertedstructure = {}
    constraints = {}

    for k,v in actuatorgraph._dictionary.items():
        key = k.__repr__().split(":")   # type:name:constraintlist
        values = [ e.__repr__().split(":")[1] for e in v ]

        constraints[ key[1] ] = ':'.join( key[2] )
        convertedstructure[key[1]] = values

    for actuators in actuatorgraph.getListOfActuators():
        types[ actuators.name ] = str(actuators.type)

    return convertedstructure, types, constraints


def __prepareStructureForImport__( importedstructure, types, constraints=None ):
    """Convert the string dictionaries to the appropriate actuatorgraph structures"""
    structure = {}
    for k, v in importedstructure.items():
        print( "importv=",v)
        actuator = __createActuator__( types[k] )
        children = []

        children = [ __createActuator__(types[e] ) for e in v ]
        print("Add %s to %s" %( children, actuator))
        structure[actuator]= children
    return structure


def __createActuator__( actuatortype ):
    """Given an actuatortype, return an actuator"""
    actuator = None
    print( "Convert:", actuatortype )
    if actuatortype == "Anchor": actuator = Anchor.Anchor( name = "Generated_Anchor" )
    if actuatortype == "Revolute": actuator = Revolute.Revolute( name = "Generated_Revolute" )
    if actuatortype == "Prismatic": actuator = Prismatic.Prismatic( name = "Generated_Prismatic" )
    if actuatortype == "Spherical": actuator = Spherical.Spherical( name = "Generated_Spherical" )
    if actuatortype == "Beam": actuator = Beam.Beam( name = "Generated_Beam" )
    assert( actuator != None )
    return actuator


def exportStructure( actuatorgraph, filename ):
    '''Export only the actuatorgraph structure into a json file'''
    convertedstructure, types, _ =     __prepareStructureForExport__( actuatorgraph )
    with open(filename+"_structure", "w") as fh:
        json.dump( convertedstructure, fh )
    with open(filename+"_types", "w") as fh:
        json.dump( types, fh )
    return True


def exportAll( actuatorgraph, filename ):
    '''Export the actuatorgraph structure including constraints into a json file'''
    convertedstructure, types, constraints =     __prepareStructureForExport__( actuatorgraph )
    with open(filename, "w") as fh:
        json.dump( convertedstructure, fh )
        json.dump( types, fh )
        json.dump( constraints, fh )
        return True
    return False


def exportEquations( actuatorgraph, filename ):
    """Export equations, values and constraints of an actuatorgraph structure"""
    Algorithms.calculateEndPosition( actuatorgraph )
    equations = {}
    constraints = {}
    parameters = {}

    listofkeys = [k for k in actuatorgraph._dictionary]
    listofvalues = [v for v in actuatorgraph._dictionary.values()]

    for e in set( sp.flatten(listofkeys+listofvalues) ):
        equations[e.name] =  str(e.globalpositionmatrix)
        constraints[e.name] = str(e.constraints)
        parameters[e.name] = str(e.__getSymbols__())

    with open(filename+"_equations", "w") as fh:
        json.dump( equations, fh )

    with open(filename+"_constraints", "w") as fh:
        json.dump( constraints, fh )

    with open(filename+"_symbols", "w") as fh:
        json.dump( parameters, fh )


    return True



def importStructure( filename ):
    with open(filename+"_structure", "r") as fh:
        importedstructure = json.load( fh )
    with open(filename+"_types", "r") as fh:
        types = json.load( fh )
    convertedstructure = __prepareStructureForImport__(importedstructure, types )

    actuatorgraph = ActuatorGraph.ActuatorGraph( "Imported Structure" )
    actuatorgraph.loadDictionary( convertedstructure )
    return actuatorgraph


def importAll( actuatorgraph ):
    pass


