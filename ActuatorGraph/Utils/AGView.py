#!/usr/bin/env python3

import tkinter

from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes

import ActuatorGraph.Tools as Tools

from sympy import pi, cos, sin

def coordinate_sys( canvas, width, height, line_distance ):
    """ Create a coordinate system """
    # vertical lines at an interval of "line_distance" pixel
    for x in range(line_distance,width,line_distance):
        canvas.create_line(x, 0, x, height, fill="#000000")
    # horizontal lines at an interval of "line_distance" pixel
    for y in range(line_distance,height,line_distance):
        canvas.create_line(0, y, width, y, fill="#000000")


def place_revolute( canvas, origin, x,y ):
    """ Revolute Actuator """
    size = 20
    canvas.create_oval( origin[0]+x-size/2, origin[1]-y-size/2, origin[0]+x+size/2, origin[1]-y+size/2, fill="green" )


def place_prismatic( canvas, origin, x, y, length, rotation=0 ):
    """ Prismatic Actuator """
    end_x = int( cos( rotation )* length )
    end_y = int( sin( rotation )* length )
    canvas.create_line( origin[0]+x, origin[1]-y, origin[0]+x+end_x, origin[1]-y-end_y, width=4 )

def place_anchor( canvas, origin, x, y):
    size=20
    canvas.create_rectangle( origin[0]+x-size/2, origin[1]-y-size/2, origin[0]+x+size/2, origin[1]-y+size/2, fill="green" )


def plot( tree=None):
    
    root = tkinter.Tk()

    origin = (0, 480 )

    canvas_width = 800
    canvas_height = 480

    unit = 60

    menu = tkinter.Canvas( root, width=canvas_width, height=canvas_height )
    menu.configure( background="black")
    menu.create_text( 20, 10, anchor="w", fill="white", text="Actuators in use:" )
    menu.create_line( 20, 20, canvas_width-20, 20, fill="white" )
    menu.grid( row=0, column=0 )

    i=1
    for actuator in tree.getListOfActuators():
        menu.create_text( 20, 40+i, fill="white", anchor="w", text="%s" %( actuator ) )
        i+=15

    w = tkinter.Canvas( root, width=canvas_width, height=canvas_height )
    w.configure( background="#f2f5c0" )
    w.grid( row=1, column=0 )

    coordinate_sys( w, width=canvas_width, height=canvas_height, line_distance=unit)

    for actuator in tree.getListOfActuators():
        if actuator.getType() == ActuatorTypes.Anchor: place_anchor( w, origin, actuator.getCoordinates()[0], actuator.getCoordinates()[1] )
        if actuator.getType() == ActuatorTypes.Revolute: place_revolute( w, origin, actuator.getCoordinates()[0], actuator.getCoordinates()[1] )
        if actuator.getType() == ActuatorTypes.Prismatic:
            prev = Tools.getPredecessor( tree, actuator )
            if ( prev != None ) and (prev.getType() == ActuatorTypes.Revolute ):
                place_prismatic( w, origin, actuator.getCoordinates()[0], actuator.getCoordinates()[1], actuator.getLength()*unit, prev.getAngle() )
            else:
                place_prismatic( w, origin, actuator.getCoordinates()[0], actuator.getCoordinates()[1], actuator.getLength()*unit )


    root.mainloop()

if __name__ == "__main__":
    plot()
