# Lookup table
from time import time
from enum import Enum, auto
import ActuatorGraph.Units.__si_units as si_units
from random import random

class lookup_table_entries( Enum ):
    '''
    Entries of the lookup table.
        Parameter, Value and Time(stamp)
        Cost: How much it cost to renew the measurement (complexity, power - abstract, comparable cost)
        Trace: Set of equations, if it is a derived cost
        Power: Power consumed when active (e.g. polling a sensor, or actuating a motor)
        assumed_value: Is the value backed by a sensor, or, in the case of a motor, is the value assigned but never
            verified? In closed loop system, this should always be False (Open loop: Always true)
        trigger: Which (if any) action shoulkd be triggered if a field changes - e.g. actuating a motor, or
            redoing some other calculations

    '''
    parameter       = "Parameter"
    value           = "Measurement"
    time            = "Creation time"
    cost            = "Derivation cost"
    trace           = "Trace"
    plain_value     = "Plain value"
    unit            = "Unit"
    power           = "Power used"
    assumed_value   = "Assumed Value"
    trigger         = "Action trigger"
    node            = "Attached node"


    def __str__( self ):
        return self.value


class lookup_table_traces( Enum ):
    equation    = "Equation"
    sensor      = "Sensor"
    datasheet   = "Datasheet"
    system      = "Polled automatically from system"


class lookup_table_special_fields( Enum ):
    value       = "value"
    write_back  = "write_back"

def current_time():
    return str(hex(int(time())))


class lt_exceptions_robotic_system_overwrite( Exception ):
    def __str__(self):
        return "Robotic system overwrite error"


class lt_exceptions_robotic_system_missing( Exception ):
    def __str__(self):
        return "Robotic system not attached"

class lt_exceptions_entry_missing( Exception ):
    def __str__(self):
        return "Entry missing from table"


class lookup_table():
    '''
    self.trigger is triggered each time a value in the table is updated
    '''
    def __init__(self):
        self.table = {}
        self.backup_table = {}
        self.robotic_system = None
        self.trigger = []

    def __backup_entry__( self, parameter ):
        if parameter in self.table:
            entry = self.table[ parameter ]
            curr_time = entry[lookup_table_entries.time]
            if curr_time in self.backup_table:
                self.backup_table[curr_time].append( entry )
            else:
                self.backup_table[ curr_time ] = [entry]
            self.table[ parameter ] = entry
            return True
        else:
            return False

    def __trigger__( self ):
        if self.trigger is not []:
            for e in self.trigger:
                e()


    def add_trigger( self, parameter=None, trigger=None, special=None ):
        if parameter == None:
            self.trigger.append( trigger )
        else:
            if special == lookup_table_special_fields.value:
                def f():
                    return trigger( self.table[parameter][lookup_table_entries.plain_value])
                self.table[parameter][lookup_table_entries.trigger].append( f )
            else:
                print( "yay")
                print( parameter, trigger, special )
                self.table[parameter][lookup_table_entries.trigger].append( trigger )

    def add_entry( self, parameter, entry ):
        '''
        Add or update entry in lookup table. Also creates a trace of old values.
        :param parameter: p
        :param entry: (m(p), t(p), c(p), trace)
        :return:
        '''
        curr_time =  entry[ lookup_table_entries.time ]
        #if parameter in self.table:
        #    if parameter in self.backup_table:
        #        self.backup_table[ curr_time ] = self.table[ parameter ]
        if curr_time in self.backup_table:
            self.backup_table[curr_time].append( entry )
        else:
            self.backup_table[ curr_time ] = [entry]
        self.table[ str(parameter) ] = entry
        for e in self.table[str(parameter)][lookup_table_entries.trigger]:
            e()
        self.__trigger__()


    def modify_entry( self, parameter, value ):
        if parameter in self.table:
            self.__backup_entry__( parameter )
            self.table[str(parameter)][lookup_table_entries.time] = current_time()
            self.table[str(parameter)][lookup_table_entries.plain_value] = value
            self.table[str(parameter)][lookup_table_entries.value].value = value
            for e in self.table[parameter][lookup_table_entries.trigger]:
                e()
            self.__trigger__()
            return True
        else:
            pass
            #raise( lt_exceptions_entry_missing )


    def show_entry( self, name ):
        if self.__has_parameter__( name ):
            v = self.table[str(name)]
            parameter = v[lookup_table_entries.parameter]
            value = v[lookup_table_entries.plain_value]
            unit = v[lookup_table_entries.unit]
            time = v[lookup_table_entries.time]
            cost = v[lookup_table_entries.cost]
            trace = v[lookup_table_entries.trace]
            print("%20s:  %15s \t %10s \t %10s \t %5s \t\t %5s" % (parameter, unit, value, time, cost, trace))
        else:
            print( "No entry found for %s" % name)

    def attach_robotic_system( self, robotic_system ):
        if self.robotic_system is None:
            self.robotic_system = robotic_system
            self.poll_robotic_system()
            self.__trigger__()
        else:
            raise lt_exceptions_robotic_system_overwrite()

    def __create_entry__( self, actuator, timestamp ):
        res = []
        if hasattr( actuator, "__getSubstitution__" ):
            subst = actuator.__getSubstitution__()
            for e in subst:
                measurement = None
                if actuator.unit is None:
                    measurement = si_units.dimensionless        # there's a bug in the si_unit ..
                    measurement.value = e[1]                    # workaround: plain value
                else:
                    measurement = actuator.unit
                    measurement.value = e[1]
                entry = {
                    lookup_table_entries.parameter  : str(e[0]),
                    lookup_table_entries.value      : measurement,
                    lookup_table_entries.cost       : 1,
                    lookup_table_entries.time       : timestamp,
                    lookup_table_entries.trace      : lookup_table_traces.system,
                    lookup_table_entries.plain_value: e[1],
                    lookup_table_entries.unit       : None,  # bug in si_units
                    lookup_table_entries.trigger    : [],
                    lookup_table_entries.node       : actuator

                }
                res.append( entry )
            return res
        else:
            return {}

    def __update_entry__( self, actuator, timestamp ):
        res = {}
        if hasattr( actuator, "__getSubstitution__" ):
            subst = actuator.__getSubstitution__()
            for e in subst:
                entry = {
                    lookup_table_entries.parameter  : str(e[0]),
                    lookup_table_entries.value      : None,
                    lookup_table_entries.cost       : 1,
                    lookup_table_entries.time       : timestamp,
                    lookup_table_entries.plain_value: e[1],
                }
                res[str(e[0])] = entry
            return res
        else:
            return {}

    def poll_robotic_system( self ):
        if self.robotic_system is None:
            raise lt_exceptions_robotic_system_missing()
        curr_time = current_time()
        if self.table == {}:
            for k,v in self.robotic_system._dictionary.items():
                entry = self.__create_entry__( k, curr_time )
                for e in entry:
                    parameter = e[ lookup_table_entries.parameter ]
                    self.add_entry( parameter , e )
                for i in v:
                    entry = self.__create_entry__( i, curr_time )
                    for el in entry:
                        parameter = el[lookup_table_entries.parameter]
                        self.add_entry(parameter, el)
                self.__trigger__()
        else:   # update table and backup old values

            for k,v in self.robotic_system._dictionary.items():
                entry = self.__update_entry__( k, curr_time )
                for e in entry:
                    parameter = e
                    self.table[parameter][lookup_table_entries.plain_value] = entry[parameter][lookup_table_entries.plain_value]
                    self.table[parameter][lookup_table_entries.value] = entry[parameter][lookup_table_entries.value]
                    self.table[parameter][lookup_table_entries.cost] = entry[parameter][lookup_table_entries.cost]
                    self.table[parameter][lookup_table_entries.time] = entry[parameter][lookup_table_entries.time]
                    #self.add_entry( parameter , e )
                for i in v:
                    entry = self.__update_entry__( i, curr_time )
                for e in entry:
                    parameter = e
                    self.table[parameter][lookup_table_entries.plain_value] = entry[parameter][lookup_table_entries.plain_value]
                    self.table[parameter][lookup_table_entries.value] = entry[parameter][lookup_table_entries.value]
                    self.table[parameter][lookup_table_entries.cost] = entry[parameter][lookup_table_entries.cost]
                    self.table[parameter][lookup_table_entries.time] = entry[parameter][lookup_table_entries.time]
                self.__trigger__()


            for k, v in self.table.items():
                for e in v[lookup_table_entries.trigger]:
                    e()
            self.__trigger__()

    def dump_current_frame( self) :
        """
        Dump current state to stdout
        :return:
        """
        print("%20s %35s %20s %25s %5s %30s %70s" % ("Parameter", "Unit", "Value", "Time", "Cost", "Trace", "Trigger"))
        for k,v in self.table.items():
            parameter   = v[ lookup_table_entries.parameter ]
            value       = v[ lookup_table_entries.plain_value ]
            unit        = v[ lookup_table_entries.unit ]
            time        = v[ lookup_table_entries.time ]
            cost        = v[ lookup_table_entries.cost ]
            trace       = v[ lookup_table_entries.trace ]
            trigger     = v[ lookup_table_entries.trigger ]
            print( "%20s %35s %25s %20s %5s %30s %70s" %( parameter, unit, value, time, cost, trace, trigger ) )
        if self.trigger == []:
            print( "Table has no triggers attached" )
        else:
            print( "Table has triggers attached:", end="")
            for e in self.trigger:
                print( e, end=" ")