#!/usr/bin/env python3
from arpeggio import Optional, ZeroOrMore, OneOrMore, EOF, ParserPython, PTNodeVisitor, visit_parse_tree
from arpeggio import RegExMatch as _

import sympy as sp

import ActuatorGraph.Actuators.Revolute as Revolute


def number():           return _(r'\d*\.\d*|\d+')
def coordinate():       return _(r'\(\d,\d,\d\)')
def robotic_system():   return _(r'[a-zA-Zr0-9_]*[_0-9]' )
def primitive():        return ( ["Anchor", "Prismatic", "Spherical", "Planar", "Revolute"] )
def action():           return ( ["Move", "Actuate" ] )
def properties():       return ( ["Configuration_Matrix", "Position_Matrix", "State_Matrix", "Get_Constraints", "Get_Parameters", "Preconstraint", "Postconstraint", "Invariant"] )


def expression():       return OneOrMore( [properties, action] ), ZeroOrMore( [primitive, robotic_system, coordinate, number] ), EOF


class ActuatorGraphVisitor( PTNodeVisitor ):
     
    def __init__( self, ActuatorGraph=None, defaults=True, **kwargs ):
        super(PTNodeVisitor, self).__init__(**kwargs)
        self.for_second_pass = []
        self.defaults = defaults
        self.debug = False
        self.ActuatorGraph = ActuatorGraph

    def visit_number(self, node, children):
        print( "<<\t >> visit number: node, children", node, children, type(node), type(children) )
        return float(node.value)

    def visit_coordinate( self, node, children ):
        print( "<<\t >> visit coordinate: node, children", node, children, type(node), type(children) )
        values = node.value.strip( "(" )
        values = values.strip( ")" )
        values = values.split( "," )
        return sp.Matrix( [values[0],values[1], values[2]] )

    def visit_robotic_system( self, node, children ):
        '''
        Match the primitive or the robotic system to an already known component or create a new one,
        :param node:
        :param children:
        :return:
        '''
        print( "<<\t >> visit robotic system: node, children", node, children, type(node), type(children) )
        return node.value

    def visit_primitive( self, node, children ):
        if self.ActuatorGraph == None:
            if node.value == "Anchor": pass
            if node.value == "Prismatic": pass
            if node.value == "Spherical": pass
            if node.value == "Planar": pass
            if node.value == "Revolute": return Revolute.Revolute()

        print( "<<\t >> visit primitive: node, children", node, children, type(node), type(children) )
        return node.value

    def visit_action( self, node, children ):
        '''
        Action solves the inverser kinematics (Move) or forward kinematics (Actuate)
        :param node:
        :param children:
        :return:
        '''
        print( "<<\t >> visit action: node, children", node, children, type(node), type(children) )
        if node.value == "Actuate": pass # Forward Kinematic
            # get list of tuples [( actuator, value), .. ]
        if node.value == "Move": pass # Inverse Kinematic
        return node

    def visit_properties(self, node, children ):
        '''
        Get some information about the system at hand
        :param node:
        :param children:
        :return:
        '''
        print( "<<\t >> visit property: node, children", node, children, type(node), type(children) )
        print( ">>>>>>>>>>>>>>>>>>>>>>>", node.value )
        if node.value == "Configuration_Matrix": print( "><>>>><<<>>>", node.value )
        if node.value == "Position_Matrix": pass
        if node.value == "State_Matrix": pass
        if node.value == "Get_Constraints": pass
        if node.value == "Get_Parameters": pass
        if node.value == "Preconstraint": pass
        if node.value == "Potconstraint": pass
        if node.value == "Invariant": pass
        return node

    def visit_expression( self, node, children ):
        print( "\t >> visit expression: node, children", node, children, type(node), type(children) )
        return node


def parseExpression( input_expression, ActuatorGraph=None, debug=True ):
    '''
    Parse the expression, and if a graph exists, try to match it. If not, create a new object and perform operation
    on that one.
    :param input_expression:
    :param ActuatorGraph:
    :param debug:
    :return:
    '''
    parser = ParserPython(expression, debug=debug)
    parse_tree = parser.parse(input_expression)
    return visit_parse_tree(parse_tree, ActuatorGraphVisitor(ActuatorGraph=ActuatorGraph, debug=debug))

