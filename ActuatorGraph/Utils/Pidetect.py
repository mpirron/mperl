"""
Check if we are running on a Raspberry Pi.
"""
import re


def detect_platform():
    """
    # Check /proc/cpuinfo, "Hardware = BCM<id>" where id=
    # 2708 : Pi 1
    # 2709 : Pi 2
    # 2835 : Pi 3
    """
    with open( '/proc/cpuinfo') as f:
        if re.findall(  r"BCM\d.*", line ) != []:
            return "pi"
        else:
            return "unknown"

def runs_on_pi():
    return "pi" == detect_platform()
