'''
Sample mechanism for testing, without hardware interaction
'''

import ActuatorGraph.GeometricAlgorithms as GeometricAlgorithms
import ActuatorGraph.SMT as SMT
import ActuatorGraph.LeastSquare as LS
import sympy as sp
#from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from ActuatorGraph.Mechanisms.Scara import single_scara as single_scara
import ActuatorGraph.Utils.LookupTable as Lookup_table
from ActuatorGraph.Algorithms import calculateEndPosition



rs, listOfActuators = single_scara( constraint=True )

table = Lookup_table.lookup_table()
table.attach_robotic_system( rs )

start = sp.Matrix( [0,0,0] )
end   = sp.Matrix( [sp.sqrt(2), sp.sqrt(2), 0] )



points =  GeometricAlgorithms.BezierCurve( start=start,
                                        end = end,
                                        numberIntermediaryPoints=15,
                                        controlPoint1 = sp.Matrix( [0,0.3,0]),
                                        controlPoint2 = sp.Matrix( [1.0,1.5,0])
                                        )


table.add_trigger( parameter="Angle_Ellbow_yaw", trigger=listOfActuators[2].setAngle, special=Lookup_table.lookup_table_special_fields.value )
table.add_trigger( parameter="Angle_Shoulder_yaw", trigger=listOfActuators[5].setAngle, special=Lookup_table.lookup_table_special_fields.value )
table.add_trigger( parameter="Angle_Endeffector_yaw", trigger=listOfActuators[3].setAngle, special=Lookup_table.lookup_table_special_fields.value )


for p in points:
    print( "Navigating to point %s" %(p))

    try:
        res = SMT.solveInverseKinematics(rs, sp.N(p), 0.001, "dreal")
        for x, v in res.items():
            print(x, " = ", v)
            # mapping back
            try:
                table.modify_entry( str(x), v)
            except:
                print( "entry %s could not be updated" %(x))

        print( "Current State of the System:")
        table.poll_robotic_system()
        table.dump_current_frame()
    except:
        print( "Coudl not move to %s" %(p) )

    rs, listOfActuators = single_scara( constraint=True )

