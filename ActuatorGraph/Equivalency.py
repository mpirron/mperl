""" 
Compare actuators, trees and graphs with each other.               
                                                                    
We differentiate between the following notions, from strongest     
precedence to weakest:
    1, Identity: Same internal structure, same constraints
    2, Equality: Different internal structure, same constraints
    3, Refinement (Substitution): Different internal structure, different
       constraints
"""                                                                    


import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor

from ActuatorGraph.ActuatorTree import *


# Check if the constraints of actuatorTree1 are a subset of the constraints 
# of actuatorTree2 in the sense that the single constraints itself are exactly
# the same.
def __constraintExactSubset__( actuatorTree1, actuatorTree2 ):
    PASS

# Same structure, different constraints?
def __constraintSubset__ ( actuatorTree1, actuatorTree2 ):
    actuatorTree1.__resetIterator__()
    actuatorTree2.__resetIterator__()

    for a1, a2 in zip( actuatorTree1, actuatorTree2 ):
        if a1.getType() != a2.getType():
            return False
        



# Oder actuatorTree1==actuatorTree2(?)
def isIdenticalTree( actuatorTree1, actuatorTree2 ):
    actuatorTree1.__resetIterator__()
    actuatorTree2.__resetIterator__()
    
    if actuatorTree1.getNumberOfActuators() != actuatorTree2.getNumberOfActuators():
        return False

    for a1, a2 in zip( actuatorTree1, actuatorTree2 ):
        if a1!=a2:
            return False
    return True


# Notion of Equality
def isEqualTree( actuatorTree1, actuatorTree2 ):
    actuatorTree1.__resetIterator__()
    actuatorTree2.__resetIterator__()
    
    if actuatorTree1.getNumberOfActuators() != actuatorTree2.getNumberOfActuators():
        return False

    for a1, a2 in zip( actuatorTree1, actuatorTree2 ):
        if a1.getType() != a2.getType():
            return False
    
    constraintsa1 = a1.getConstraints()
    constraintsa2 = a2.getConstraints()

    if constraintsa1 != constraintsa2:
        return False

    return True
 


# Notion of Equivalence
# Not correct atm ..
def isEquivalentTree( actuatorTree1, actuatorTree2 ):
    PASS



# Return the strictest relationship between two objects
def compare( a1, a2 ):
    PASS
