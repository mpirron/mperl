"""
Tools for analyzing and checking the semantics of ActuatorTree structures.
"""

import ActuatorGraph.Actuators.ActuatorTypes as ActuatorTypes
import ActuatorGraph.GraphDecomposition as GraphDecomposition


#
# Defining the exceptions
#

class ActuatorSemanticsException( Exception ):
    """ 
    Base class for exceptions occurring when handling ActuatorTree or
    ActuatorGraph structures.
    """
    pass


class OpenTreeError( ActuatorSemanticsException ):
    """Valid trees must have either none or two anchor points. """
    def __init__ ( self, graph ):
        self.tree = graph
    def __str__( self ):
        return repr( "Incorrect number of anchor points" )

class UnconnectedModule( ActuatorSemanticsException):
    """Modules must start and end with actuators or with anchor points, but not both"""
    def __init__ ( self, graph ):
        self.tree = graph
    def __str__( self ):
        return repr( "Unconnected Module" )


class AnchorNotAllowedHere( ActuatorSemanticsException ):
    """First or last Anchor node has pre- or successors"""
    def __init__ ( self, graph ):
        self.tree = graph
    def __str__( self ):
        return repr( "Anchor at invalid position" )


class UnknownActuator( ActuatorSemanticsException ):
    """ 
    Unknown type of actuator used. Check ActuatorGraph.ActuatorTypes for
    list of valid types.
    """
    def __init__ ( self, actuatorType ):
        self.actuatorType = actuatorType

    def __str__( self ):
        return repr( "Unknown Actuatortype %s" %( self.actuatorType ) )


class IncompleteVirtualJoint( ActuatorSemanticsException ):
    """Virtual joints must start and end with an actuator and have at least one sensor"""
    def __init__ ( self, graph ):
        self.tree = graph
    def __str__( self ):
        return repr( "Incorrect Virtual Joint" )


class IncorrectDOF( ActuatorSemanticsException ):
    """The module has not the correct degrees of freedom"""
    def __init__ ( self, graph, actual_dof, needed_dof ):
        self.tree = graph
    def __str__( self ):
        return repr( "Incorrect degrees of freedom fpr module" )


class UnconnectedActuatorGraph( ActuatorSemanticsException ):
    """The ActuatorGraph has unconnected components"""
    def __init__ ( self, graph ):
        self.tree = graph
    def __str__( self ):
        return repr( "ActuatorGraph has unconnected components" )


class NotSerialActuatorGraph( ActuatorSemanticsException ):
    """The ActuatorGraph has is not serial"""
    def __init__ ( self, graph ):
        self.tree = graph
    def __str__( self ):
        return repr( "ActuatorGraph is not serial" )


class NotParallelActuatorGraph( ActuatorSemanticsException ):
    """The ActuatorGraph has is not parallel"""
    def __init__ ( self, graph ):
        self.tree = graph
    def __str__( self ):
        return repr( "ActuatorGraph is not parallel" )

#
# Starting the checks
#

def assertValidActuator( actuator ):
    """
    Checks if the actuator is valid
    :param actuator: Actuator to check
    :return:
    """
    if actuator.getType() in ActuatorTypes.ActuatorTypes.__members__:
        return True
    else:
        raise UnknownActuator



def assertValidGraph( actuatorgraph ):
    pass


def assertConnected( actuatorgraph ):
    """
    Checks if an ActuatorGraph structure is connected
    :param actuatorgraph:
    :return:
    """
    graph = actuatorgraph._dictionary

    unconnected_nodes = []
    for node in graph:
        if not graph[node]:
            unconnected_nodes += node
    if unconnected_nodes is not []:
        raise UnconnectedActuatorGraph
    else:
        return True


def assertParallel( actuatorgraph ):
    paths = GraphDecomposition.find_all_paths( actuatorgraph )
    if len(paths) > 1:
        return True
    else:
        raise NotParallelActuatorGraph


def assertSerial( actuatorgraph ):
    paths = GraphDecomposition.find_all_paths( actuatorgraph )
    if len(paths) != 1:
        return True
    else:
        raise NotParallelActuatorGraph


