import ActuatorGraph.Actuators.Revolute_new as Revolute
import ActuatorGraph.Actuators.Spherical_new as Spherical
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor

import ActuatorGraph.Algorithms as Algorithms

import ActuatorGraph.Tools as Tools
import ActuatorGraph.ActuatorGraph as ActuatorGraph

import sympy as sp


def delta():
    A00 = Anchor.Anchor( name="Anchor0" )
    P00 = Prismatic.Prismatic( name="Shoulder0", actuated=False )
    R00 = Revolute.Revolute( name="Angle_Shoulder0", actuated=True )
    R10 = Revolute.Revolute( name="Angle_Endeffector0", actuated=False )
    P10 = Prismatic.Prismatic( name="Ellbow0", actuated=False )
    S00 = Spherical.Spherical( name="Spherical_0_Endeffector")

    P01 = Prismatic.Prismatic( name="Shoulder1", actuated=False )
    R01 = Revolute.Revolute( name="Angle_Shoulder1", actuated=True )
    R11 = Revolute.Revolute( name="Angle_Endeffector1", actuated=False )
    P11 = Prismatic.Prismatic( name="Ellbow1", actuated=False )
    S01 = Spherical.Spherical( name="Spherical_1_Endeffector")

    P02 = Prismatic.Prismatic( name="Shoulder1", actuated=False )
    R02 = Revolute.Revolute( name="Angle_Shoulder1", actuated=True )
    R12 = Revolute.Revolute( name="Angle_Endeffector1", actuated=False )
    P12 = Prismatic.Prismatic( name="Ellbow1", actuated=False )
    S02 = Spherical.Spherical( name="Spherical_1_Endeffector")

    E0 = Anchor.Anchor( name="Effector0" )

    offset0 = Revolute.Revolute( name="Offset0")
    offset1 = Revolute.Revolute( name="Offset1")
    offset2 = Revolute.Revolute( name="Offset2")



    structure_delta = {A00: [offset0, offset1, offset2],
                       offset0: [R00],
                        R00: [P00],
                        P00: [R10],
                        R10: [P10],
                        P10: [S00],
                        S00: [E0],

                        offset1: [R01],
                        R01: [P01],
                        P01: [R11],
                        R11: [P11],
                        P11: [S01],
                        S01: [E0],

                        offset2: [R02],
                        R02: [P02],
                        P02: [R12],
                        R12: [P12],
                        P12: [S02],
                        S02: [E0],
                       }

    R00.constraintRange( -sp.pi, sp.pi)
    R10.constraintRange( -sp.pi, sp.pi)
    S00.constraintRange( -sp.pi, sp.pi)
    R01.constraintRange( -sp.pi, sp.pi)
    R11.constraintRange( -sp.pi, sp.pi)
    S01.constraintRange( -sp.pi, sp.pi)
    R02.constraintRange( -sp.pi, sp.pi)
    R12.constraintRange( -sp.pi, sp.pi)
    S02.constraintRange( -sp.pi, sp.pi)

    delta = ActuatorGraph.ActuatorGraph( name="delta" )
    delta.loadDictionary( structure_delta )
    delta.setAnchors([A00])
    delta.setEndeffectors([E0])
    delta.partitionLinkages()
    for linkage in delta.linkages:
        Tools.initChain( linkage )

    Algorithms.calculateEndPosition( delta )
    print( "Initial end position E0:", E0.globalpositionmatrix)
    #print( "Initial end position S00:", S00.globalpositionmatrix)
    #print( "Initial end position S01:", S01.globalpositionmatrix)
    #print( "Initial end position S02:", S02.globalpositionmatrix)

    return delta
