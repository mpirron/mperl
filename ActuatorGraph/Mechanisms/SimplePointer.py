import ActuatorGraph.Actuators.Spherical_new as Spherical
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor

import ActuatorGraph.Algorithms as Algorithms

import ActuatorGraph.Tools as Tools
import ActuatorGraph.ActuatorGraph as ActuatorGraph

import sympy as sp


def simple_pointer_spherical():
    A00 = Anchor.Anchor( name="Anchor" )
    S00 = Spherical.Spherical( name="Base_joint")
    P00 = Prismatic.Prismatic( name="rod", actuated=False )

    structure_single = {A00: [S00],
                        S00: [P00]
                        }


    S00.constraintRange( -sp.pi, sp.pi)

    simple_pointer = ActuatorGraph.ActuatorGraph( name="simple_pointer" )
    simple_pointer.loadDictionary( structure_single )
    simple_pointer.setAnchors([A00])
    simple_pointer.setEndeffectors([P00])
    simple_pointer.partitionLinkages()
    for linkage in simple_pointer.linkages:
        Tools.initChain( linkage )

    Algorithms.calculateEndPosition( simple_pointer )
    print( "Initial end position:", P00.globalpositionmatrix)

    S00.setAngle(None)

    return simple_pointer


