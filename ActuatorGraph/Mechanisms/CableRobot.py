import ActuatorGraph.Actuators.Revolute_new as Revolute
import ActuatorGraph.Actuators.Spherical_new as Spherical
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor

import ActuatorGraph.Algorithms as Algorithms

import ActuatorGraph.Tools as Tools
import ActuatorGraph.ActuatorGraph as ActuatorGraph

import sympy as sp


def cable_robot():
    A00 = Anchor.Anchor( name="Anchor0" )

    offset0 = Revolute.Revolute( name="Offset0")
    offset1 = Revolute.Revolute( name="Offset1")
    offset2 = Revolute.Revolute( name="Offset2")
    offset3 = Revolute.Revolute( name="Offset3")


    P00 = Prismatic.Prismatic( name="Cable_0", actuated=False )
    S00 = Spherical.Spherical( name="Upper_Joint_0")
    S10 = Spherical.Spherical( name="Lower_Joint_0")

    P01 = Prismatic.Prismatic( name="Cable_1", actuated=False )
    S01 = Spherical.Spherical( name="Upper_Joint_1")
    S11 = Spherical.Spherical( name="Lower_Joint_1")

    P02 = Prismatic.Prismatic( name="Cable_2", actuated=False )
    S02 = Spherical.Spherical( name="Upper_Joint_2")
    S12 = Spherical.Spherical( name="Lower_Joint_2")

    P03 = Prismatic.Prismatic( name="Cable_3", actuated=False )
    S03 = Spherical.Spherical( name="Upper_Joint_3")
    S13 = Spherical.Spherical( name="Lower_Joint_3")

    E0 = Anchor.Anchor( name="Effector0" )




    structure_cable_robot = {A00: [offset0, offset1, offset2, offset3],
                       offset0: [S00],
                       S00: [P00],
                       P00: [S10],
                       S10: [E0],

                       offset1: [S01],
                       S01: [P01],
                       P01: [S11],
                       S11: [E0],

                       offset2: [S02],
                       S02: [P02],
                       P02: [S12],
                       S12: [E0],

                       offset3: [S03],
                       S03: [P03],
                       P03: [S13],
                       S13: [E0],
                       }


    cable_robot = ActuatorGraph.ActuatorGraph( name="cable_robot" )
    cable_robot.loadDictionary( structure_cable_robot )
    cable_robot.setAnchors([A00])
    cable_robot.setEndeffectors([E0])
    cable_robot.partitionLinkages()
    for linkage in cable_robot.linkages:
        Tools.initChain( linkage )

    Algorithms.calculateEndPosition( cable_robot )
    print( "Initial end position:", E0.globalpositionmatrix)
    #print( "Initial end position S10:", S10.globalpositionmatrix)
    #print( "Initial end position S11:", S11.globalpositionmatrix)
    #print( "Initial end position S12:", S12.globalpositionmatrix)
    #print( "Initial end position S13:", S13.globalpositionmatrix)

    return cable_robot
