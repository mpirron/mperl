import ActuatorGraph.Actuators.Revolute_new as Revolute
import ActuatorGraph.Actuators.Spherical_new as Spherical
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor
import ActuatorGraph.Sensors.Flex as Flex
import ActuatorGraph.Algorithms as Algorithms

import ActuatorGraph.Tools as Tools
import ActuatorGraph.ActuatorGraph as ActuatorGraph

import sympy as sp


def single_scara( posed = False, constraint=False ):
    A00 = Anchor.Anchor( name="Anchor0" )
    P00 = Prismatic.Prismatic( name="Shoulder", actuated=False )
    R00 = Revolute.Revolute( name="Angle_Shoulder", actuated=True )
    R20 = Revolute.Revolute( name="Angle_Endeffector", actuated=False )
    P10 = Prismatic.Prismatic( name="Ellbow", actuated=False )
    R10 = Revolute.Revolute( name="Angle_Ellbow", actuated=True )
    E0  = Anchor.Anchor( name="Effector0" )

    structure_single = {A00: [R00],
                             R00: [P00],
                             P00: [R10],
                             R10: [P10],
                             P10: [R20],
                             R20: [E0]}


    scaraArmSingle = ActuatorGraph.ActuatorGraph( name="scaraArmSingle" )
    scaraArmSingle.loadDictionary( structure_single )
    scaraArmSingle.setAnchors([A00])
    scaraArmSingle.setEndeffectors([E0])
    scaraArmSingle.partitionLinkages()
    for linkage in scaraArmSingle.linkages:
        Tools.initChain( linkage )

    if posed:
        R00.constraintRange( -sp.pi, sp.pi)
        R10.constraintRange( -sp.pi, sp.pi)
        R20.constraintRange( -sp.pi, sp.pi)
        A00.setCoordinates(sp.Matrix([0,0,0]))
        P00.setLength( 1 )
        P10.setLength( 1 )
        R00.setAngle( sp.pi/4 )
        R10.setAngle( -sp.pi/4 )
        R20.setAngle( 0 )

    if constraint:
        R00.constraintRange( -sp.pi, sp.pi)
        R10.constraintRange( -sp.pi, sp.pi)
        R20.constraintRange( -sp.pi, sp.pi)
        P00.setLength( 1 )
        P10.setLength( 1 )
        A00.setCoordinates(sp.Matrix([0,0,0]))



    Algorithms.calculateEndPosition( scaraArmSingle )
    #print( "Initial end position:", E0.globalpositionmatrix)

    listOfActuators = [ A00, P00, R00, R20, P10, R10, E0 ]

    return scaraArmSingle, listOfActuators

def single_scara_spherical():
    A00 = Anchor.Anchor( name="Anchor0" )
    #A00.setCoordinates(sp.Matrix([0,0,0]))
    P00 = Prismatic.Prismatic( name="Shoulder", actuated=False )
    S00 = Spherical.Spherical( name="Angle_Shoulder", actuated=True )
    S20 = Spherical.Spherical( name="Angle_Endeffector", actuated=False )
    S10 = Spherical.Spherical( name="Angle_Ellbow", actuated=True )
    P10 = Prismatic.Prismatic( name="Ellbow", actuated=False )
    E0 = Anchor.Anchor( name="Effector0" )

    structure_single = {A00: [S00],
                        S00: [P00],
                        P00: [S10],
                        S10: [P10],
                        P10: [S20],
                        S20: [E0]}

    S00.constraintRange( -sp.pi, sp.pi)
    S10.constraintRange( -sp.pi, sp.pi)
    S20.constraintRange( -sp.pi, sp.pi)

    scaraArmSingle = ActuatorGraph.ActuatorGraph( name="scaraArmSingle" )
    scaraArmSingle.loadDictionary( structure_single )
    scaraArmSingle.setAnchors([A00])
    scaraArmSingle.setEndeffectors([E0])
    scaraArmSingle.partitionLinkages()
    for linkage in scaraArmSingle.linkages:
        Tools.initChain( linkage )

    Algorithms.calculateEndPosition( scaraArmSingle )
    print( "Initial end position:", E0.globalpositionmatrix )

    return scaraArmSingle


def single_scara_tilted():
    A0 = Anchor.Anchor(name="Anchor0", coordinates=sp.Matrix([0, 0, 0]))
    A1 = Anchor.Anchor(name="Anchor1", coordinates=sp.Matrix([0, 0, 0]))
    P0 = Prismatic.Prismatic(name="Prismatic0")
    P1 = Prismatic.Prismatic(name="Prismatic1")

    R0 = Revolute.Revolute(name="Revolute0")
    R1 = Revolute.Revolute(name="Revolute1")
    R2 = Revolute.Revolute(name="Revolute2")

    _P0 = Prismatic.Prismatic(name="P0_sensor")
    _P1 = Prismatic.Prismatic(name="P1_sensor")
    _P0.setLength(1)
    _P1.setLength(1)
    _F0 = Flex.Flex(name="Flex0_sensor")  # default: loop device
    S0 = Tools.makeActuatorGraph([_P0, _F0, _P1])
    S0.partitionLinkages()

    chain_sensor = Tools.makeActuatorGraph(sp.flatten([A0, R0, S0.getListOfActuators(), R1, P1, R2, A1]))
    chain_sensor.partitionLinkages()
    R0.constraintRange(-sp.pi / 2, sp.pi / 2)
    R1.constraintRange(-sp.pi / 2, sp.pi / 2)
    R2.constraintRange(-sp.pi / 2, sp.pi / 2)
    P0.setLength(1)
    P1.setLength(1)
    Tools.initChain(chain_sensor)
    return chain_sensor


def dual_scara( ):
    A00 = Anchor.Anchor( name="Anchor0" )
    P00 = Prismatic.Prismatic( name="Prismatic0_left", actuated=False )
    P10 = Prismatic.Prismatic( name="Prismatic1_left", actuated=False )
    R00 = Revolute.Revolute( name="Revolute0_left", actuated=True )
    R10 = Revolute.Revolute( name="Revolute1_left", actuated=False )
    R20 = Revolute.Revolute( name="Revolute2_left", actuated=False )

    A01 = Anchor.Anchor( name="Anchor1", coordinates= [sp.sqrt(2)*2,0,0] )
    P01 = Prismatic.Prismatic( name="Prismatic0_right", actuated=False )
    P11 = Prismatic.Prismatic( name="Prismatic1_right", actuated=False )
    R01 = Revolute.Revolute( name="Revolute0_right", actuated=True )
    R11 = Revolute.Revolute( name="Revolute1_right", actuated=True )
    R21 = Revolute.Revolute( name="Revolute2_right", actuated=False )

    E0 = Anchor.Anchor( name="Effector0" )

    structure_dual = {A00: [R00, R01],
                           R00: [P00],
                           R01: [P01],
                           P00: [R10],
                           P01: [R11],
                           R10: [P10],
                           R11: [P11],
                           P10: [R20],
                           P11: [R21],
                           R20: [E0],
                           R21: [E0],
                           }

    scaraArmDual = ActuatorGraph.ActuatorGraph( name="scaraArmDual" )
    scaraArmDual.loadDictionary( structure_dual )
    scaraArmDual.setAnchors([A00])
    scaraArmDual.setEndeffectors([E0])
    scaraArmDual.partitionLinkages()
    for linkage in scaraArmDual.linkages:
        Tools.initChain( linkage )

    Algorithms.calculateEndPosition( scaraArmDual )
    print( "Initial end position:", E0.globalpositionmatrix)
    sp.pprint(  E0.globalpositionmatrix)
    return scaraArmDual
