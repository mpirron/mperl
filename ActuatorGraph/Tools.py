"""
Implementation of tools which manipulate ActuatorTree structures, e.g.
cloning trees, tree-ifying lists of actuators &c.
"""

import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor

from ActuatorGraph import Algorithms
from ActuatorGraph import GraphDecomposition

from ActuatorGraph.ActuatorTree import *
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes
from ActuatorGraph.Utils.NameGenerator import NameGenerator

import ActuatorGraph.ActuatorGraph as ActuatorGraph

import collections

# Traverse the tree, note operator and type of actuators. Then built
# the tree with same type of actuators.
def cloneChain(tree, constraints=False):
    """
    Make a structural copy of a given tree
    If constraints is set to True, cloneChain will also subject the newly created
    tree to the same constraints than the origin tree.
    """
    l = tree.getListOfActuators()
    tree2_list = []
    for e in l:
        if e == None:
            print( "Nonetype encountered")
        elif e.getType() == ActuatorTypes.Revolute:
            a = Revolute.Revolute( name=NameGenerator.next(ActuatorTypes.Revolute) )
            a.setCoordinates( e.getCoordinates()) 
            a.minAngle = e.minAngle
            a.maxAngle = e.maxAngle
            tree2_list.append( a )
        elif e.getType() == ActuatorTypes.Prismatic:
            a = Prismatic.Prismatic( name=NameGenerator.next(ActuatorTypes.Prismatic) )
            a.setCoordinates( e.getCoordinates()) 
            a.setLength( e.getLength() )
            a.minLen = e.minLen
            a.maxLen = e.maxLen
            tree2_list.append( a )
        elif e.getType() == ActuatorTypes.Anchor:
            a = Anchor.Anchor( name=NameGenerator.next(ActuatorTypes.Anchor))
            a.setCoordinates( e.getCoordinates() )
            tree2_list.append( a )
        else:
            print( "clone chain error?" )
    return makeActuatorGraph(tree2_list)


def copyConstraints( actuator1, actuator2 ):
    """Copy constraints from actuator1 to actuator2"""
    pass

# Make a tree recursively from a list of actuators / actuatortypes

def getRoot( tree ):
    """Get the root of the ActuatorTree"""
    return tree.anchors

def getLeaves( tree ):
    """ """
    pass


# Buggy: Not all cases covered
def appendActuator( tree, actuator=None, actuatorType=None ):
    """ Appends an actuator to an existing tree. Currently only union supported """
    if tree.right != None:
        appendActuator( tree.right, actuator, actuatorType )
    else:
        tree.left = ActuatorTree( tree.token )
        tree.right = ActuatorTree( actuator )
        tree.token = "union"

def removeAnchors( tree ):
    """Removes all anchor points from a tree and returns a new tree"""
    actuators = tree.getListOfActuators()
    return makeActuatorGraph([a for a in actuators if a.getType() != ActuatorTypes.Anchor])


def addAnchors( tree ):
    """Adds anchors to a tree"""
    actuators = tree.getListOfActuators()
    actuators.insert(0, Anchor.Anchor() )
    actuators.insert(len(actuators), Anchor.Anchor() )
    return makeActuatorGraph(actuators)


def removeActuator( tree, actuator=None, actuatorType=None ):
    """Removes an actuator from a tree"""
    pass


def appendChain(tree, treeToAppend):
    tree = removeAnchors( tree )
    treeToAppend = removeAnchors( treeToAppend )
    actuators_tree = tree.getListOfActuators()
    actuators_treeToAppend = treeToAppend.getListOfActuators()
    return makeActuatorGraph(actuators_tree + actuators_treeToAppend)


def setAnchor( tree, coordinate ):
    """Set coordinate of root anchor"""
    l = tree.getListOfActuators()
    if l[0].getType() == ActuatorTypes.Anchor:
        l[0].setCoordinates( coordinate )
        return True
    return False


def makeActuatorGraph(listOfActuators):
    """
    Given a list of single actuators, create a ActuatorGraph structure:
    makeActuatorGraph( A,R,P,A ) -> {A:R, R:P,P:A}
    makeActuatorGraph( A,R,[P1,P2] ) -> {A:R, R: [P1, P2]}
    makeActuatorGraph( A,R,[P1,P2],[[],R] ) -> {A:R, R: [P1, P2], P2:R}
    makeActuatorGraph( A,R,[P1,P2],A ) -> {A:R, R: [P1, P2], P1:A, P2:A}
    """
    d = {}
    #listOfActuators = sp.flatten( listOfActuators ) # for 2r2p, this costs >1s oO
    assert( type(listOfActuators)==list )
    if len(listOfActuators)>1:
        for i in range( len( listOfActuators)-1 ):
            d[ listOfActuators[i] ] = [listOfActuators[i+1]]
        #d[ listOfActuators[-1]] = [listOfActuators[-1]]
    else:
        d[listOfActuators[0]] = [listOfActuators[0]]
    g = ActuatorGraph.ActuatorGraph( name="makeActuatorGraph_generated_graph")
    g._dictionary = d
    g.setAnchors( listOfActuators[0] )
    g.setEndeffectors( listOfActuators[-1])
    return g



def getPredecessor( tree=None, actuator=None, actuatorType=None ):
    """Given an actuator, return its predecessor"""
    nodes = tree.getListOfActuators()
    pos = [ i for (i,e) in enumerate( nodes ) if e == actuator ][-1]    # only interested in the last occurence
    return nodes[ pos-1 ] if pos>0 else None


def getSuccessor( tree=None, actuator=None, actuatorType=None ):
    """Given an actuator, return its successor"""
    nodes = tree.getListOfActuators()
    pos = [ i for (i,e) in enumerate( nodes ) if e == actuator ][0]    # only interested in the first occurence
    return nodes[ pos+1 ] if pos<len(nodes)-1 else None


def printTree( tree ):
    """Textual and graphical representation of the tree"""
    if tree is None: 
        print("")
        return 
    print( tree.token, end=" " )
    printTree( tree.left )
    printTree( tree.right )


def getListOfVariables( tree=None, actuatorType=None, actuated=None, name=None ):
    """Compile a list of specific variables"""
    listOfActuators = tree.getListOfActuators( actuatorType=actuatorType, actuated=actuated, name=name )
    variables = []
    #variables = [ e.__getSymbols__() for e in listOfActuators ]       
    for e in listOfActuators:
        variables.append( e.__getSymbols__() )
    return variables


def __flatten__( l ):
    """Unnest a list of lists (or list of tuples .. )"""
    for el in l:
        if isinstance( el, collections.Iterable ) and not isinstance( el, (str, bytes )):
            yield from flatten( el )
        else:
            yield el

def flatten( l ):
    """Unnest a list of lists (or list of tuples .. )"""
    return list( __flatten__( l ) ) 


def updateCoordinates( tree ):
    """After changing parameters of joints, update their corresponding coordinates"""
    Algorithms.calculateEndPosition( tree )
    #l = tree.getListOfActuators()
    #for actuator in l:
    #    position = Algorithms.calculateEndPosition( tree, endActuator=l )
    #    #actuator.setCoordinates( position.col(-1) )


# to change to dict!
def initChain( actuatorgraph ):
    """
    After composing the tree, the coordinates of the indivual actuators have to be set once
    configurationMatrix: 4x4, lfor, orientation / length
    positionMatrix: 4x4, gfor, no orientation / length
    coordinates: 3x1, gfor, no orientation / length
    """

    #actuators = tree.getListOfActuators()+tree.endeffectors

    #traverse path in order
    position = sp.eye(4)
    paths = []
    #print( "Calculating paths")
    for a in actuatorgraph.anchors:
        for e in actuatorgraph.endeffectors:
            #print( "Compose paths from", a.name, "to", list(map( lambda l: l.name, actuatorgraph.endeffectors ) ) )
            paths.append(GraphDecomposition.find_all_paths( actuatorgraph, a, e ) )

    #print( "Calculating initial position")
    for a in actuatorgraph.anchors:
        if a.getType() == ActuatorTypes.Anchor:
            position = a.getPositionMatrix()  #holds origin in global frame of reference
            assert( position.shape == (4,4))
        else:
            position = sp.Matrix( [[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]] )
            assert( position.shape == (4,4))
        #print("Set initial position to", position)
        Algorithms.calculateEndPosition( actuatorgraph )

    #print( "Calculating positions" )
    current_offset = sp.eye(4)
    for a in actuatorgraph._dictionary.keys():
        #print( "Current node:", a)
        M = current_offset.multiply(a.getConfigurationMatrix())
        M.col_del(-1)
        M = M.col_insert(3, sp.Matrix( [0,0,0,1] ) )
        #print( "Set offset to ", M )
        for actuator in actuatorgraph._dictionary[a]:
            actuator.parent_orientation = M
            actuator.setCoordinates( position.col(-1)[0:3])
            position = position.multiply( actuator.getConfigurationMatrix() ) #gfor + configuration
            #print( "Current configuration Matrix", actuator.getConfigurationMatrix() )
            #print("Set position of", actuator.name,"to", position.col(-1))
            assert( position.shape == (4,4))
        Algorithms.calculateEndPosition( actuatorgraph )
        current_offset = M

#Bug -> (4,3) matrix vs. (4,4) matrix ..
def matrix_translation( M, coords ):
    assert( False )
    assert( M.shape == (4,4))
    M.col_del(-1)
    print( "MT1", M)
    assert( M.shape == (4,3))
    M = M.col_insert( 3, sp.Matrix( [coords[0], coords[1], coords[2], 0] ))
    assert( M.shape == (4,4))
    print( "MT2", M )

    return M

def matrix_get_rotation( M ):
    M2 = M[:, :]
    M2.col_op(3, lambda i, j: 1 if j == 3 else 0)
    assert( M2.shape == (4,4))
    return sp.Matrix(M2)

def getPartialChain( tree, A1, A2):
    paths = GraphDecomposition.find_all_paths( tree, A1, A2)
    for path in paths:
        print( "Now on path", [a.name for a in path] )
    return paths[0] if len(paths)==1 else paths


def getTransformationMatrices( graph, A1, A2, substitution=True ):
    path = getPartialChain( graph, A1, A2 )
    res = {}
    R = sp.eye(4)
    subst = []
    for tree in graph.adjacentNodes:
        subst = subst + tree.__getSubstitutions__()
    subst1 = []
    for e in subst:
        for f in e:
            subst1.append( f)

    for node in path:
        M = node.globalpositionmatrix
        R = R.multiply(M).subs(subst1) if substitution else R.multiply(M)
        res[node] = R

    return res



def mapSolutions( graph, solutions ):
    """
    """
    for x,v in solutions:
        x = str(x).split("_")
        parameter = x[-1]
        actuator = '_'.join(x[:len(x)-1])
        for tree in graph:
            for a in tree._actuators:
                if a.name == actuator:
                    a.setAngle( v )
    print("map ok")


def getRotationMatrix( axis ):
    theta_x, theta_y, theta_z = sp.symbols( "theta_x theta_y theta_z" )
    X = sp.Matrix( [[1,0,0,0],[0, sp.cos(theta_x), -sp.sin(theta_x),0], [0, sp.sin(theta_x), sp.cos(theta_x),0], [0, 0, 0, 1]])
    Y = sp.Matrix( [[sp.cos(theta_y), 0, sp.sin(theta_y), 0], [0, 1, 0, 0], [-sp.sin(theta_y), 0, sp.cos(theta_y), 0 ], [0, 0, 0, 1]] )
    Z = sp.Matrix( [[sp.cos(theta_z), -sp.sin(theta_z), 0, 0], [sp.sin(theta_z), sp.cos(theta_z), 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])

    f = lambda e: X if e == "X" else Z if e == "Z" else Y

    res = sp.eye(4)
    for m in [e for e in axis ]:
        res = res.multiply( f(m) )
    return (res, [theta_x, theta_y, theta_z])



def loadIncidenceMatrix ( incidenceMatrix, actuatorMatrix ):
    """
    Convert an incidence matrix into an actuator graph structure
    """
    pass

def loadDictionary( dictionary ):
    """ """

def traverseLinkageReversed( graph ):
    pass

