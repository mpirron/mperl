import ActuatorGraph.Units.__si_units as si_units
import sympy as sp


def torqueFLt( F, l, theta ):
    if F != si_units.newton or l != si_units.metre or theta != si_units.radian:
        raise si_units.UnitMismatch
    else:
        T = si_units.newton_metre_torque
        T.set_value( F*l*sp.sin( theta ) )
        return T


def torqueTrF( r, F ):
    if F != si_units.newton or r != si_units.metre:
        raise si_units.UnitMismatch
    if type(F.value) != sp.Matrix or type(r) != sp.Matrix:
        raise si_units.UnitArgumentMismatch

    T = si_units.newton_metre()
    T.set_value( r.cross(F))
    return T


def youngsModulus():
    pass




