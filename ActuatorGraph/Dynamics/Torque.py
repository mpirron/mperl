import ActuatorGraph.GeometricAlgorithms as GeometricAlgorithms
import ActuatorGraph.Tools as Tools
import ActuatorGraph.Dynamics.Algorithms as DynamicAlgorithms
import ActuatorGraph.Units.__si_units as si_units


import ActuatorGraph.Actuators.Revolute as Revolute
from ActuatorGraph import GraphDecomposition
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes


import numpy as np
import sympy as sp

def calculateTorque( robotic_system, weight=0 ):
    '''
    Argument is dictionary of a robotic system or the system itself.
    Weight can be applied to the end effector or omitted, if applied prior.
    :param dictionary:
    :return: Nothing. Check individual actuators.
    '''

    linkage = sp.flatten( [list(robotic_system._dictionary.keys())[0]] + list(robotic_system._dictionary.values()))
    print(linkage)
    endeffector_position = sp.Matrix(linkage[-1].globalpositionmatrix.col(-1)[0:3])

    # we need to go backwards now ..
    dictionary = robotic_system._dictionary
    src = list(dictionary.keys())

    total_force = weight

    for e in reversed( src ):
        dst = dictionary[e]
        if type( dst ) != list:
            dst = [dst]

        for actuator in reversed( dst ):
            if actuator.endeffector:
                total_force += actuator.force
            if actuator.getType() == ActuatorTypes.Prismatic:
                total_force += actuator.weight / 2
            print("total force:", total_force)

            if actuator.getType() == ActuatorTypes.Revolute:
                print( actuator.name )
                l = si_units.metre
                F = si_units.newton
                theta = si_units.radian
                current_position = sp.Matrix(actuator.globalpositionmatrix.col(-1)[0:3])
                length_vector = (endeffector_position-current_position)
                if length_vector == sp.Matrix([0,0,0]): #
                    continue
                #force_vector =  -endeffector_position+sp.Matrix([1,0,0])
                angle = GeometricAlgorithms.angleBetweenVectors( length_vector, sp.Matrix([1,0,0]) )
                print( "Angle between connecting vector and x-axis:", angle)
                F.set_value( total_force )
                l.value = length_vector.norm()
                theta.value = sp.pi/2-angle
                torque = DynamicAlgorithms.torqueFLt( F, l, theta )
                print("On %s: F=%s, l=%s, theta=%s, torque=%s" %( actuator.name, F, l, theta, torque) )
                #angle = GeometricAlgorithms.angleBetweenVectors(P, sp.Matrix(actuator.globalpositionmatrix.col(-1)[0:3]))
                actuator.torque = torque if torque == torque else total_force
                print( "Torque on %s is %s" %(actuator.name, torque))




class Torque( ):
    def __init__( self, actuatorgraph, load ):
        self._actuatorgraph = actuatorgraph
        self.load = self.add_load(load)


    def __analyzeGraph__( self ):
        '''
        Check if the graph is serial, implicit, or true parallel
        :return:
        '''
        pass


    def __torqueSerial__( self ):
        assert( len(self._actuatorgraph.linkages) == 1 )
        actuators = self._actuatorgraph.linkages
        origin = self._actuatorgraph.getRootAnchor()

        total_force = self.load

       # linkage = self._actuatorgraph.getListOfActuators()
        linkage = sp.flatten([list(self._actuatorgraph._dictionary.keys())[0]]+list(self._actuatorgraph._dictionary.values()))
        print(linkage)

        endeffector_position= sp.Matrix(linkage[-1].globalpositionmatrix.col(-1)[0:3])

        #for actuator in reversed(linkage):      # Bug: order is not preserved
        for i in range( len(linkage)-1,0,-1):

            actuator = linkage[i]
            print("Actuator:", actuator.name)

            if actuator.endeffector:
                total_force += actuator.force
            if actuator.getType() == ActuatorTypes.Prismatic:
                total_force += actuator.weight / 2
            print("total force:", total_force)
            if actuator.getType() == ActuatorTypes.Revolute:
                l = si_units.metre
                F = si_units.newton
                theta = si_units.radian

                current_position = sp.Matrix(actuator.globalpositionmatrix.col(-1)[0:3])

                length_vector = (endeffector_position-current_position)
                #force_vector =  -endeffector_position+sp.Matrix([1,0,0])



                angle = GeometricAlgorithms.angleBetweenVectors( length_vector, sp.Matrix([1,0,0]) )
                print( "Angle between connecting vector and x-axis:", angle)

                F.set_value( total_force )
                l.value = length_vector.norm()
                theta.value = angle
                torque = DynamicAlgorithms.torqueFLt( F, l, theta )
                print("On %s: F=%s, l=%s, theta=%s, torque=%s" %( actuator.name, F.value, l.value, theta.value, torque.value) )


                #angle = GeometricAlgorithms.angleBetweenVectors(P, sp.Matrix(actuator.globalpositionmatrix.col(-1)[0:3]))


                actuator.torque = torque

                print( "Torque on %s is %s" %(actuator.name, torque))


    def __torqueSerial_linkage__( self, linkage ):
        total_force = self.load

        # linkage = self._actuatorgraph.getListOfActuators()
        #linkage = sp.flatten([list(self._actuatorgraph._dictionary.keys())[0]]+list(self._actuatorgraph._dictionary.values()))

        print(linkage)

        endeffector_position= sp.Matrix(linkage[-1].globalpositionmatrix.col(-1)[0:3])

        #for actuator in reversed(linkage):      # Bug: order is not preserved
        for i in range( len(linkage)-1,0,-1):

            actuator = linkage[i]
            print("Actuator:", actuator.name)

            if actuator.endeffector:
                total_force += actuator.force
            if actuator.getType() == ActuatorTypes.Prismatic:
                total_force += actuator.weight / 2
            print("total force:", total_force)
            if actuator.getType() == ActuatorTypes.Revolute:
                l = si_units.metre
                F = si_units.newton
                theta = si_units.radian

                current_position = sp.Matrix(actuator.globalpositionmatrix.col(-1)[0:3])

                length_vector = (endeffector_position-current_position)

                #force_vector =  -endeffector_position+sp.Matrix([1,0,0])



                angle = GeometricAlgorithms.angleBetweenVectors( length_vector, sp.Matrix([1,0,0]) )
                print( "Angle between connecting vector and x-axis:", angle)

                F.set_value( total_force )
                l.value = length_vector.norm()
                theta.value = angle
                torque = DynamicAlgorithms.torqueFLt( F, l, theta )
                print("On %s: F=%s, l=%s, theta=%s, torque=%s" %( actuator.name, F.value, l.value, theta.value, torque.value) )


                #angle = GeometricAlgorithms.angleBetweenVectors(P, sp.Matrix(actuator.globalpositionmatrix.col(-1)[0:3]))


                actuator.torque = torque

                print( "Torque on %s is %s" %(actuator.name, torque))


    #def __torqueSerial__(self):
    #    pass


    def __torqueImplicit__( self ):
        number_linkages = len( self._actuatorgraph.linkages )
        for endeffector in self._actuatorgraph.getEndEffectors():
            endeffector.force = self.load/number_linkages

        for linkage in self._actuatorgraph.linkages:
            self.__torqueSerial_linkage__( linkage )


    def __torqueParallel__( self ):
        pass


    def calculateTorque( self ):
        pass


    def add_load(self, w):
        '''
        Apply a load to the system
        '''
        if w != si_units.newton:
            raise si_units.UnitMismatch
        else:
            self.load = w