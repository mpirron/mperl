"""
Cyclic Coordinate Descent
"""

from math import sqrt

import ActuatorGraph.GeometricAlgorithms as GeometricAlgorithms
import ActuatorGraph.Tools as Tools
import ActuatorGraph.Algorithms as Algorithms

import ActuatorGraph.Actuators.Revolute as Revolute
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes

import numpy as np
import sympy as sp


norm = lambda v1: distanceBetweenPoints( sp.Matrix([0,0,0]), v1 )
withinSpec = lambda M: list(map( lambda x: abs(x[0]-x[1])<0.001, zip(M[0],M[1]))).count( False ) == 0

def distanceBetweenPoints( v1, v2 ):
    """Calculates the euclidean distance between two points in 3D"""
    return sp.sqrt( (v1[0]-v2[0])**2+(v1[1]-v2[1])**2+(v1[2]-v2[2])**2 )


def getAngle( j, e, t ):
    """
    In order to move the end effector to the target position, we need
    to rotate it by a certain angle theta. We use the dot product to get
    this angle: e.j = |e||j|cos(theta)
    j = current joint position
    e = current end effector position
    t = target position
    Note: Returns value in radians!
    """
    return sp.acos( (e-j).dot(t-j) / ( norm( e-j )*norm( t-j ) ) )


def getAngleOfProjection( origin, P, T ):
    """
    Denote by P the current direction of the vector and by T the target vector.
    Returns a tuple with the angles of the plane-projected angles ( xy, xz, yz )
    Can then be used directly for the individual revolute matrices.
    """
    v = P+T-origin
    return sp.acos( v[0]/sqrt(v[0]**2+v[1]**2)), sp.acos( v[0]/sqrt(v[0]**2+v[2]**2) ), sp.acos( v[1]/sqrt( v[1]**2+v[2]**2) ) #xy, xz, yz


def getDirection( j, e, t ):
    """
    Calculates the direction angle
    j = current joint position
    e = current end effector position
    t = target position
    Check for result <0 or >0.
    """
    n = ( e[0]-j[0] )*( t[1]-j[1] ) - ( e[1]-j[1] )*( t[0]-j[0] ) 
    d = norm( e-j ) * norm( t-j )
    return sp.asin( n/d )


def unifyAngle( angle ):
    """ """
    angle = angle % (2.0 * sp.pi)
    if (angle < -sp.pi):
        angle += (2.0 * sp.pi)
    else:
        angle -= (2.0 * sp.pi)
    return angle



def setLengthofPrismaticActuators( listOfActuators, target ):
    """Set length of prismatic actuators uniform from A0 to target."""
    length = GeometricAlgorithms.distanceBetweenPoints( (0,0,0,0), target )
    n = sum( map( lambda x: (0,1)[ x.getType() == 'Prismatic' ], listOfActuators ) )    
    [ a.setLength(length/n) for a in listOfActuators if a.getType() == 'Prismatic' ]


#def orientActuatorTree2D( tree, target, tol=0.01, maxIterations=10 ):
#    """
#    Given a tree and a target, iterate over the tree and calculate the angular
#    values using CCD. Works for delta robot e.g., though needs some iterations to
#    get it right.
#    """
#    endPosition = Algorithms.calculateEndPosition( tree ).col(-1)
#    if endPosition == target:
#        return True
#
#    l = tree.getListOfActuators()
#    #for actuator in l:
#    #    if actuator.getType() == ActuatorTypes.Revolute: actuator.setAngle(0)
#    #    if actuator.getType() == ActuatorTypes.Prismatic: actuator.setLength( actuator.minLen )
#
#    for i in range( 0, maxIterations ):
#        endPosition = __orientActuatorTree2D__( tree, target )
#        distance = GeometricAlgorithms.distanceBetweenPoints( target, endPosition.col(-1)[0:3] )
#        print( "distance:", distance )
#        if distance < tol:
#            return True
#    return False




# def __orientActuatorTree2D__( tree, target, maxIterations=100 ):
#     Tools.initChain(tree)
#     for i in range( maxIterations ):
#         l = tree.getListOfActuators()
#         for actuator in reversed(l):
#             if actuator.getType() == ActuatorTypes.Revolute:
#                 Algorithms.calculateEndPosition( tree )
#                 origin = actuator.getGlobalPositionMatrix()
#                 #origin = Algorithms.calculateEndPosition( tree, actuator )
#                 endeffector = l[-1].getGlobalPositionMatrix()
#                 #endeffector= sp.Matrix( Algorithms.calculateEndPosition( tree ).col(-1)[0:3] )
#                 angle = getAngle( sp.Matrix(origin.col(-1)[0:3]) , sp.Matrix(endeffector.col(-1)[0:3]), target )
#                 print( "")
#                 print(" origin:", origin.col(-1)[0:3], "endeffector", endeffector.col(-1)[0:3], "target", target )
#                 direction = getDirection( sp.Matrix(origin.col(-1)[0:3]) , sp.Matrix(endeffector.col(-1)[0:3]), target )
#                 print( "direction", direction )
#                 if direction < 0: angle = -angle
#                 actuator.setAngle(  angle  )
#                 Algorithms.calculateEndPosition( tree )
#                 endPosition = l[-1].getGlobalPositionMatrix()
#                 if withinSpec( (endPosition, target) ):
#                     return endPosition
#     Algorithms.calculateEndPosition( tree )
#     endPosition = l[-1].getGlobalPositionMatrix()
#     print( "ccd: endposition %s target %s" %( endPosition.col(-1), target ) )
#     for a in tree.getListOfActuators():
#         print( "\t", a )
#     return endPosition

def __orientActuatorTree2D__( tree, target, maxIterations=20 ):
    listOfActuators = tree.getListOfActuators( actuatorType=ActuatorTypes.Revolute, actuated=True)

    print( "->", listOfActuators)
    Tools.initChain( tree )
    Algorithms.calculateEndPosition( tree )
    n = len(listOfActuators)

    # ccd needs some initial angles
    for actuator in listOfActuators:
        actuator.setAngle(0)
    Tools.initChain(tree)

    #bug: must substitute all variables with values before continuing

    for m in range(maxIterations):
        for i in range( n ):
            actuator = listOfActuators[i]

            joint = actuator.getGlobalPositionMatrix()
            end = joint.multiply( sp.Matrix( [[1,0,0,1],[0,1,0,0],[0,0,1,0],[0,0,0,1]]))

            print( "debug joint>", joint)
            print( "debug end>", end )

            angle = getAngle( sp.Matrix(joint.col(-1)[0:3]), sp.Matrix(end.col(-1)[0:3]), target )
            direction = getDirection( sp.Matrix(joint.col(-1)[0:3]), sp.Matrix(end.col(-1)[0:3]), target )


            print( angle, direction )

            angle = -angle if direction < 0 else angle
            actuator.setAngle( angle )
            Algorithms.calculateEndPosition( tree )

            #end = sp.Matrix(list(tree._dictionary.values())[-1][-1].getGlobalPositionMatrix().col(-1)[0:3])
            end = sp.Matrix(list(tree._dictionary.values())[-1][-1].globalpositionmatrix.col(-1)[0:3])
            #end = sp.Matrix(tree[-1].getGlobalPositionMatrix().col(-1)[0:3])


            print("\t -> endeffector:", tree.endeffectors, end )

            print( "distance between points:", distanceBetweenPoints( end, target ))
            if distanceBetweenPoints( end, target ) < 1e-1:
                print("->", distanceBetweenPoints( sp.N(end), sp.N(target) ) )
                print( "end", end )
                print( "target", target )
                return True



#def orientActuatorGraph( graph, target, tol=0.1 ):
#    for linkage in graph.getLinkage():
#        orientActuatorTree2D( linkage, target, tol )
#
#    for linkage in graph.getLinkage():
#        for actuator in linkage:
#            print( actuator )

