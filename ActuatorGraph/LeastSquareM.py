"""
Try to solve the inverse kinematics using least square optimisation
"""
from sympy.utilities.autowrap import ufuncify
from functools import reduce
from operator import concat
from scipy.optimize import least_squares
import sympy as sp
import numpy as np
import logging

import ActuatorGraph.Algorithms as Algorithms
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes
import ActuatorGraph.GraphDecomposition as GraphDecomposition

# log to the console
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('[%(name)s] %(levelname)s: %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

def logMultiline(level, msg):
    for l in msg.split('\n'):
        logger.log(level, l)

#TODO solveInverseKinematics(graph, effector, target, tolerance, debug):
# get all the paths from anchor points to the effector (can we replace the effector by an anchor at target?)
# for each path: create distance function
# for any two paths which share an actuator add an extra distance function for the shared elements reached through the different paths
class LeastSquares():
    def __init__(self, graph, endeffector, tolerance=1e-4, debug=True):
        self.paths          = None
        self.variables      = None
        self.lowerBounds    = None
        self.upperBounds    = None
        self.paths          = None
        self.penalty        = None

        self.graph          = graph
        self.endeffector    = endeffector


        self.tolerance = tolerance
        self.debug = debug

    def solveInverseKinematics(self, target, tolerance =1e-4, debug = False):
        graph = self.graph
        effector = self.endeffector

        if debug:
            logger.setLevel(logging.DEBUG)
        else:
            logger.setLevel(logging.INFO)
        if not self.paths:
            self.paths = self._getPaths(graph, effector)
        if not self.penalty:
            self.penalty = self._computeParallelStructPenaltyFunctions(graph, self.paths, effector)
        distances = self._computeObjectiveFunction(self.paths, target) + self.penalty
        #print( "distances,", distances )
        distance = reduce(sp.Add, distances)
        logMultiline(logging.DEBUG, "minimize:\n" + sp.pretty(distance))

        if not ( self.variables or self.lowerBounds or self.upperBounds ):
            self.variables, self.lowerBounds, self.upperBounds = self._getBoundsFor(graph, distance.free_symbols)

        return self._solve(distance, self.variables, self.lowerBounds, self.upperBounds, tolerance, debug)


    def _distance2( self, p1, p2 ):
        l_p1 = len( p1 )
        l_p2 = len( p2 )
        assert( l_p1 == l_p2 )
        sum = 0
        for i in range( l_p1 ):
            sum += (p1[i]-p2[i])**2
        return sum

    def _getVarList( self, actuator ):
        syms = []
        _syms = actuator.__getSymbols__() #assumes all the parameters are different
        if isinstance(_syms, tuple):
            for s in _syms:
                syms.append(s)
        elif isinstance(_syms, list):
            syms.extend(_syms)
        else:
            syms.append(_syms)
        return syms

    def _getSymbolBounds( self, conjuncts, symbol ):
        b = sp.Interval(-sp.oo, sp.oo)
        related = [c for c in conjuncts if symbol in c.free_symbols]
        for c in conjuncts:
            if symbol in c.free_symbols:
                try:
                    i = sp.solve_univariate_inequality(c, symbol, relational=False)
                    b = b.intersect(i)
                except:
                    logger.warning("constraints '%s' is not a bound. TODO convert it to a cost function", c)
        lb = sp.N(b.start) if b.start != -sp.oo else -np.inf
        ub = sp.N(b.end) if b.end != sp.oo else np.inf
        return (lb, ub)

    def _getActuatorBounds( self, actuator ):
        variables = []
        lowerBounds = []
        upperBounds = []
        logger.debug("actuator %s", actuator)
        #get variables
        syms = self._getVarList(actuator)
        variables.extend(syms)
        #get bounds
        constraints = reduce(concat, actuator.getConstraints())
        logger.debug("constraints %s", constraints)
        if sp.flatten( constraints ) != []:
            conjuncts = sp.And.make_args( reduce( sp.And, sp.flatten( constraints ) ) )
        else:
            conjuncts = sp.And.make_args( True )
        for s in syms:
            lb, ub = self._getSymbolBounds(conjuncts, s)
            lowerBounds.append(lb)
            upperBounds.append(ub)
        return (variables, lowerBounds, upperBounds)

    def _getActuatorsBounds( self, actuators ):
        variables = []
        lowerBounds = []
        upperBounds = []
        for act in actuators:
            vs, lbs, ubs = self._getActuatorBounds(act)
            variables.extend(vs)
            lowerBounds.extend(lbs)
            upperBounds.extend(ubs)
        return (variables, lowerBounds, upperBounds)

    def _getBoundsFor( self, graph, symbols ):
        actuators = graph.getListOfActuators()
        variables, lowerBounds, upperBounds = self._getActuatorsBounds(actuators)
        zipped = zip(variables, lowerBounds, upperBounds)
        variables = []
        lowerBounds = []
        upperBounds = []
        for v, l, u in zipped:
            if v in symbols:
                variables.append(v)
                lowerBounds.append(l)
                upperBounds.append(u)
        logger.debug("variables: %s", variables)
        logger.debug("lower bounds: %s", lowerBounds)
        logger.debug("upper bounds: %s", upperBounds)
        return (variables, lowerBounds, upperBounds)

    def _getPaths( self, graph, effector ):
        anchors = [a for a in graph.getListOfActuators(ActuatorTypes.Anchor) if a != effector]
        logger.debug("anchors: %s", ", ".join(x.getName() for x in anchors))
        paths = []
        for a in anchors:
            logger.debug("finding path from %s to %s:", a.getName(), effector.getName())
            newPaths = GraphDecomposition.find_all_paths(graph, a, effector)
            paths.extend(newPaths)
            #for p in newPaths:
            #    logger.debug("    %s:", ", ".join(x.getName() for x in p))
        return paths

    def _computeObjectiveFunction( self, paths, target ):
        logger.debug("computing effector objective function.")
        distances = []
        for p in paths:
            #logger.debug("processing path %s", ",".join( a.getName() for a in p))
            Algorithms.calculateEndPosition( p )
            endPosition = p.getEndEffector()[-1].getGlobalPositionMatrix()  #bug
            distances.append( self._distance2( target, endPosition.col(-1)[0:3] ) )
        return distances

    def _computeParallelStructPenaltyFunctions( self, graph, paths, effector):
        logger.debug("computing constraints due to parallel structure.")
        distances = []
        notAnchorOrEffector = [a for a in graph.getListOfActuators() if a.getType() != ActuatorTypes.Anchor and a != effector]
        for na in notAnchorOrEffector:

            #pathToNa = [ p[:p.index(na)] for p in paths if p.count(na) > 0 ]

            pathToNa = [] #bug
            for p in paths:
                tentative_path = GraphDecomposition.find_all_paths( p, p.anchors[0], na )   # Might hold a partial path
                if tentative_path != []:
                    pathToNa.append( tentative_path )






            if len(pathToNa) > 1:
                logger.debug("looking at %s paths to %s", len(pathToNa), na.getName())
                head = pathToNa.pop()
                Algorithms.calculateEndPosition( head )
                logger.debug(" representative path %s", ", ".join( x.getName() for x in head))
                endPosition1 = head[-1].getGlobalPositionMatrix().col(-1)[0:3]
                logger.debug("endPosition1: %s", endPosition1)
                for other in pathToNa:
                    logger.debug(" other path %s", ", ".join( x.getName() for x in other))
                    Algorithms.calculateEndPosition( other )
                    endPosition2 = other[-1].getGlobalPositionMatrix().col(-1)[0:3]
                    logger.debug("endPosition2: %s", endPosition2)
                    dist = self._distance2( endPosition1, endPosition2 )
                    distances.append(dist)
        return distances

    def _solve( self, distance, variables, lowerBounds, upperBounds, tolerance, debug ):
        #get Jacobian
        matrix = sp.Matrix(1, 1, [distance])
        jacobian = matrix.jacobian( variables )
        logMultiline(logging.DEBUG, "jacobian:\n" + sp.pretty(jacobian))
        #create the optimisation problem
        l_distance = sp.lambdify(tuple(variables), distance)
        def f_distance(x):
            return l_distance(*x)
        l_jacobian_separate = [sp.lambdify(tuple(variables), j) for j in jacobian]
        def f_jacobian(x):
            return [f(*x) for f in l_jacobian_separate]
        x0 = np.array([0] * len(variables))
        verb = 0
        if debug:
            verb = 2
        res = least_squares(f_distance, x0, f_jacobian, (lowerBounds, upperBounds), verbose = verb)
        if res.status < 0:
            raise Exception("ill-formed problem (a bug somewhere)")
        elif res.cost < tolerance:
            return zip(variables, res.x)
        else:
            raise Exception("could not find a solution")
