import sympy as sp
import logging
import ActuatorGraph.Algorithms as Algorithms
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes
import ActuatorGraph.GraphDecomposition as GraphDecomposition
from Utils.Solver import *

class IK():

    def __init__(self, solver = SolverKind.least_squares, tolerance =1e-4, debug = False):
        self.solver = solver
        self.debug = debug
        self.tolerance = tolerance
        # log to the console
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        ch = logging.StreamHandler()
        if debug:
            ch.setLevel(logging.DEBUG)
        else:
            ch.setLevel(logging.INFO)
        formatter = logging.Formatter('[%(name)s] %(levelname)s: %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    def solve(self, graph, effector, target):
        solver = self.solver.create(self.tolerance, self.debug)
        paths = self._getPaths(graph, effector)
        distances = self._computeObjectiveFunction(paths, target) + self._computeParallelStructPenaltyFunctions(graph, paths, effector)
        syms = set()
        for (l,r) in distances:
            if type(l) == list:
                for e in l:
                    syms.update(e.free_symbols)
            else:
                syms.update(l.free_symbols)

            if type(r) == list:
                for e in r:
                    syms.update(e.free_symbols)
            else:
                syms.update(r.free_symbols)

            solver.addEq(l, r)
        variables, lowerBounds, upperBounds = self._getBoundsFor(graph, syms)
        for (v,l,u) in zip(variables, lowerBounds, upperBounds):
            solver.addVar(v, l, u)
        return solver.solve()

    def _getVarList(self, actuator):
        syms = []
        _syms = actuator.__getSymbols__() #assumes all the parameters are different
        if isinstance(_syms, tuple):
            for s in _syms:
                syms.append(s)
        elif isinstance(_syms, list):
            syms.extend(_syms)
        else:
            syms.append(_syms)
        return syms

    def _getSymbolBounds(self, conjuncts, symbol):
        b = sp.Interval(-sp.oo, sp.oo)
        related = [c for c in conjuncts if symbol in c.free_symbols]
        for c in conjuncts:
            if symbol in c.free_symbols:
                try:
                    i = sp.solve_univariate_inequality(c, symbol, relational=False)
                    b = b.intersect(i)
                except:
                    self.logger.warning("constraints '%s' is not a bound. TODO convert it to a cost function", c)
        lb = sp.N(b.start)
        ub = sp.N(b.end)
        return (lb, ub)

    def _getActuatorBounds(self, actuator):
        variables = []
        lowerBounds = []
        upperBounds = []
        self.logger.debug("actuator %s", actuator)
        #get variables
        syms = self._getVarList(actuator)
        variables.extend(syms)
        #get bounds
        constraints = reduce(concat, actuator.getConstraints())
        self.logger.debug("constraints %s", constraints)
        if sp.flatten( constraints ) != []:
            conjuncts = sp.And.make_args( reduce( sp.And, sp.flatten( constraints ) ) )
        else:
            conjuncts = sp.And.make_args( True )
        for s in syms:
            lb, ub = self._getSymbolBounds(conjuncts, s)
            lowerBounds.append(lb)
            upperBounds.append(ub)
        return (variables, lowerBounds, upperBounds)

    def _getActuatorsBounds(self, actuators):
        variables = []
        lowerBounds = []
        upperBounds = []
        for act in actuators:
            vs, lbs, ubs = self._getActuatorBounds(act)
            variables.extend(vs)
            lowerBounds.extend(lbs)
            upperBounds.extend(ubs)
        return (variables, lowerBounds, upperBounds)

    def _getBoundsFor(self, graph, symbols):
        actuators = graph.getListOfActuators()
        variables, lowerBounds, upperBounds = self._getActuatorsBounds(actuators)
        zipped = zip(variables, lowerBounds, upperBounds)
        variables = []
        lowerBounds = []
        upperBounds = []
        for v, l, u in zipped:
            if v in symbols:
                variables.append(v)
                lowerBounds.append(l)
                upperBounds.append(u)
        self.logger.debug("variables: %s", variables)
        self.logger.debug("lower bounds: %s", lowerBounds)
        self.logger.debug("upper bounds: %s", upperBounds)
        return (variables, lowerBounds, upperBounds)

    def _getPaths(self, graph, effector):
        anchors = [a for a in graph.getListOfActuators(ActuatorTypes.Anchor) if a != effector]
        self.logger.debug("anchors: %s", ", ".join(x.getName() for x in anchors))
        paths = []
        for a in anchors:
            self.logger.debug("finding path from %s to %s:", a.getName(), effector.getName())
            newPaths = GraphDecomposition.find_all_paths(graph, a, effector)
            paths.extend(newPaths)
            for p in newPaths:
                self.logger.debug("    %s:", ", ".join(x.getName() for x in p))
        return paths

    def _computeObjectiveFunction(self, paths, target):
        self.logger.debug("computing effector objective function.")
        distances = []
        for p in paths:
            self.logger.debug("processing path %s", ",".join( a.getName() for a in p))
            Algorithms.calculateEndPositionForChain( p )
            endPosition = p[-1].getGlobalPositionMatrix()
            distances.append( ( target, endPosition.col(-1)[0:3] ) )
        return distances

    def _computeParallelStructPenaltyFunctions(self, graph, paths, effector):
        self.logger.debug("computing constraints due to parallel structure.")
        distances = []
        notAnchorOrEffector = [a for a in graph.getListOfActuators() if a.getType() != ActuatorTypes.Anchor and a != effector]
        for na in notAnchorOrEffector:
            pathToNa = [ p[:p.index(na)] for p in paths if p.count(na) > 0 ]
            if len(pathToNa) > 1:
                self.logger.debug("looking at %s paths to %s", len(pathToNa), na.getName())
                head = pathToNa.pop()
                Algorithms.calculateEndPositionForChain( head )
                self.logger.debug(" representative path %s", ", ".join( x.getName() for x in head))
                endPosition1 = head[-1].getGlobalPositionMatrix().col(-1)[0:3]
                self.logger.debug("endPosition1: %s", endPosition1)
                for other in pathToNa:
                    self.logger.debug(" other path %s", ", ".join( x.getName() for x in other))
                    Algorithms.calculateEndPositionForChain( other )
                    endPosition2 = other[-1].getGlobalPositionMatrix().col(-1)[0:3]
                    self.logger.debug("endPosition2: %s", endPosition2)
                    distances.append( ( endPosition1, endPosition2 ) )
        return distances
