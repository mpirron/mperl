"""
Simulated Annealing for ActuatorGraph Structures ..
"""

import _thread

from random import random

from math import sin, cos, pi, sqrt, exp


import ActuatorGraph.GeometricAlgorithms as GeometricAlgorithms
import ActuatorGraph.Algorithms as Algorithms

import ActuatorGraph.Actuators.Revolute as Revolute
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes

import numpy as np
import sympy as sp

norm = lambda v1: distanceBetweenPoints( sp.Matrix([0,0,0]), v1 )

def distanceBetweenPoints( v1, v2 ):
    """Calculates the euclidean distance between two points in 3D"""
    return sp.sqrt( (v1[0]-v2[0])**2+(v1[1]-v2[1])**2+(v1[2]-v2[2])**2 )

def getAngle( t ):
    u = sp.Matrix( [1,0,0] )
    return sp.acos( u.dot(t) / ( norm( u )*norm( t ) ) )

def getDirection( j, e, t ):
    """ """
    n = ( e[0]-j[0] )*( t[1]-j[1] ) - ( e[1]-j[1] )*( t[0]-j[0] )
    d = norm( e-j ) * norm( t-j )
    return sp.asin( n/d )

def generate_random_solution( tree, target ):
    # test: length of prismatic actuator is 1
    # and we know the target and origin position
    # lets assume 2d for a strt, so xy plane .

    angle = getAngle(target)

    u = random()*pi
    while u > angle:
        u = random()*pi

    return u


def run_sa( tree, target, tolerance ):

    revoluteActuators = tree.getListOfActuators( actuatorType = ActuatorTypes.Revolute )

    tentative_solution_vector = []

    for actuator in revoluteActuators:
        tentative_solution_v1 = generate_random_solution( tree, target )
        actuator.setAngle( tentative_solution_v1 )
        tentative_solution_vector.append( tentative_solution_v1 )
    Algorithms.calculateEndPosition( tree )
    tentative_solution = sp.Matrix(list(tree._dictionary.values())[-1][-1].getGlobalPositionMatrix().col(-1)[0:3])
    tentative_cost = distanceBetweenPoints( target, tentative_solution )

    k = 25    # 50 25
    t = 350   # 350 250

    s=0

    while t > 1e-5:
        neighbor_solution_vector = []

        for actuator in revoluteActuators:
            tentative_solution_v1 = generate_random_solution( tree, target )
            actuator.setAngle( tentative_solution_v1 )
            neighbor_solution_vector.append( tentative_solution_v1 )

        Algorithms.calculateEndPosition( tree )
        neighbor_solution = sp.Matrix(list(tree._dictionary.values())[-1][-1].getGlobalPositionMatrix().col(-1)[0:3])
        #neighbor_solution = tree[-1].getGlobalPositionMatrix().col(-1)[0:3]
        neighbor_cost = GeometricAlgorithms.distanceBetweenPoints( target, neighbor_solution )
        try:
            f = exp( -( tentative_cost-neighbor_cost ) / ( k*t ) )
        except:
            f = 0.5

        if tentative_cost > neighbor_cost:
            tentative_solution_vector = neighbor_solution_vector
            tentative_cost = neighbor_cost
            tentative_solution = neighbor_solution
            s += 1
        elif f < random():
            tentative_solution_vector = neighbor_solution_vector
            tentative_cost = neighbor_cost
            tentative_solution = neighbor_solution
        t *= 0.8

        if tentative_cost < tolerance:
            return tentative_cost

        print( "tentative_solution_vector = ", tentative_solution_vector)

    return tentative_cost

def solveInverseKinematics( tree, target, tolerance=0.05 ):
    tentative_cost = 10

    while tentative_cost > tolerance:
        tentative_cost = run_sa (tree, target, tolerance )
        print( "cost:", tentative_cost)

    Algorithms.calculateEndPosition( tree )
    #print( "Target", target, tree[-1].getGlobalPositionMatrix() )
    print( "Target", target, tree.endeffectors[-1].getGlobalPositionMatrix() )
    for actuator in tree.getListOfActuators():
        print( "\t", actuator )
        Algorithms.calculateEndPosition( tree )
    return tree.endeffectors[-1].getGlobalPositionMatrix()
