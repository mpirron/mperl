# Newton file for derived signals like area moment, flexural modulus,
# torque &c.


# TOC:

#param_acceleration       : acceleration       ,
#param_angle              : angle              ,
#param_angularAcceleration: angularAcceleration,
#param_angularDisplacement: angularDisplacement,
#param_angularImpulse     : angularImpulse     ,
#param_angularJerk        : angularJerk        ,
#param_angularMomentum    : angularMomentum    ,
#param_angularRate        : angularRate        ,
#param_angularVelocity    : angularVelocity    ,
#param_area               : area               ,
#param_charge             : charge             ,
#param_concentration      : concentration      ,
#param_current            : current            ,
#param_density            : density            ,
#param_distance           : distance           ,
#param_flexuralModulus    : flexuralModulus    ,
#param_force              : force              ,
#param_frequency          : frequency          ,
#param_frictionCoefficient: frictionCoefficient,
#param_friction           : friction           ,
#param_impulse            : impulse            ,
#param_jerk               : jerk               ,
#param_jounce             : jounce             ,
#param_magneticFluxDensity: magneticFluxDensity,
#param_mass               : mass               ,
#param_material           : material           ,
#param_momentOfForce      : momentOfForce      ,
#param_momentum           : momentum           ,
#param_power              : power              ,
#param_pressure           : pressure           ,
#param_relativeHumidity   : relativeHumidity   ,
#param_secondAreaMoment   : secondAreaMoment   ,
#param_temperature        : temperature        ,
#param_time               : time               ,
#param_torque             : torque             ,
#param_velocity           : velocity           ,
#param_volume             : volume             ,
#param_weight             : weight             ,
#param_work               : work


# Non derived signals

mass                : signal ={
	name		    = "mass" English;
	symbol		    = mass;
	derivation	    = none;
}

time                : signal = {
	name		    = "second" English;
	symbol		    = s;
	derivation	    = none;
}

distance            : signal = {
	name		    = "meter" English;
	symbol		    = m;
	derivation	    = none;
}

material            : signal = {
	name		    = "mole" English;
	symbol		    = mol;
	derivation	    = none;
}

temperature         : signal = {
	name		    = "Kelvin" English;
	symbol		    = K;
	derivation	    = none;
}

charge              : signal = {
	name		    = "Coulomb" English;
	symbol		    = C;
	derivation	    = none;
}

# Derived signals

velocity            : signal = {
    name            = "velocity" English;
    symbol          = velocity;
    derivation      = distance / time;
}

acceleration        : signal = {
    name            = "acceleration" English;
    symbol          = acceleration;
    derivation      = velocity / time;
}

force               : signal = {
	name		    = "force" English;
	symbol		    = force;
	derivation	    = mass * acceleration;
}

weight              : signal = {
    name            = "weight" English;
    symbol          = kilogram;
    derivation      = force;
}

impulse             : signal = {
    name            = "impulse" English;
    symbol          = impulse;
    derivation      = force * time;
}



angularDisplacement : signal = {
	name		    = "radian" English;
	symbol		    = rad;
	derivation	    = distance / distance;
}


angularVelocity     : signal = {
    name            = "angularVelocity" English;
    symbol          = angularVelocity;
    derivation      = angularDisplacement / time;
}

angularAcceleration : signal = {
    name            = "angularAcceleration" English;
    symbol          = angularAcceleration;
    derivation      = angularVelocity / time;
}

angularMomentum     : signal = {
    name            = "angularMomentum" English;
    symbol          = angularMomentum;
    derivation      = impulse * mass * angularDisplacement;
}

momentOfForce       : signal = {
    name            = "momentOfForce" English;
    symbol          = momentOfForce;
    derivation      = angularMomentum / time;
}

angularImpulse      : signal = {
    name            = "angularImpulse" English;
    symbol          = angularImpulse;
    derivation      = momentOfForce * time;
}



angularJerk         : signal = {
    name            = "angularJerk" English;
    symbol          = angularJerk;
    derivation      = angularAcceleration / time;
}

secondAreaMoment    : signal = {
    name            = "secondAreaMoment" English;
    symbol          = secondAreaMoment;
    derivation      = distance * distance * distance * distance;
}


flexuralModulus     : signal = {
    name            = "flexuralModulus" English;
    symbol          = flexuralModulus;
    derivation      = ( distance*distance*distance ) * force / ( secondAreaMoment*distance );
}

friction            : signal = {
    name            = "friction" English;
    symbol          = Friction;
    derivation      = force / force;
}

frictionCoefficient : signal = {
    name            = "frictionCoefficient" English;
    symbol          = m_friction;
    derivation      = friction / friction;
}


jerk                : signal = {
    name            = "jerk" English;
    symbol          = jerk;
    derivation      = angularVelocity / time;
}

jounce              : signal = {
    name            = "jounce" English;
    symbol          = jounce;
    derivation      = jerk / time;
}


momentum            : signal = {
    name            = "momentum" English;
    symbol          = momentum;
    derivation      = weight * distance / time;
}

torque              : signal = {
    name            = "torque" English;
    symbol          = torque;
    derivation      = distance * angularDisplacement * force;
}





# Additional signals

current             : signal = {
	name		    = "Ampere" English;
	symbol		    = A;
	derivation	    = charge / time;
}

angle               : signal = {
	name		    = "degree" English;
	symbol		    = deg;
	derivation	    = distance / distance;
}


work                : signal = {
	name		    = "Joule" English;
	symbol		    = J;
	derivation	    = force * distance;
}

power               : signal = {
	name		    = "Watt" English;
	symbol		    = W;
	derivation	    = work / time;
}

magneticFluxDensity : signal = {
	name		    = "Tesla" English;
	symbol		    = T;
	derivation	    = mass / (charge * time);
}

angularRate         : signal = {
	name		    = "anglejiffy" English;
	symbol		    = ajf;
	derivation	    = angle / time;
}

frequency           : signal = {
	name		    = "Hertz" English;
	symbol		    = Hz;
	derivation	    = 1 / time;
}

area                : signal = {
	name		    = "square" English;
	symbol		    = sq;
	derivation	    = distance**2;
}

density             : signal = {
	name		    = "rho" English;
	symbol		    = rho;
	derivation	    = mass/(distance**3);
}

volume              : signal = {
	name		    = "vol" English;
	symbol		    = vol;
	derivation	    = distance**3;
}

pressure            : signal = {
	name		    = "Pascal" English;
	symbol		    = Pa;
	derivation	    = force / area;
}

concentration       : signal = {
	name		    = "concentration" English;
	symbol		    = ppm;
	derivation	    = dimensionless;
}

relativeHumidity    : signal = {
    name		    = "Relative Humidity" English;
    symbol		    = RH;
    derivation	    = dimensionless;
}

BeltFeed : signal = {
    name        =   "Feed of Belt" English;
    symbol      =   lpr;
    derivation  =   angularDisplacement * distance;
}


# Constants

kNewtonUnitfree_pi				            : constant = 3.1415926535897932384626433832795;
kNewtonUnithave_SpeedOfLight			    : constant = 299792458 (meter * (second ** -1));
kNewtonUnithave_AccelerationDueToGravity	: constant = (196133 / 20000) (meter * (second ** -2));
kNewtonUnithave_GravitationalConstant		: constant = 6.67428E-11 ((meter ** 3) * (kilogram ** -1) * (second ** -2));
kNewtonUnithave_AvogadroConstant		    : constant = 6.02214E23 mole;
kNewtonUnithave_BoltzmannConstant		    : constant = 1.38065E-23 (Joule / Kelvin);
kNewtonUnithave_ElectronCharge			    : constant = 1.60218E-19 Coulomb;
kNewtonUnithave_SpeedOfSound			    : constant = 340.292 (meter * (second ** -1));
kNewtonUnithave_EarthMass			        : constant = 5.9742E24 kilogram;
