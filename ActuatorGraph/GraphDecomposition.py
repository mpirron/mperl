"""
Decompose graph structures into serial chains.
"""




#bug!
#def __find_all_paths__(graph, start, end, current_path=[]):
#    print( "graph >>>", type(graph) )
#    print( "start >>>", type(start) )
#    print( "end >>>", type(end) )
#    print( "curr_path >>>", type(current_path) )
#    print( ">>>", start==end )
#
#    if type( graph ) != dict:
#        return __find_all_paths__( graph._dictionary, start, end )
#    else:
#        current_path = current_path + [start]
#        if start == end:
#            print("s=e>>>>", current_path )
#            return [current_path]
#
#        if start not in graph:
#            return []
#
#        paths = []  # This holds a list of serial paths
#
#        if type(graph[start]) == list:
#            for node in graph[start]:
#                if node not in current_path:
#                    newpaths = __find_all_paths__(graph, node, end, current_path)
#                    print( "<<<", node, end, newpaths )
#                    for newpath in newpaths:
#                        if type( newpath ) == list:
#                            paths.append(newpath)
#        else:
#            node = graph[start]
#            if node not in current_path:
#                newpaths = __find_all_paths__(graph, node, end, current_path)
#                for newpath in newpaths:
#                    paths.append(newpath)


def __find_all_paths__(actuatorgraph, start_vertex, end_vertex, path=[]):
    graph = actuatorgraph._dictionary
    path = path + [start_vertex]
    if start_vertex == end_vertex:
        return [path]
    if start_vertex not in graph:
        return []
    paths = []
    for vertex in graph[start_vertex]:
        if vertex not in path:
            extended_paths = __find_all_paths__(actuatorgraph,vertex, end_vertex, path)
            for p in extended_paths:
                paths.append(p)
    return paths

def find_all_paths( graph, start, end ):
    """
    Given a parallel ActuatorGraph structure, return a list of serial chains.
    :param graph: An ActuatorGraph Structure
    :param start: A root node
    :param end: A leaf node
    :param current_path:
    :return: A list of ActuatorGraph structures holding serial chains
    """

    paths = __find_all_paths__( graph, start, end )
    #print( "Paths=", paths )
    #assert( len(paths)>0 )

    res = []  # Now lets convert those serial paths in ActuatorGraph structures
    #print( "Available paths", paths )

    import ActuatorGraph.Tools as Tools
    for p in paths:
        #print("Type of path", type(p))
        #print("p=",p)
        #for e in p:
        #    print(e)
        res.append( Tools.makeActuatorGraph(p))
    return res


def getLinkages( graph, start, end ):
    pass
    listOfLinkages = []
    paths = find_all_paths( graph, start, end )
    linkages = []
    for path in paths:
        import ActuatorGraph.Tools as Tools
        listOfLinkages.append( Tools.makeChain( path ) )
    return listOfLinkages




#def find_common_elements( listOfActuatorGraph ):
#    """
#    Given a list of actuatorgraphs, search for common elements and return a list of those
#    """
#
#    l = list( map( set, list_of_paths))
#    get_common_elements = lambda l1, l2: l1-(l1-l2)
#
#    print("common elements")
#    for x, y in zip(l[:-1], l[1:]):
#        g=get_common_elements( x,y )
#        #print( [e.name for e in g])
#    return list(g)

#def add_common_constraints( list_of_common_elements ):
#    """
#    Add constraints for common elements
#    """
#    pass

