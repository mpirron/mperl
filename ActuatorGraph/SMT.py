"""
Use a SMT solver to calculate kinematics and properties of ActuatorTree /
ActuatorGraph structures. Uses Z3 solver and the respective python bindings, but
can also print out generic smt lib v2 code.
"""
from numpy.ma import var

import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes

import ActuatorGraph.Tools as Tools
import ActuatorGraph.Algorithms as Algorithms

import ActuatorGraph.Utils.Z3Converter as Z3Converter

import z3

import re

import sympy
from Utils.DrealInterface import DrealInterface

# Not clean ..

def solveForwardKinematics( tree, setOfInputParameters ):
    """
    Solves the forward kinematic problem with help of the smt solver.
    Recommended, as it circumvents the problems inherit to numerical /
    approximative solutions like CCD, like being stuck in local optima.

    For now, setOfInputParameters is a list of tuples with the values for each
    joint. Is there a better representation? dicts e.g.?

    """
    
    listOfActuators = tree.getListOfActuators()
    i = 0
    for actuator in listOfActuators:        # Set values for each actuator
        #if actuator.getType() == ActuatorTypes.Revolute: actuator.setAngle( *setOfInputParameters[i] )
        #if actuator.getType() == ActuatorTypes.Revolute: actuator.setAngle( setOfInputParameters )
        #if actuator.getType() == ActuatorTypes.Prismatic: actuator.setLength( setOfInputParameters )
        i += 1
    
    #Tools.printTree( tree )
    return  Algorithms.calculateEndPosition( tree )
    
def getConstraints( tree ):
    """Return z3 assertions from actuatortree constraints"""


def getSolver( tree, targetPosition, tol=0.1 ):
    solver = Z3Converter.getSolverFromTree( tree, targetPosition, tol )

    # Add additional constraints from tree
    # todo: filter true stmt from getCOnstraint
    
    #print( "constra" )
    #print( tree.getConstraints( tree ) )
    #c = str(tree.getConstraints( tree))
    # 
    #print(c)
    #c = re.sub( r'([<>=])=', r'\1', c )
    #c = c.split( "&" )
    
    #print("Get Constraints from tree" )
    #constraintsSolver = []
    #for e in c:
    #    if e != "True":
    #        print( e.strip(" ,(,)"), type(e) )
    #    constraintsSolver.append( Z3Converter.getSolverFromExpression( e.strip(" ,(,)"), debug=False ) )

    #solver = Z3Converter.unifySolver(  constraintsSolver+[solver] )

    return solver


def solveInverseKinematics( actuatorgraph, targetPosition, tol=0.1, solver = "z3"):
    """
    Solves the inverse kinematic problem with help of the smt solver.
    Recommended, as it circumvents the problems inherit to numerical /
    approximative solutions like CCD, like being stuck in local optima.
    
    First version: Do positioning, but no orientation

    z3 has no support for trigonometric functions. 
    """
    if solver == "z3":
        solver = getSolver( actuatorgraph, targetPosition, tol )
        #print( type( solver ) )
        #print( "Use assertions:")
        #for assertion in solver.assertions():
        #    print( "\t%s" %(assertion) )

        if solver.check() == z3.sat:
            print( "Found Solution")
            print( solver.model() )
            return( solver )
        else:
            print( "No Solution found: %s %s" %( solver.check(), solver.reason_unknown() ) )
            print( "Removing assertions" )
            while solver.check() != z3.sat:
                solver.pop();print(solver); print( "Check: %s - %s" %(solver.check(), solver.reason_unknown() ) )
                print("Removed Assertion")
            return solver
    elif solver == "dreal":
        constraints = []
        Algorithms.calculateEndPosition( actuatorgraph )
        for linkage in actuatorgraph.linkages:

            actuators = linkage

            #endPosition = actuatorgraph.linkages[-1][-1].getGlobalPositionMatrix()
            endPosition = linkage.endeffectors[-1].globalpositionmatrix   #bug
            #endPosition = actuatorgraph.endeffectors[0]

            for e, t in zip(endPosition.col(-1)[0:3], targetPosition):
                constraints.append(sympy.Eq(e, t))

            for act in actuators.getListOfActuators():
                for cstrs in act.getConstraints():
                    constraints.extend(cstrs)
            #print( "Calling solver" )
            solver = DrealInterface( tol, debug=False )
            result, intervals = solver.run(constraints)
            #print( "->", result, intervals )
            if result:
                return solver.approximateModel(intervals)
            else:
                raise Exception("dreal returned unsat")
    else:
         raise Exception("unknown solver " + solver)


def mapSolutions( actuatorTree, model ):
    """ Given a model from the z3 solver, map the solutions to the resp. actuators / joints """
    for actuator in actuatorTree.getListOfActuators():
        
        if actuator.getType() == ActuatorTypes.Prismatic:
            variable = actuator.__getSymbols__()
            val = model[z3.Real( str( variable ) ) ]
            if val != None: actuator.setLength( float(val.as_decimal(10) ) )
            #print( "with %s, map variable %s to solution %s with value %s" %( actuator, actuator.__getSymbols__(), variable, model[z3.Real(str(variable))]  ) )
        elif actuator.getType() == ActuatorTypes.Revolute:
            variable = actuator.__getSymbols__()
            val = model[z3.Real( str( variable ) ) ]
            if val != None: actuator.setAngle( float(val.as_decimal(10) ) )
            #print( "with %s, map variable %s to solution %s with value %s" %( actuator, actuator.__getSymbols__(), variable, model[z3.Real(str(variable))]  ) )
        elif actuator.getType() == ActuatorTypes.Spherical:
            actuator.thetaX = float( model[ z3.Real( str( variable[0] ) ) ].as_decimal() )
            actuator.thetaY = float( model[ z3.Real( str( variable[1] ) ) ].as_decimal() )
            actuator.thetaZ = float( model[ z3.Real( str( variable[2] ) ) ].as_decimal() )
            #print( "with %s, map variable %s to solution %s with value %s" %( actuator, actuator.__getSymbols__(), variable, model[z3.Real(str(variable))]  ) )

        else:
            variable = actuator.__getSymbols__()
        


def generateSMTCode( actuatorTree, endPosition, jacobian=True ):
    """ Generate smtlib v2 code for use in other programs"""
    code = []

    # Getting preliminaries, declaration of variables, constraints
    for actuator in actuatorTree.getListOfActuators():
        if type( actuator.__getSymbols__() ) is tuple:
            for s in actuator.__getSymbols__():
                code.append( "( declare-const %s Real )" %( s ) )
        else:
            code.append( "( declare-const %s Real )" %( actuator.__getSymbols__() ) )
        
        if not actuator.getConstraints() == "True":
            constraint = map( str, actuator.getConstraints().args )
            for c in constraint: 
                c = c.split( ' ' )
                code.append( "( assert ( %s ( %s ) ( %s ) ) )" %( c[1], c[0], c[2] ) )

    # Main Equations -> convert to infix!
    if ( jacobian == False ):
        formula = actuatorTree.calculateEndPosition( actuatorTree )
        code.append( "( assert = %s ( %s ) )" %( endPosition[0], formula.col(-1)[0] ) )
        code.append( "( assert = %s ( %s ) )" %( endPosition[1], formula.col(-1)[0] ) )
        code.append( "( assert = %s ( %s ) )" %( endPosition[2], formula.col(-1)[0] ) )
    else:
        M = actuatorTree.calculateEndPosition( actuatorTree ).col(-1)
        #J = J[ 0:len(J.col(-1))-1 ] # number of actuated parameters
        n = len( actuatorTree.getListOfVariables( actuated=True ) )
        M.row_del(-1)    
        if M.row(-1) == sp.Matrix( [0] ): M.row_del(-1)
        J=M.jacobian( sp.flatten( actuatorTree.getListOfVariables( actuated=True ) ) )
        for line in J:
            code.append("( assert( != 0 ( %s ) ) " %( line ) )

    # values, get model &c. 
    code.append( "( check-sat-using-gfnra-nlsat )" )
    for actuator in actuatorTree.getListOfActuators():
        if type( actuator.__getSymbols__() ) is tuple:
            for s in actuator.__getSymbols__():
                code.append( "( get_value( %s ) )" %( s ) )

        else:
            code.append( "( get_value( %s ) )" %( actuator.__getSymbols__() ) )

    code.append( "( get-model )" )

    print('\n'.join(code))
