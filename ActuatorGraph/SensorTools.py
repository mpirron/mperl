from ActuatorGraph import Algorithms
from ActuatorGraph import GraphDecomposition

from ActuatorGraph.ActuatorTree import *
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes
from ActuatorGraph.Sensors import SensorTypes
from ActuatorGraph.Utils.NameGenerator import NameGenerator


def get_associated_sensors(self, actuatorgraph, sensorgraph):
    '''
    Return a dict, actuator as key, sensors as values
    '''
    assert( len(actuatorgraph) == len(sensorgraph) )
    actuators = list( actuatorgraph )
    sensors = list( sensorgraph )
    res = {}
    t = []

    for i in range( 0, len(actuators) ):
        t = [sensors[i]]
        for sensor in sensors[:i]:
            t.append( sensor )
        t = list(filter( lambda a: a.type != SensorTypes.Null, t ))
        res[ actuators[i] ] = t

    return res


def get_list_of_affected_actuators(self, actuatorgraph, sensorgraph, sensor):
    '''
    Return a list of actuators which is affected by a an out-of-spec reading
    '''
    d = self.get_associated_sensors( actuatorgraph, sensorgraph )
    res=[]
    for k, v in d.items():
        if sensor in v:
            res.append(k)
    return res


def get_solution(self):
    '''
    Find a solution to the problem
    '''
    pass
