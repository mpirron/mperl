import sympy as sp

from ActuatorGraph.Sensors.__SensorPrimitive import SensorPrimitive
from ActuatorGraph.Sensors.SensorTypes import SensorTypes
from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Sources import UART, Loop

import ActuatorGraph.Units.__si_units as si_units

import logging


class Current( SensorPrimitive, object ):
    def __init__( self, name=NameGenerator.next("Flex"), coordinates=sp.Matrix( [0,0,0] ) ):
        self.name = name
        t = sp.symbols( str( self.name ) + "_A", real=True )

        self.coordinates = coordinates
        self.PreConstraints = []
        self.type = SensorTypes.Electrical
        self.positionMatrix = sp.eye(4)
        self.parent_orientation = sp.eye(4)

        self.invariants = []
        self.preconstraints = []
        self.postconstraints = []
        self.constraints = [ self.invariants, self.preconstraints, self.postconstraints ]

        self.globalpositionmatrix = sp.eye(4)
        self.log = logging.getLogger( __name__ )
        self.mutable = True
        self.offsetAngle=sp.eye(4)

        #self.connection = UART.UART()
        #self.connection.setRange( 16910.55390, 28261.69650, 0, 90, -113.3615)
        self.connection = Loop.Loop()

        self.endeffector = False

        self.unit = si_units.ampere
        self.dimension = self.unit.dimension_symbol


        self.mutableProperties = []

        for e in self.__dir__():
            if e.startswith( "set" ):
                self.mutableProperties.append(e)

    def __str__( self ):
        return "Current %s" %( self.name)

    def __getSymbols__( self ):
        t = sp.symbols( str( self.name ) + "_A", real=True )
        return ( t )

    def __setSymbols__( self, arg ):
        self.coordinates = arg

    def __getSubstitution__( self ):
        '''
        Substitution with values read from sensor
        '''
        t = self.__getSymbols__()
        values = []
        values.append( (t, self.connection.readSensor() ) )
        return values

    def getName( self ):
        return self.name

    def getCoordinates( self ):
        if self.coordinates != None:
            #print( self.coordinates)
            x,y,z = self.coordinates
        else:
            x,y,z = self.__getSymbols__()
        #M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        M = sp.Matrix( [x,y,z])
        return M

    def getPositionMatrix( self ):
        x,y,z = self.coordinates
        M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        return M

    def getConfigurationMatrix( self ):
        return sp.eye(4)

    def setPositionMatrix( self, matrix ):
        self.positionMatrix = matrix

    def getType( self ):
        return self.type

    def setGlobalPositionMatrix( self, matrix ):
        assert( matrix.shape == (4,4))
        self.globalpositionmatrix = matrix


