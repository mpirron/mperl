from enum import Enum, auto

class SensorTypes( Enum ):
    """
    Determines the type of sensors.
    """
    SensorPrimitive     = "SensorPrimitive"
    Null                = "Null"
    Mechanical          = "Mechanical"
    Electrical          = "Electric"
    Pneumatical         = "Pneumatic"


    def __str__( self ):
        return self.value
