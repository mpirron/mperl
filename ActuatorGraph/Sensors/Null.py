import sympy as sp

from ActuatorGraph.Sensors.__SensorPrimitive import SensorPrimitive
from ActuatorGraph.Sensors.SensorTypes import SensorTypes
from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Sources import UART, Loop
import logging

import ActuatorGraph.Units.__si_units as si_units

class Null( SensorPrimitive, object ):
    def __init__( self, name=NameGenerator.next("Null"), coordinates=sp.Matrix( [0,0,0] ) ):
        self.name = name
        t = sp.symbols( str( self.name ) + "_t", real=True )

        self.coordinates = coordinates
        self.PreConstraints = []
        self.type = SensorTypes.Null
        self.positionMatrix = sp.eye(4)
        self.parent_orientation = sp.eye(4)

        self.invariants = []
        self.preconstraints = []
        self.postconstraints = []
        self.constraints = [ self.invariants, self.preconstraints, self.postconstraints ]

        self.globalpositionmatrix = self.getGlobalEndCoordinates()
        self.log = logging.getLogger( __name__ )
        self.mutable = False
        self.offsetAngle=sp.eye(4)

        self.connection = None

        self.unit = si_units.dimensionless
        self.dimension = self.unit.dimension_symbol

        self.mutableProperties = []

        for e in self.__dir__():
            if e.startswith( "set" ):
                self.mutableProperties.append(e)

    def __str__( self ):
        return "Null %s "%( self.name )

    def __getParameters__( self ):
        return 0

    def __getSymbols__( self ):
        return ()


    def __getSubstitution__( self ):
        '''
        Substitution with values read from sensor
        '''
        return []


    def __setParentOrientation__( self, orientation ):
        self.parent_orientation = orientation

    def getName( self ):
        return self.name

    def setCoordinates( self, coordinates ):
        self.coordinates = coordinates
        self.globalpositionmatrix = self.getGlobalEndCoordinates()

    def getGlobalEndCoordinates( self ):
        return self.parent_orientation*self.getPositionMatrix()

    def getCoordinates( self ):
        if self.coordinates != None:
            #print( self.coordinates)
            x,y,z = self.coordinates
        else:
            x,y,z = self.__getSymbols__()
        #M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        M = sp.Matrix( [x,y,z])
        return M


    def getPositionMatrix( self ):
        x,y,z = self.coordinates
        M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        assert( M.shape == (4,4))
        return M

    def getConfigurationMatrix( self ):
        M = sp.eye(4)
        return M


    def setPositionMatrix( self, matrix ):
        self.positionMatrix = matrix


    def getType( self ):
        return self.type



