"""
Adaptive Grid

We want a fast and efficient check for singularities and local lipschitz
continuity.
"""

import sympy as sp
import json

import matplotlib
import matplotlib.pyplot as plt

import ActuatorGraph.LeastSquare as LS
import ActuatorGraph.SMT as SMT

import matplotlib.pyplot as plt


# Use the Froebeniusdistance -> can be calculated by spur(StS)
# =sum of singular values
from ActuatorGraph import Algorithms

class adaptive_grid():
    """
    This class holds the adaptive grid and the lookup table for currently known configurations. This is used to quickly
    get the configurations for known points or to quickly generate additional constraints if the position is not known
    before.
    The grid is refinable; sampling strategies supported as of yet are equidistant, uniform sampling and halton sequence.
    """
    def __init__( self ):
        self.grid = []                          # holds current grid
        self.lookup_table = {}                  # holds (known point, configuration)
        self.grid_pos = 0
        self.grid_sampled = False
        self.origin = (0,0,0)
        self.boundary = 10                      # For now: sphere / circle with radius 10 as bounding box
        self.dimensions = 2
        self.robotic_system = None
        self.endeffector = None
        self.actuators = []
        self.jacobian = None



    def __make_grid_2D__( self, n, number_sequence ):
        '''
        :param p
        :param n
        :param number_sequence:
        :return:
        '''

        sequence = [ i for i in number_sequence( self.dimensions, n) ]

        x0 = self.origin[0]
        y0 = self.origin[1]

        grid = []
        for i in range(self.grid_pos, n):
            t0 = sequence[i][0]*self.boundary
            t1 = sequence[i][1]*self.boundary
            assert( t0 <= 10)
            assert( t1 <= 10)
            grid.append((x0 +         t0, y0 +        t1))
            grid.append((x0 +  (-1) * t0, y0 + (-1) * t1))
            grid.append((x0 +         t0, y0 + (-1) * t1))
            grid.append((x0 +  (-1) * t0, y0 +        t1))
        self.grid_pos = n
        return grid


    def make_grid( self ):
        '''
        Make an initial grid with 20 samples
        :return:
        '''
        self.grid = self.__make_grid_2D__( 20, Algorithms.halton_sequence )
        self.grid_sampled = False

#    def sample_grid( self ):
#        '''
#        '''
#        assert(self.robotic_system is not None)
#        if not self.grid_sampled:
#            for p in self.grid:
#                if p not in self.lookup_table:
#                    print( "Now probing ", p )
#                    # Get configuration
#                    try:
#                        conf = SMT.solveInverseKinematics(self.robotic_system, p, tol=1e-2, solver="dreal")
#                        if conf:
#                            # here: apply configuration
#                            M = Algorithms.calculateEndPosition( self.robotic_system )
#                            A = J.subs( conf )
#                            sv = get_singular_values( A )
#                    except:
#                        conf = None
#                        sv = None
#                    print(" Found configuration:", conf, sv )
#                    self.lookup_table[ p ] = ( conf, sv )
#        self.grid_sampled = True

    def __get_jacobian__(self):
        symbols = []
        for actuator in self.actuators:
            symbols.append(actuator.__getSymbols__())
        Algorithms.calculateEndPosition( self.robotic_system )
        M = sp.Matrix(self.endeffector.globalpositionmatrix.col(-1)[0:3])
        J = M.jacobian( symbols )
        self.jacobian = J
        return J


    def sample_grid_FK( self ):
        '''
        '''
        self.grid = []
        assert(self.robotic_system is not None)
        if not self.grid_sampled:
            seq = [ i for i in Algorithms.halton_sequence(len(self.actuators), 50) ]    # initial 20 configs
            for c in seq:
                conf = []
                symb = []
                i = 0
                for actuator in self.actuators:
                    conf.append( sp.N(c[i]*sp.pi) )
                    symb.append( actuator.__getSymbols__())
                    actuator.setAngle( c[i]*sp.pi)
                    i += 1
                M = Algorithms.calculateEndPosition( self.robotic_system )
                p = self.endeffector.globalpositionmatrix.col(-1)[0:2]

                subs = list( zip( symb, conf) )
                J = sp.N(self.jacobian.subs( subs) )
                sv = get_singular_values( J )
                self.lookup_table[ tuple(p) ] = conf, min(sv[0])
                self.grid.append( tuple(p[0:2]) )
        self.grid_sampled = True


    def __plot_grid__( self ):
        print( self.grid )
        plt.scatter( *zip(*self.grid)  )
        plt.show()


    def __print_lookup_table__( self ):
        print("Point \t\t\t\t\t\t\t\t Configuration")
        for k,v in self.lookup_table.items():
            print( k, "\t\t\t", v )

    def __save_lookup_table__( self ):
        j = json.dumps( self.lookup_table )
        f = open( self.robotic_system.name+"_lookup", "w" )
        f.write( j )
        f.close()
        j = json.dumps( self.grid )
        f = open( self.robotic_system.name+"_grid", "w" )
        f.write( j )
        f.close()


def froebenius_distance( A ):
    sv = get_singular_values(A)[0]
    return sum( map( lambda x: x**2, sv))


# p: some point h: width of grid, n: number of points
def get_equidistant_grid_2D( p, h, n ):
    """
    Make an equidistant grid in 2D space.
    Arguments are:
        p: origin
        h: width of grid (used for x and y direction)
        n: number of points
    Returns:
        nested list of coordinates
    """
    x0 = p[0]
    y0 = p[1]
    grid=set()
    for i in range( 0, n ):
        for j in range( 0,n ):
            grid.add( (x0+i*h, y0+j*h, 0) )
            grid.add( (x0+i*h, y0-j*h, 0) )
            grid.add( (x0-i*h, y0-j*h, 0) )
            grid.add( (x0-i*h, y0+j*h, 0) )
    return grid


def get_equidistant_grid_3D( p, h, n ):
    """
    Make an equidistant grid in 3D space.
    Arguments are:
        p: origin
        h: width of grid (used for x and y direction)
        n: number of points
    Returns:
        nested list of coordinates
    """
    x0 = p[0]
    y0 = p[1]
    z0 = p[2]
    grid=set()
    for i in range( 0, n ):
        for j in range( 0,n ):
            for k in range( 0, n ):
                grid.add( (x0+i*h, y0+j*h, z0+j*h) )
                grid.add( (x0+i*h, y0+j*h, z0-j*h) )

                grid.add( (x0+i*h, y0-j*h, z0+j*h) )
                grid.add( (x0+i*h, y0-j*h, z0-j*h) )

                grid.add( (x0-i*h, y0-j*h, z0+j*h) )
                grid.add( (x0-i*h, y0+j*h, z0-j*h) )
    return grid


    
def get_singular_values( A ):
    """
    Calculate the singular value of a matrix using eigenvalues.
    Returns:
        tuple of eigenvalues
    """
    AAt = A.multiply( sp.transpose(A) )  # EV -> Columns of S
    tAA = sp.transpose(A).multiply( A )  # EV -> Rows of S
    
    l1 = list( map( sp.sqrt,list(tAA.eigenvals() ) ) )
    l2 = list( map( sp.sqrt,list(AAt.eigenvals() ) ) )
    try:
        l1 = list( map( float, l1))
    except:
        l1=[-1]
    try:
        l2 = list( map( float, l2))
    except:
        l2 = [-1]
    l1.sort()
    l2.sort()
    return (l1,l2)


def get_singular_matrix( A ):
    """Calculate singular matrix"""
    sv = get_singular_values( A )
    S = [ [0 for i in range( len(sv[0]) ) ] for j in range( len(sv[1]) ) ]
    for i in range( len(sv[0]) ):                    
                S[i][i] = sv[0][i]                    
    return sp.Matrix(S)

    
def closeness_to_singularity( grid, coordinate ):
    """
    Return distance of a coordinate to the nearest singularity
    """
    pass


def lipschitz_constant( p0, p1 ):
    """
    """
    pass
