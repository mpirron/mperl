from ActuatorGraph.Constraints import __ConstraintPrimitive
from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Constraints.ConstraintTypes import ConstraintProperty

class Constraint( __ConstraintPrimitive ):
    def __init__( self, name=NameGenerator.next( "ConstraintPrimitive" ), constraint=None, category=ConstraintProperty.Invariant, debug=True ):
        super().__init__( name, constraint, category, debug )
