from enum import Enum, auto

class ConstraintTypes( Enum ):
    """
    Determines the type of constraints.
    Geometric: Denotes geometric constraints on actuators, e.g. position, orientation &c., therefore forming the
    structural description of the system.
    Logic: Describes if some nodes of the graph can only be executed / actuated if certain conditions on other nodes
    hold.
    Wiring: Check if each module is wired correctly (e.g. power, messaging)
    """
    ConstraintPrimitive = "ConstraintPrimitive"
    Geometric           = "Geometric"
    Logic               = "Logic"
    Wiring              = "Wiring"

    def __str__( self ):
        return self.value

class ConstraintProperty( Enum ):
    Preconstraint       = "Preconstraint"
    Postconstraint      = "Postconstraint"
    Invariant           = "Invariant"

    def __str__( self ):
        return self.value
