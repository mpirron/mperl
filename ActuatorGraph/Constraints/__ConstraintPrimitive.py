"""
Base class of a ConstraintPrimitive.

"""

from time import time
import sympy as sp

from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Constraints.ConstraintTypes import ConstraintTypes
from ActuatorGraph.Constraints.ConstraintTypes import ConstraintProperty



import logging



class ConstraintPrimitive( object ):
    def __init__( self, name=NameGenerator.next( "ConstraintPrimitive" ), constraint=None, category=ConstraintProperty.Invariant, debug=True ):
        self.name = name

        self.constraint = constraint
        self.type = ConstraintTypes.ConstraintPrimitive
        self.category = category
        self.isrelaxable = False

        # log to the console
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        ch = logging.StreamHandler()
        if debug:
            ch.setLevel(logging.DEBUG)
        else:
            ch.setLevel(logging.INFO)
        formatter = logging.Formatter('[%(name)s] %(levelname)s: %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)


    def __str__( self ):
        return "ConstraintPrimitive %s: %s, %s" %( self.name, self.constraint, self.category )


    def __repr__(self):
        return self.constraint


    def __checkConstraints__( self, parameter ):
        pass


    def getName( self ):
        return self.name


    def getType( self ):
        return self.type


    def getParameters( self ):
        return "Not yet implemented"


    def getConstraints( self ):
        return self.constraint


    def isRelaxable( self ):
        return self.isrelaxable


    def setRelaxable( self, relaxable ):
        '''Denotes if the constraint can be relaxed'''
        self.isrelaxable = relaxable
