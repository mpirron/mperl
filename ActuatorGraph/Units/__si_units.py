'''
Small library to check for correct units when calculating physics

Sources:
https://physics.nist.gov/cuu/Units/checklist.html
https://en.wikipedia.org/wiki/International_System_of_Units
'''


from enum import Enum, auto
import math
import re
import sympy as sp


class UnitsAssertions:
    def assertUnitsMatch(self, one, other):
        if one.unit_symbol != other.unit_symbol:
            raise UnitMismatch

    def assertQuantityMatch(self, one, other):
        if one.quantity_name != other.quantity_name:
            raise QuantityMismatch


    def assertEqualValue(self, one, other, tolerance=10e-4, matchUnits=True):
        if matchUnits:
            self.assertUnitsMatch(one, other)
        n = float(one)
        m = float(other)
        if abs(n-m)>tolerance:
            raise ValueNotEqual


class UnitsException( Exception ):
    def __str__(self):
        return "General Unit Exception"


class UnitMismatch( UnitsException ):
    def __str__( self ):
        return repr( "Units do not match" )


class UnitArgumentMismatch( UnitsException ):
    def __str__(self):
        return "Argument can not have a unit"


class QuantityMismatch( UnitsException ):
    def __str__(self):
        return "Quantities do not match"

class VectorExpected( UnitsException ):
    def __str__(self):
        return "Vector expected"

class ValueNotEqual( UnitsException ):
    def __str__(self):
        return "Values not equal within tolerance"

class Unit( object ):
    def __init__( self, value, unit_symbol, quantity=None, name=None, base_unit=None, other_si_unit=None, dimension_symbol=None, quantity_name=None, derived_unit=False, in_si_base_units=None ):
        self.name               = name
        self.quantity           = quantity
        self.base_unit          = base_unit
        self.other_si_unit      = other_si_unit
        self.value              = value
        self.dimension_symbol   = dimension_symbol
        self.quantity_name      = quantity_name
        self.derived_unit       = derived_unit
        self.in_si_base_units   = in_si_base_units
        self.unit_symbol        = unit_symbol


    def __str__(self):
        #return str(self.value)
        return str(self.value)+str("")+str(self.unit_symbol)

    def __float__(self):
        return float(self.value)

    def __int__(self):
        return int(math.floor( self.value ))

    def __repr__(self):
        return self
        #return self.value
        #return str(self.value)+str("")+str(self.unit_symbol)


    def __add__(self, other):
        if str(type(other)) == "<class 'ActuatorGraph.Units.__si_units.Unit'>":
            if self.unit_symbol == other.unit_symbol:
               n = Unit( self.value+other.value, self.unit_symbol )
            else:
                raise UnitMismatch()
            return n
        else:
            return other+self.value


    def __radd__(self, other):
        return self.__add__(other)


    def __sub__(self, other):
        if type(other)== float or type(other)==int:
            n = Unit( self.value-other, self.unit_symbol )
            return n
        if self.unit_symbol == other.unit_symbol:
            n = Unit( self.value-other.value, self.unit_symbol )
            return n
        else:
            raise UnitMismatch()

    def __rsub__(self, other):
        return self.__sub__(other)


    def __mul__(self, other):
        if str(type(other)) == "<class 'ActuatorGraph.Units.__si_units.Unit'>":
            if other.unit_symbol == "":
                n = Unit( self.value*other.value, self.unit_symbol)
            elif self.unit_symbol == "":
                n = Unit( self.value*other.value, other.unit_symbol)
            else:
                n = Unit( self.value*other.value, self.unit_symbol+"·"+other.unit_symbol)
        else:
            n = Unit( self.value*other, self.unit_symbol)
        return n


    def __rmul__(self, other):
        return self.__mul__(other)


    def __truediv__(self, other):
        if str(type(other)) == "<class 'ActuatorGraph.Units.__si_units.Unit'>":
            if other.unit_symbol == "":
                unit = str(self.unit_symbol)
            elif self.unit_symbol == "":
                unit = str(1)+"/"+str(other.unit_symbol)
            else:
                unit = str(self.unit_symbol)+"/"+str(other.unit_symbol)
            n = Unit( self.value/other.value, unit)
        else:
            unit = str(self.unit_symbol)
            n = Unit( self.value/other, unit)
        return n


    def __rtruediv__(self, other):
        if str(type(other)) == "<class 'ActuatorGraph.Units.__si_units.Unit'>":
            unit = str(self.unit_symbol)+"/"+str(other.unit_symbol)
            n = Unit( other.value/self.value, unit)
        else:
            unit = str("1/")+str(self.unit_symbol)
            n = Unit( other/self.value, unit)
        return n


    def __floordiv__(self, other):
        return math.floor(self.__truediv__(other))


    def __rfloordiv__(self, other):
        return math.floor(self.__rtruediv__(other))


    def __mod__(self, other):
        if type(other) == int or type(other) == float:
            return Unit( self.value%other, self.unit_symbol )
        else:
            raise UnitArgumentMismatch


    def __rmod__(self, other):
        raise Exception


    def __divmod__(self, other):
        if type(other) == int or type(other) == float:
            return Unit( divmod(self.value,other), self.unit_symbol )
        else:
            raise UnitArgumentMismatch


    def __rdivmod__(self, other):
        raise UnitArgumentMismatch


    def __pow__(self, power, modulo=None):
        if (type(power) == int or type(power) == float) and (type(modulo) == int or type(modulo) == float or modulo == None):
            return Unit( pow(self.value, power, modulo), self.unit_symbol )
        else:
            raise UnitArgumentMismatch


    def __rpow__(self, other):
        raise UnitArgumentMismatch


    def __lshift__(self, other):
        if ( type(other) == int ):
            return Unit( self.value<<other, self.unit_symbol )
        else:
            raise UnitArgumentMismatch

    def __rlshift__(self, other):
        raise UnitArgumentMismatch


    def __rshift__(self, other):
        if ( type(other) == int ):
            return Unit( self.value>>other, self.unit_symbol )
        else:
            raise UnitArgumentMismatch


    def __rrshift__(self, other):
        raise UnitArgumentMismatch


    def __cross__(self, other):
        if type( self.value ) == sp.Matrix and type(other.value) == sp.Matrix:
            value = self.value.cross( other.value )
            unit = self.unit_symbol+"·"+other.unit_symbol
            return Unit( value=value, unit_symbol=unit, derived_unit=True)#, in_si_base_units=self.in_si_base_units+"*"+other.in_si_base_unit )
        else:
            raise VectorExpected

    def cross(self, other):
        return self.__cross__(other)

    def get_si_base_unit(self):
        return self.in_si_base_units


    def set_value(self, value):
        self.value = value


    def get_value(self):
        return self.value


    def unify_unit_symbol( unit_symbol1, unit_symbol2, operator ):
        if operator == "*":
            t=unit_symbol1.replace("^","@")
            if str(t).find("@") != -1:
                q = "("+str(unit_symbol2)+"@)([0-9])"
                r = re.search(q, t)
                if r != None:
                    t=str(r.group(1)) + str(int(r.group(2)) + 1)
                else:
                    t=""
                    for e in unit_symbol1:
                        if e == unit_symbol2:
                            t += e+"^2"
                        else:
                            t += e
            else:
                t = ""
                for e in unit_symbol1:
                    if e == unit_symbol2:
                        t += e+"^2"
                    else:
                        t += e


        if operator == "/":
            s=unit_symbol1.replace("^","@")
            if str(s).find(unit_symbol2+"@") != -1:
                q = "("+str(unit_symbol2)+"@)([0-9])"
                r = re.search(q, s)
                if r != None:
                    if int(r.group(2)) > 2:
                        t=str(r.group(1)) + str(int(r.group(2)) - 1)
                        t=re.sub( r.group(0), t, s)
                    else:
                        t=str(r.group(1)).strip("@2")
                        t=re.sub( r.group(0), t, s)
                else:
                    t=""
                    for e in unit_symbol1:
                        if e != unit_symbol2:
                            t += e

            else:
                t=""
                for e in unit_symbol1:
                    if e != unit_symbol2:
                        t += e

        #clean up string (len>0: unit- / dimensionless quantity
        if len(t)>0:
            if t[0] == "·":
                t = t[1:]

            t=t.replace("·/", "/")
            t=t.replace("·*", "/")
            t=t.replace("@","^")

        return t

# Base units
second              = Unit( value=sp.Symbol("T"),       name = "second",    unit_symbol = "s",      dimension_symbol = "T",     quantity_name = "time",                         derived_unit = False  )
metre               = Unit( value=sp.Symbol("L"),       name = "metre",     unit_symbol = "m",      dimension_symbol = "L",     quantity_name = "distance",                     derived_unit = False  )
kilogram            = Unit( value=sp.Symbol("M"),       name = "kilogram",  unit_symbol = "kg",     dimension_symbol = "M",     quantity_name = "mass",                         derived_unit = False  )
ampere              = Unit( value=sp.Symbol("I"),       name = "ampere",    unit_symbol = "A",      dimension_symbol = "I",     quantity_name = "electric current",             derived_unit = False  )
kelvin              = Unit( value=sp.Symbol("theta"),   name = "kelvin",    unit_symbol = "K",      dimension_symbol = "Q",     quantity_name = "thermodynamic temperature",    derived_unit = False  )
mole                = Unit( value=sp.Symbol("N"),       name = "mole",      unit_symbol = "mol",    dimension_symbol = "N",     quantity_name = "amount of substance",          derived_unit = False  )
candela             = Unit( value=sp.Symbol("J"),       name = "candela",   unit_symbol = "cd",     dimension_symbol = "J",     quantity_name = "luminous intensity",           derived_unit = False  )

# Dimensionless Unit
dimensionless       = Unit( value=sp.Symbol("x"),       name = "Dimensionless Unit", unit_symbol="",dimension_symbol = "",      quantity_name = "Dimensionless Unit",            derived_unit = False )

# Derived Units
newton              = Unit( value=sp.Symbol("x"),       name = "newton",       unit_symbol = "N",         dimension_symbol =  "L*M*T**-2",                quantity_name = "force",                         derived_unit = True, in_si_base_units = "kg·m·s**-2"  )
newton_metre_torque = Unit( value=sp.Symbol("tau"),     name = "newton metre", unit_symbol = "N·m",       dimension_symbol = "M*L**2*T**-2",              quantity_name = "momentOfTorque",                derived_unit = True, in_si_base_units = "m**2·kg·s**−2" )
radian              = Unit( value=sp.Symbol("theta"),   name = "radian",       unit_symbol = "rad",       dimension_symbol = "1",                         quantity_name = "angularDisplacement",           derived_unit = True, in_si_base_units = "m/m" )
pascal              = Unit( value=sp.Symbol("P"),       name = "Pascal",       unit_symbol = "Pa",        dimension_symbol = "M*L**-1*T**-2",             quantity_name = "pressure",                      derived_unit = True, in_si_base_units = "kg/(m·s**2)" )
ohm                 = Unit( value=sp.Symbol("Ohm"),     name= "Ohm",           unit_symbol = "Ohm",       dimension_symbol = "M*L**2*T**-3*I**-2",        quantity_name = "electricResistance",            derived_unit = True, in_si_base_units = "kg*m**2*s**-3*A**-2")
