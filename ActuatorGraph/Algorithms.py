"""
Algorithms concerned with kinematics and mathematical properties of
ActuatorTree structures.
"""

import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor
from ActuatorGraph import Tools

from ActuatorGraph.Actuators.ActuatorTypes import  ActuatorTypes

#import ActuatorGraph.Tools as Tools

import sympy as sp


def op_mul_trans( currPos, v1, v2 ):
    """
    For rotation, we must account for any possible translation, as we may not
    rotate around the x,y or z axis anymore. "currPos" holds the current position,
    i.e. the point in which the rotation is done.
    """
    return v1.multiply( v2 )
    return currPos.inv().multiply( v1.multiply( v2 ) ).multiply( currPos ) 


def getEndPositionFormula( tree ):
    """
    Set operations like "union", "intersection" &c. affect the arithmetic operators, depending on their
    type. Also, here we run into the problem of the categorical asymmetry of "union" - union( Prismatic, 
    Revolution) is different from union( Revolution, Prismatic) - see for example the difference between
    a cable driven parallel robot and a rail based parallel robot. We can avoid this problem however by
    introducing "Anchors", or Anchor points, which not only impose a direction to the actuator tree, but
    also force a specific order. 
    """
    if tree == None:
        return ''
    if tree.token is not "union":
        result =  str( tree.token.getName() )
    else:   
        result = 'u'

    return( result+tree.getEndPositionFormula( tree.left )+ tree.getEndPositionFormula( tree.right ))

# bug in calculation up to endActuator ..
def calculateEndPosition2( tree, endActuator=None ):
    """Returns a vector of the end effector."""
    aop_add = lambda v1, v2: v1+v2
    op_mul = lambda v1, v2: v1.multiply( v2 )
    result = op_mul
    translationPos = sp.Matrix( [ [1,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]] )
    
    if tree.token is not "union":
        result = tree.token.getPositionMatrix()
        return result

    if tree == None:
        return 1

    else:
        translationPos += tree.left.token.getConfigurationMatrix()
        matrix = sp.Matrix( [[1,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]] )
    

        if( tree.left is not None ) and ( tree.right is not None ):
            if ( endActuator != None and tree.left.token == endActuator ):
                return op_mul_trans( translationPos, tree.left.token.getConfigurationMatrix(), matrix)
            elif ( endActuator != None and tree.right.token == endActuator ):
                return op_mul_trans( translationPos, tree.left.token.getConfigurationMatrix(), tree.right.token.getConfigurationMatrix())      # analogous to anchorpoint
            else: 
                if tree.left.type == "Revolute" and tree.right.type != "Anchor":
                    return op_mul_trans(translationPos, tree.calculateEndPosition2( tree.left, endActuator ),tree.calculateEndPosition2( tree.right, endActuator ))
                elif tree.left.type == "Revolute" and tree.right.type == "Anchor":
                    return op_mul_trans(translationPos, tree.left.token.getMatrix(), matrix)
                elif tree.right.type != "Anchor":
                    return op_mul( calculateEndPosition2( tree.left, endActuator ), calculateEndPosition2( tree.right, endActuator ))
                else:
                    return op_mul(tree.left.token.getMatrix(), matrix )


#def __calculateEndPosition__( tree ):
#    actuators = tree.getListOfActuators()
#    I = sp.eye(4)
#    positionMatrix = Tools.matrix_translation( sp.eye(4), actuators[0].getCoordinates() )
#
#    actuators[0].setPositionMatrix( Tools.matrix_translation(I, actuators[0].getCoordinates() ) )
#    configuration = sp.Matrix( [[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]] )
#
#    for i in range( 1, len( actuators ) ):
#        configurationMatrix = actuators[i].getConfigurationMatrix()
#        configuration = configuration.multiply( configurationMatrix )   # new configuration
#        coords = positionMatrix.multiply( configuration )
#        actuators[i].setCoordinates( coords.col(-1)[0:3] )
#
#
#
#        actuators[i].setPositionMatrix = actuators[i].getConfigurationMatrix().multiply( actuators[i-1].getPositionMatrix() )
#
#
#        configuration = configuration.multiply( configurationMatrix )   # new configuration
#
#        positionMatrix = actuators[i].getPositionMatrix()
#
#        actuators[i].setCoordinates( positionMatrix.col(-1)[0:3] )              # set global coordinates
#
#        print( "PositionMatrix:", positionMatrix )
#        print( "ConfigurationMatrix", configurationMatrix )
#        #ConfigurationMatrix Matrix([[1, 0, 0, 1], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
#
#    return actuators[-1].getPositionMatrix()

#def __calculateEndPosition__(tree):
#    actuators = tree.getListOfActuators()
#    position = actuators[0].getPositionMatrix()
#    configuration = sp.Matrix([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
#
#    parent_orientation = sp.eye(4)
#    assert( parent_orientation.shape == (4,4))
#
#    for i in range(1, len(actuators)):
#        configurationMatrix = actuators[i].getConfigurationMatrix()
#        configuration = configuration.multiply(configurationMatrix)  # new configuration
#        print( "configuration", configurationMatrix )
#        coords = configuration.multiply(position)
#        actuators[i].setCoordinates(coords.col(-1)[0:3])
#
#        if actuators[i-1].getType() == ActuatorTypes.Revolute:
#            parent_orientation = parent_orientation.multiply( actuators[i-1].getConfigurationMatrix() )
#            actuators[i].__setParentOrientation__( Tools.matrix_get_rotation( parent_orientation ) )
#        else:
#            actuators[i].__setParentOrientation__( actuators[i-1].parent_orientation )
#            parent_orientation = actuators[i].parent_orientation
#
#    return position.multiply(configuration)
#
#def calculateEndPosition( tree, endActuator=None ):
#    if endActuator != None:
#        listOfActuators = tree.getListOfActuators()
#        p = listOfActuators.index( endActuator )
#        tree = Tools.makeChain(listOfActuators[:p])
#    return __calculateEndPosition__( tree )



def calculateEndPosition( dictionary ):
    """
    Needs chain / serial structure, but may have more than one successor
    """

    if type( dictionary ) != dict:
        return calculateEndPosition( dictionary._dictionary )
    M = list( dictionary.keys())[0].getPositionMatrix()
    parent_orientation = list( dictionary.keys())[0].offsetAngle
    M = parent_orientation.multiply(M)
    assert( parent_orientation.shape == (4,4))
    assert( M != None )
    i=0
    for src, dst in dictionary.items():
        if type(dst)==list:
            for actuator in dst:
                c = actuator.offsetAngle.multiply(actuator.getConfigurationMatrix())
                actuator.setGlobalPositionMatrix( M.multiply(src.getConfigurationMatrix().multiply( c)) )
        else:
            c = dst.offsetAngle.multiply(dst.getConfigurationMatrix())
            dst.setGlobalPositionMatrix( M.multiply(src.getConfigurationMatrix().multiply( c )) )
        assert( src.globalpositionmatrix.shape == (4,4))
        M = src.globalpositionmatrix


def calculateEndPositionForChain( chain ):
    tree = Tools.makeActuatorGraph( chain )
    return calculateEndPosition( tree )


def prime_numbers( n ):
    '''
    Generate n prime numbers
    :param n:
    :return:
    '''
    pos = 0
    current = 1
    while pos < n:
        current += 1
        while True:
            for i in range(2, current // 2 + 1):
                if current % i == 0:
                    current += 1
                    break
            else:
                break
        pos += 1
        yield current


def halton_sequence( d, n ):
    '''
    Generates d-dimensional points w.r.t. to the halton sequence.
    Note: The halton sequence uses primes 2 and 3 as starting point. This might or might not be a good idea ..
    :param d:  Dimension
    :param n: Number
    :return: d- dimensional points
    '''
    pos = 0
    while pos < n:
        p = __halton_sequence_prime__(pos, d)
        pos += 1
        yield tuple(p)


def __halton_sequence__( i, m, b ):
        t = [ i for j in range(m) ]
        b_inv = [ 0 for j in range(m) ]
        r = [ 0 for j in range(m) ]

        for j in range( len(b) ):
            b_inv[j] = 1 / b[j]

        while 0 < sum(t):
            for j in range(m):
                d = (t[j] % b[j])
                r[j] = r[j] + d * b_inv[j]
                b_inv[j] = b_inv[j] / b[j]
                t[j] = int(t[j] / b[j])
        return r


def __halton_sequence_prime__( i, m ):
    b = [ j for j in prime_numbers(m) ]
    assert( len(b) == m )
    return __halton_sequence__( i, m, b )