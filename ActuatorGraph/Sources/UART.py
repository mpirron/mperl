import serial

class UART():
    def __init__( self, name="UART", port='/dev/ttyUSB0', baudrate=9600, timeout=3, numeric_only=True ):
        self.port = port
        self.baudrate = baudrate
        self.connection = serial.Serial( port, baudrate, timeout=timeout)
        self.range = False

    def setRange(self, sensor_min, sensor_max, range_min, range_max, offset):
        self.sensor_max = sensor_max
        self.sensor_min = sensor_min
        self.range_max = range_max
        self.range_min = range_min
        self.offset = offset
        self.range = True

    def mapValue( self, x ):
        return (x-self.sensor_min) * (self.range_max - self.range_min) / (self.sensor_max-self.sensor_min) + self.range_min - self.offset

    def readSensorRaw( self, n=1 ):
        '''
        Returns raw sensor values.
        '''
        if( n==1 ):
            return float( self.connection.readline() )
        else:
            values = []
            for i in range( 0, n ):
                values.append( float( self.connection.readline() ) )
            return values

    def readSensor( self, n=1 ):
        '''
        Maps raw sensor values into a specific range and returns these new values.
        '''
        assert( self.range )
        if( n==1 ):
            return self.mapValue( self.readSensorRaw() )
        else:
            return map( self.mapValue(), self.readSensorRaw(n) )


    def sendDataRaw( self, data ):
        '''
        Send raw data (e.g. to an attached motor)
        :param data:
        :return:
        '''
        self.connection.write( str(data).encode() )

    def sendAngle( self, data ):
        pass
