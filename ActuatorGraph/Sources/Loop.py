from random import uniform
import ActuatorGraph.Sensors.__SensorPrimitive as __SensorPrimitive

class Loop():
   def __init__( self, name="loop", numeric_only=True, range=[10000, 20000] ):
        self.name = name
        self.numeric_only = numeric_only
        self.min = range[0]
        self.max = range[1]
        self.value = None
        self.range = range
        self.setRange( self.min, self.max, self.min, self.max ,0 )

   def setRange(self, sensor_min, sensor_max, range_min, range_max, offset):
        self.sensor_max = self.max
        self.sensor_min = self.min
        self.range_max = range_max
        self.range_min = range_min
        self.offset = offset
        self.range = True

   def mapValue( self, x ):
        return (x-self.sensor_min) * (self.range_max - self.range_min) / (self.sensor_max-self.sensor_min) + self.range_min - self.offset

   def __setValue__( self, value=None ):
        self.value=value

   def readSensorRaw( self, n=1 ):
        if self.value is not None:
            return self.value
        elif( n==1 ):
            value = uniform( self.min, self.max )
            return value
        else:
            values = []
            for i in range( 0, n ):
                values.append( uniform( min, max ) )
            return values

   def readSensor( self, n=1 ):
        '''
        Maps raw sensor values into a specific range and returns these new values.
        '''
        if self.value != None:
            return self.value
        elif( n==1 ):
            assert( self.range )
            return self.mapValue( self.readSensorRaw() )
        else:
            assert( self.range )
            return map( self.mapValue(), self.readSensorRaw(n) )

