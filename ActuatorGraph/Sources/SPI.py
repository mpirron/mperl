import spidev

class SPI():
    def __init__( self, name="SPI0", bus=None, device=None, speed=None, mode=None, init=None, numeric_only=True ):
        self.name = name
        self.bus = bus
        self. device = device
        self.init = init
        self.numeric_only = numeric_only
        self.range = False
        self.speed = speed
        self.mode = mode
        self.connection = self.__connect__()

    def __connect__( self ):
        connection = spidev.SpiDev()
        connection.open( self.bus, self.device )
        connection.max_speed_hz = self.speed
        connection.mode = mode
        return connection

    def setRange(self, sensor_min, sensor_max, range_min, range_max, offset):
        self.sensor_max = sensor_max
        self.sensor_min = sensor_min
        self.range_max = range_max
        self.range_min = range_min
        self.offset = offset
        self.range = True

    def mapValue( self, x ):
        return (x-self.sensor_min) * (self.range_max - self.range_min) / (self.sensor_max-self.sensor_min) + self.range_min - self.offset

    def readSensorRaw( self, n=1 ):
        '''
        Returns raw sensor values.
        '''

        if( n==1 ):
            try:
                rec = self.connection.readbytes(3)
                if rec[0] != 255:
                    value = rec[1]+rec[2]
                    return value
            else:
                return None
        else:
            values = []
            j=0
            for i in range( 0, n ):
                try:
                    rec = self.connection.readbytes(3)
                    if rec[0] != 255:
                        value = rec[1]+rec[2]
                        values.append( value )
                        j += 1
                else:
                    return None
                if j==n:
                    break
            return values

    def readSensor( self, n=1 ):
        '''
        Maps raw sensor values into a specific range and returns these new values.
        '''
        assert( self.range )
        if( n==1 ):
            return self.mapValue( self.readSensorRaw() )
        else:
            return map( self.mapValue(), self.readSensorRaw(n) )


    def sendDataRaw( self, data ):
        '''
        Send raw data (e.g. to an attached motor)
        :param data:
        :return:
        '''
        pass
