import sympy as sp
import ActuatorGraph.Actuators.Revolute as Revolute


from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Actuators.__ActuatorPrimitive import  ActuatorPrimitive
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes
import ActuatorGraph.Units.__si_units as si_units
from ActuatorGraph.Utils import Pidetect

import logging


class Beam( ActuatorPrimitive ):
    def __init__( self, name=NameGenerator.next("Beam"), coordinates = sp.Matrix( [0,0,0] ), orientation='cw', length=1):
        self.length = length
        self.name = name
        self.coordinates = coordinates

        self.invariants = []
        self.preconstraints = []
        self.postconstraints = []
        self.constraints = [ self.invariants, self.preconstraints, self.postconstraints ]


        self.orientation = orientation
        self.init = 0
        self.Rotate = None
        self.type = ActuatorTypes.Prismatic
        self.parent_orientation = sp.eye(4)
        self.actuatorModule = None
        self.endeffector=False

        self.globalpositionmatrix = None

        self.log = logging.getLogger( __name__ )
        self.offsetAngle = sp.eye(4)

        self.unit = si_units.metre
        self.dimension = self.unit.dimension_symbol

    def __str__( self ):
        return "Beam %s length: %s at %s" %( self.name, self.getLength(), self.getCoordinates() )


    def __getSymbols__( self ):
        return ( sp.Symbol( self.name + "_length", real=True) )

    def __setSymbols__( self, length):
        self.setLength( length )


    def __getSubstitution__(self ):
        return ( [(self.__getSymbols__(), self.length)] )


    def __setParentOrientation__( self, orientation ):
        self.parent_orientation = orientation


    def getName( self ):
        return self.name


    def setLength( self, length ):
        l = self.__getSymbols__()
        self.addInvariant( l<=length, l>=length )
        if self.__checkConstraints__( length ):
            self.length = length
            return True
        else:
            return False


    def constraintLength(self, minLen, maxLen ):
        l = self.__getSymbols__()
        self.addInvariant( l>=minLen )
        self.addInvariant( l<=maxLen )


    def getLength( self ):
        return self.length


    def setOrientation( self, orientation ):
        self.orientation = orientation


    def setCoordinates( self, coordinates ):
        self.coordinates = coordinates


    def getCoordinates( self ):
        if self.coordinates != None:
            x,y,z = self.coordinates
        else:
            x,y,z = self.__getSymbols__() 
        #M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        M = sp.Matrix( [x,y,z ] )
        return M


    def getGlobalEndCoordinates( self ):
        return self.getGlobalPositionMatrix()


    def getConfigurationMatrix( self ):
        if self.length == None:
            length = self.__getSymbols__()
        else:
            length = self.length

        M = sp.Matrix( [ [1,0,0,length],[0,1,0,0],[0,0,1,0],[0,0,0,1]] )

        assert( self.parent_orientation.shape == (4,4))
        if self.Rotate!=None:
            M = self.Rotate.getMatrix().multiply( M )
        if self.length != None:
            return M.subs( length, self.length )
        else:
            return M


    # prismatic: bug, because does not take into account the orientation of the parents ..
    # e.g., length+x only true if translation, for add. rot, use x+cos(t)*length and y+sin(t)*length or similar
    def getPositionMatrix( self ):
        length = self.__getSymbols__()
        x,y,z = self.coordinates
        M = sp.Matrix( [ [1,0,0,length+x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        if self.Rotate!=None:
            M = self.Rotate.getMatrix().multiply( M )
        if self.length != None:
            assert( M.shape == (4,4))
            return M.subs( length, self.length )
        else:
            assert( M.shape == (4,4))
            return M
        #return self.positionMatrix


    def setPositionMatrix( self, matrix ):
        self.positionMatrix = matrix


    def getType( self ):
        return self.type


    def rotate( self, thetaX=None, thetaY=None, thetaZ=None ):
        if thetaX != None: self.Rotate = Revolute.Revolute( thetaX=thetaX, thetaY=0, thetaZ=0 )
        elif thetaY != None: self.Rotate = Revolute.Revolute( thetaX=0, thetaY=thetaY, thetaZ=0 )
        elif thetaZ != None: self.Rotate = Revolute.Revolute( thetaX=0, thetaY=0, thetaZ=thetaZ )
            

    def isActuated( self ): 
        return False


    def getInputs( self ):
        return [];


    def getParameters( self ):
        return [];

