from enum import Enum, auto

class ActuatorTypes( Enum ):
    """
    Determines the type of actuators. Prismatic, revolute, spherical and
    cylindrical actuators are considered lower pairs, while higher kinematic
    pairs are compositions thereof. Anchors are somewhat outside of this scheme,
    as they don't translate an impulse into mechanical energy, but they propagate
    the changes in the kinematic to an attached rigid body.
    """
    ActuatorPrimitive = "ActuatorPrimitive"
    Anchor            = "Anchor"
    Prismatic         = "Prismatic"
    Revolute          = "Revolute"
    Spherical         = "Spherical"
    Cylindrical       = "Cylindrical"
    Planar            = "Planar"
    HigherPair        = "HigherPair"
    Virtual           = "Virtual"
    Composition       = "Composition"
    Universal         = "Universal"

    def __str__( self ):
        return self.value
