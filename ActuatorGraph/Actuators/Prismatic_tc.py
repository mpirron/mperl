######################################################################
#                                                                    #
# Implementation of a prismatic actuator with static types           #
#                                                                    #
# Need mypy and mypy_extensions, as well as the stub file            #
# sympy.pyi for types and signatures native only to sympy            #
#                                                                    #
# Check with $ mypy Prismatic_tc.py                                  #
# and execute via $ python3 Prismatic_tc.py                          #
#                                                                    #
# References:                                                        #
#                                                                    #
# * http://mypy-lang.org/                                            #
# * http://mypy.readthedocs.io/                                      #
#                                                                    #
######################################################################

from __future__ import division

from math import pi, sin, cos
from time import time

import sympy as sp
import Revolute

import GeometricAlgorithms
from NameGenerator import NameGenerator

from typing import List, Dict, Any, cast, Any, cast
from mypy_extensions import NoReturn



class Prismatic():
    def __init__( self,
            name=NameGenerator.next("Prismatic"), 
            actuated=False, 
            coordinates =
            sp.Matrix( [0,0,0,0] ), 
            orientation='cw',
            minLen=0, 
            maxLen=1) -> None:

        self.length  = None # type: float
        self.name = name # type: str
        self.minLen = minLen # type: float
        self.maxLen = maxLen # type: float
        self.device = device # type: str
        self.coordinates = coordinates # type: sp.MutableDenseMatrix
        self.orientation = orientation # type: str
        self.actuated = actuated #type: bool
        self.init = 0  # type: int
        self.Rotate= None # type: Revolute.Revolute

    def __str__( self ) -> str:
        return "Prismatic Actuator " + str( self.name ) + "> Current length:" + str( self.length )

    def __checkConstraints__( self, length:float ) -> NoReturn:
        assert( (length < self.maxLen) and (length > self.minLen) )


    def __getSymbols__( self ) -> sp.Symbol:
        return sp.symbols( self.name + ".length")

    def getName( self ) -> str:
        return self.name

    def setLength( self, length:float ) -> NoReturn:
        self.__checkConstraints__( length )
        self.length = length

    def getLength( self ) -> float:
        return self.length

    def setOrientation( self, orientation: str ) -> NoReturn:
        self.orientation = orientation

    def setCoordinates( self, coordinates ) -> NoReturn:
        self.coordinates = coordinates

    def getCoordinates( self ) -> sp.MutableDenseMatrix:
        return self.coordinates


    def getMatrix( self ) -> sp.MutableDenseMatrix:
        length = self.__getSymbols__()
        M = sp.Matrix( [ [1,0,0,length],[0,1,0,0],[0,0,1,0],[0,0,0,1]] )
        if self.Rotate!=None:
            M = self.Rotate.getMatrix().multiply( M )
        if self.length != None:
            return M.subs( length, self.length )
        else:
            return M

    def getType( self ) -> str:
        return "Prismatic"

# Constraint Handling. We distinguish between preconstraints, which affect the
# input of the actuator, and postconstraints, which affect the output of the 
# actuator. 

    def __getConstraints__( self ) -> bool:
        l = self.__getSymbols__()
        constraints = sp.sympify(True)
        if self.maxLen != None: constraints = sp.And(constraints, l <= self.maxLen)
        if self.minLen != None: constraints = sp.And(constraints, l >=self.minLen)
        return constraints

    def getPreConstraints( self ) -> bool:        
        if self.isActuated():
            return self.__getConstraints__()
        else:
            return sp.sympify( True )

    def getPostConstraints( self ) -> bool:
        if self.isActuated():
            return sp.sympify( True )
        else:
            return self.__getConstraints__()

    def getConstraints( self ) -> bool:
        return sp.And(self.getPreConstraints(), self.getPostConstraints())


    def rotate( self, thetaX=None, thetaY=None, thetaZ=None ) -> NoReturn:
        if thetaX != None: self.Rotate = Revolute.Revolute( thetaX=thetaX, thetaY=0, thetaZ=0 )
        elif thetaY != None: self.Rotate = Revolute.Revolute( thetaX=0, thetaY=thetaY, thetaZ=0 )
        elif thetaZ != None: self.Rotate = Revolute.Revolute( thetaX=0, thetaY=0, thetaZ=thetaZ )
            

    def isActuated( self ) -> bool: 
        return self.actuated

    def getInputs( self ) -> List[ sp.Symbol ]:
        if self.isActuated():
            return [self.__getSymbols__()]
        else:
            return [];

    def getParameters( self ) -> List[ sp.Symbol ]:
        if self.isActuated():
            return []
        elif self.length == None:
            return [self.__getSymbols__()]
        else:
            return [];

        
