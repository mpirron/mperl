######################################################################
#                                                                    #
# Implementation of an anchor point.                                 #
# For use in the ActuatorTree model.                                 #
#                                                                    #
#                                                                    #
######################################################################

from __future__ import division

import sympy as sp

from ActuatorGraph.Actuators.__ActuatorPrimitive import  ActuatorPrimitive
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes
from ActuatorGraph.Utils.NameGenerator import NameGenerator
import logging


class Anchor(ActuatorPrimitive, object):
    def __init__( self, name=NameGenerator.next("Anchor"), coordinates=None ):
        self.name = name
        x = sp.symbols( str( self.name ) + "_x", real=True )
        y = sp.symbols( str( self.name ) + "_y", real=True )
        z = sp.symbols( str( self.name ) + "_z", real=True )
        self.coordinates = coordinates

        self.PreConstraints = []
        self.type = ActuatorTypes.Anchor
        self.positionMatrix = sp.eye(4)
        self.parent_orientation = sp.eye(4)
        self.endeffector = False

        self.invariants = []
        self.preconstraints = []
        self.postconstraints = []
        self.constraints = [ self.invariants, self.preconstraints, self.postconstraints ]

        self.globalpositionmatrix = self.getGlobalEndCoordinates()
        self.log = logging.getLogger( __name__ )
        self.mutable = True
        self.offsetAngle=sp.eye(4)

        self.mutableProperties = []

        self.unit = None
        self.dimension = None

        for e in self.__dir__():
            if e.startswith( "set" ):
                self.mutableProperties.append(e)

    def __str__( self ):
        return "Anchor %s at %s" %( self.name, self.coordinates)

    def __getSymbols__( self ):
        x = sp.symbols( str( self.name ) + "_x", real=True )
        y = sp.symbols( str( self.name ) + "_y", real=True )
        z = sp.symbols( str( self.name ) + "_z", real=True )
        return ( x, y, z )

    def __setSymbols__( self, arg ):
        self.coordinates = arg
    
    def __getConstraints__( self ):
        x, y, z = self.__getSymbols__()
        constraints = sp.sympify(True)
        if self.coordinates != None: 
            constraints = sp.And( constraints, sp.Eq( x, self.coordinates.col(-1)[0]) )
            constraints = sp.And( constraints, sp.Eq( y, self.coordinates.col(-1)[1]) )
            constraints = sp.And( constraints, sp.Eq( z, self.coordinates.col(-1)[2]) )
            constraints = sp.And( constraints, self.PreConstraints )
        return constraints.subs(self.__getSubstitution__())

    def __getSubstitution__( self ):
        x, y, z = self.__getSymbols__()
        values = []
        if self.coordinates != None:
            p = self.globalpositionmatrix.col(-1)[0:3]
            self.coordinates = p                            # bug
            values.append( (x, self.coordinates[0] ) )
            values.append( (y, self.coordinates[1] ) )
            values.append( (z, self.coordinates[2] ) )
        return values


    def __setParentOrientation__( self, orientation ):
        self.parent_orientation = orientation

    def getName( self ):
        return self.name

    def setCoordinates( self, coordinates ):
        self.coordinates = coordinates
        self.globalpositionmatrix = self.getGlobalEndCoordinates()

    def getGlobalEndCoordinates( self ):
        return self.parent_orientation*self.getPositionMatrix()

    def getCoordinates( self ):
        if self.coordinates != None:
            #print( self.coordinates)
            x,y,z = self.coordinates
        else:
            x,y,z = self.__getSymbols__()
        #M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        M = sp.Matrix( [x,y,z])
        return M


    def getPositionMatrix( self ):
        if self.coordinates != None:
            x,y,z = self.coordinates
        else:
            x,y,z = self.__getSymbols__()
        M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        assert( M.shape == (4,4))
        return M

    def getConfigurationMatrix( self ):
        M = sp.Matrix( [ [1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]] )
        assert( M.shape == (4,4))
        return M


    def setPositionMatrix( self, matrix ):
        self.positionMatrix = matrix


    def getType( self ):
        return self.type


    def isActuated( self ): 
        return False


    def setMutable(self, mutable ):
        ''' Can the anchor point be moved by other actuators?'''
        self.mutable=mutable
        if mutable == False:
            x,y,z = self.__getSymbols__()
            assert( self.coordinates is not None )
            if self.coordinates is not None:
                px, py, pz = self.coordinates
                self.preconstraints.append( x<=px )
                self.preconstraints.append( y<=py )
                self.preconstraints.append( z<=pz )
                self.preconstraints.append( x>=px )
                self.preconstraints.append( y>=py )
                self.preconstraints.append( z>=pz )
                self.setMutablePropery( 'setCoordinates' )
                self.setMutablePropery( 'setPositionMatrix' )
                self.setMutablePropery( 'setGlobalPositionMatrix' )
                self.setMutablePropery( 'setMatrix' )


