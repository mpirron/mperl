"""
Revolute Actuator, an ActuatorTree primitive. Inherits from
ActuatorPrimitive.
Lies flat in the xy plane, rotation direction is around the z-axis.

Rotate this actuator to get arbitrary rotation, or use the spherical actuator
(with fixed parameters).
"""


from __future__ import division

import sympy as sp

from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Actuators.__ActuatorPrimitive import  ActuatorPrimitive
from ActuatorGraph.Actuators import Spherical
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes
#from ActuatorGraph.Utils import drv8825
import ActuatorGraph.Units.__si_units as si_units
import logging


class Revolute( ActuatorPrimitive ):
    def __init__( self, name=NameGenerator.next("Revolute"), actuated=False, coordinates = sp.Matrix( [0,0,0] ) ):
        self.name = name
        self.actuated = actuated
        self.coordinates = coordinates

        self.type = ActuatorTypes.Revolute
        self.spherical = Spherical.Spherical(name=name, coordinates = coordinates)

        self.minAngle = 0
        self.maxAngle=360

        self.preconstraints = []
        self.postconstraints = []
        self.invariants = []
        self.constraints = [ self.invariants, self.preconstraints, self.postconstraints ]

        self.spherical.setAngle( thetaX=0, thetaY=0 )
        self.angle = self.spherical.getAngle()[2]
        self.listOfAngles = self.spherical.getAngle()

        self.parent_orientation = sp.eye(4)
        self.endeffector=False

        self.globalpositionmatrix = None
        self.log = logging.getLogger( __name__ )

        self.connection = None

        self.offsetAngle = sp.eye(4)
        self.weight = 0
        self.force = 0
        self.torque = 0

        self.unit = si_units.radian
        self.dimension = self.unit.dimension_symbol

    def __str__(self):
        return "Revolute %s angle: %s at %s" %( self.name, self.getAngle(), self.getCoordinates() )
        
    def __getSymbols__( self ):
        return self.spherical.__getSymbols__()[2]
        #return (sp.Symbol(self.name + "_yaw", real=True))

    def __setSymbols__(self, arg):
        self.spherical.__setSymbols__( arg )

    def __getSubstitution__( self ):
        #return self.spherical.__getSubstitution__()
        values = []
        tz = self.__getSymbols__()
        values.append( (tz, self.spherical.getAngle()[2]) )
        return values


    def __setParentOrientation__( self, orientation ):
        self.spherical.__setParentOrientation__( orientation )
        self.parent_orientation = orientation


    def setAngle( self, angle=0 ):
        if self.__checkConstraints__( angle ):
            self.angle = angle
            self.spherical._setAngle( thetaX=0, thetaY=0, thetaZ=angle )
            x = self.__getSymbols__()
            # this might break things ..
            #self.globalpositionmatrix = self.globalpositionmatrix.subs( x, angle )
            return True
        else:
            return False

    def setFixedAngle( self, angle ):
        tz = self.__getSymbols__()
        self.addInvariant( tz<=angle )
        self.addInvariant( tz>=angle )
        self.angle = angle
        self.spherical.setFixedAngle( thetaX=0, thetaY=0, thetaZ=angle )
        self.minAngle = angle
        self.maxAngle = angle

    def getAngle( self ):
        return self.spherical.angle[2]

    def setOrientation( self, orientation ):
        self.spherical.setOrientation( orientation )

    def setCoordinates( self, coordinates ):
        self.spherical.setCoordinates( coordinates )

    def getCoordinates( self ):
        return self.spherical.getCoordinates()

    def getGlobalEndCoordinates( self ):
        return self.spherical.getGlobalEndCoordinates()

    def setGlobalPositionMatrix( self, matrix ):
        self.globalpositionmatrix = matrix
        self.spherical.setGlobalPositionMatrix( matrix )
        
    def getConfigurationMatrix( self ):
        tx, ty, tz = self.spherical.__getSymbols__()
        M = self.spherical.getConfigurationMatrix().subs( [(tx, 0), (ty, 0), (tz, self.getAngle())])
        #print("M=", M)
        #r = self.__getSubstitution__()
        assert( M.shape == (4,4))
        return M.subs( self.__getSubstitution__() )

    def getPositionMatrix( self ):
        #M = self.spherical.getPositionMatrix()
        return self.spherical.getPositionMatrix()



    def setPositionMatrix( self, matrix ):
        self.spherical.setPositionMatrix( matrix )

    def getInputs( self ):
        return self.spherical.getInputs()

    def getParameters( self ):
        return self.spherical.getParameters() 

    def constraintRange( self, lowerBound, upperBound):
        t = self.__getSymbols__()
        self.spherical.minAngleZ = lowerBound
        self.spherical.maxAngleZ = upperBound
        self.addInvariant( t >= lowerBound )
        self.addInvariant( t <= upperBound )
        

    def actuate(self, ):
        pass
