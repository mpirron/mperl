import sympy as sp
import ActuatorGraph.Actuators.Revolute as Revolute


from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Actuators.__ActuatorPrimitive import  ActuatorPrimitive
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes

import ActuatorGraph.Units.__si_units as si_units
from ActuatorGraph.Utils import Pidetect

import logging


class Virtual( ActuatorPrimitive ):
    def __init__( self, name=NameGenerator.next("Virtual"), actuated=False, coordinates = sp.Matrix( [0,0,0] ), orientation='cw', minLen=0, maxLen=1):
        self.length = None
        self.name = name
        self.coordinates = coordinates

        self.invariants = []
        self.preconstraints = []
        self.postconstraints = []
        self.constraints = [ self.invariants, self.preconstraints, self.postconstraints ]

        self.graph = None

        self.orientation = orientation
        self.actuated = actuated
        self.init = 0
        self.Rotate = None
        self.type = ActuatorTypes.Virtual
        self.parent_orientation = sp.eye(4)
        self.actuatorModule = None
        self.endeffector=False

        self.anchors = []
        self.endeffectors = []

        self.globalpositionmatrix = None

        self.log = logging.getLogger( __name__ )
        self.offsetAngle = sp.eye(4)

        self.unit = [ si_units.metre, si_units.radian, si_units.pascal, si_units.ohm ]
        self.dimensions = [ e.dimension_symbol for e in self.unit ]

    def __str__( self ):
        return "Virtual %s at %s" %( self.name,  self.getCoordinates() )

    def __getSymbols__( self ):
        '''Return symbols of actuators'''
        pass


    def __setSymbols__( self, length):
        '''Set symbols of actuators'''
        pass


    def __getSubstitution__(self ):
        pass


    def __setParentOrientation__( self, orientation ):
        self.parent_orientation = orientation


    def getName( self ):
        return self.name


    def setOrientation( self, orientation ):
        self.orientation = orientation



    def setCoordinates( self, coordinates ):
        self.coordinates = coordinates


    def getCoordinates( self ):
        if self.coordinates != None:
            x,y,z = self.coordinates
        else:
            x,y,z = self.__getSymbols__() 
        #M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        M = sp.Matrix( [x,y,z ] )
        return M


    def getGlobalEndCoordinates( self ):
        return self.getGlobalPositionMatrix()


    def getConfigurationMatrix( self ):
        '''Get local configuration matrix from associated graph'''
        from ActuatorGraph.GraphDecomposition import find_all_paths

        paths = sp.flatten( find_all_paths( self.graph, self.graph.anchors[0], self.graph.endeffectors[0]  ) )

        depth = lambda l: isinstance( l, list ) and max( map(depth, l) ) + 1
        print("depth", depth(paths), paths)
        assert( depth(paths) == 1 ) # parallel not yet implemented

        M = sp.eye(4)
        for node in paths:
            M = M.multiply( node.getConfigurationMatrix() )

        return M

    def getPositionMatrix( self ):
        length = self.__getSymbols__()
        x,y,z = self.coordinates
        M = sp.Matrix( [ [1,0,0,length+x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        if self.Rotate!=None:
            M = self.Rotate.getMatrix().multiply( M )
        if self.length != None:
            assert( M.shape == (4,4))
            return M.subs( length, self.length )
        else:
            assert( M.shape == (4,4))
            return M
        #return self.positionMatrix

    def setPositionMatrix( self, matrix ):
        self.positionMatrix = matrix
    

    def rotate( self, thetaX=None, thetaY=None, thetaZ=None ):
        if thetaX != None: self.Rotate = Revolute.Revolute( thetaX=thetaX, thetaY=0, thetaZ=0 )
        elif thetaY != None: self.Rotate = Revolute.Revolute( thetaX=0, thetaY=thetaY, thetaZ=0 )
        elif thetaZ != None: self.Rotate = Revolute.Revolute( thetaX=0, thetaY=0, thetaZ=thetaZ )
            

    def isActuated( self ): 
        return self.actuated

    def getInputs( self ):
        if self.isActuated():
            return [self.__getSymbols__()]
        else:
            return [];

    def getParameters( self ):
        if self.isActuated():
            return []
        elif length == None:
            return [self.__getSymbols__()]
        else:
            return [];


    def setGraph( self, graph ):
        self.graph = graph






        
