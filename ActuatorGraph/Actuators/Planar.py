"""
Planar Actuator.
Per default, the actuator lies in the xy plane and is able to move freely
(within its constraints) in xy-direction and to rotate around the z axis. 
"""

from math import pi, sin, cos
from time import time
import sympy as sp

from ActuatorGraph.Actuators.__ActuatorPrimitive import  ActuatorPrimitive
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes
from ActuatorGraph.NameGenerator import NameGenerator


class Planar(ActuatorPrimitive ):
    def __init__( self, name=NameGenerator.next("Planar"), coordinates=None, angular_orientation=None ):
        self.name = name
        self.coordinates = coordinates
        self.angular_orientation = angular_orientation
        self.PreConstraints = sp.sympify( True )
        self.type = ActuatorTypes.Anchor


    def __getSymbols__( self ):
        x = sp.symbols( str( self.name ) + "_x", real=True )
        y = sp.symbols( str( self.name ) + "_y", real=True )
        z = sp.symbols( str( self.name ) + "_z", real=True )
        tz = sp.symbols( str( self.name ) +"_pitch", real=True )
        return ( x, y, z, tz )
    
    def __getConstraints__( self ):
        x, y, z, tz = self.__getSymbols__()
        constraints = sp.sympify(True)
        if self.coordinates != None: 
            constraints = sp.And( constraints, sp.Eq( x, self.coordinates.col(-1)[0]) )
            constraints = sp.And( constraints, sp.Eq( y, self.coordinates.col(-1)[1]) )
            constraints = sp.And( constraints, sp.Eq( z, self.coordinates.col(-1)[2]) )
            constraints = sp.And( constraints, self.PreConstraints )
        return constraints.subs(self.__getSubstitution__())

    def __getSubstitution__( self ):
        x, y, z, tz = self.__getSymbols__()
        values = []
        if self.coordinates != None:
            values.append( (x, self.coordinates.col(-1)[0] ) )
            values.append( (y, self.coordinates.col(-1)[1] ) )
            values.append( (z, self.coordinates.col(-1)[2] ) )
            values.append( (tz, self.angular_orientation ) )
        return values

    def getMatrix( self ):
        x,y,z,tz = self.__getSymbols__()
        matrix = sp.Matrix( [ [sp.cos(tz), -sp.sin(tz), 0, x ], [sp.sin(tz), sp.cos(tz), 0, y], [0, 0, 1, z], [0,0,0,1] ] )
        return matrix.subs( self.__getSubstitution__() )
    
    def setAngle( self, angle ):
        self.angular_orientation = angle

    def getAngle( self ):
        return self.angular_orientation

    def addPreConstraint( self, constraint ):
        if self.PreConstraints == None:            
            self.PreConstraints = sp.And( sp.sympify(True), constraint )
        else:
            self.PreConstraints = sp.And( self.PreConstraints, constraint )
