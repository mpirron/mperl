"""
Spherical Actuator, an ActuatorTree primitive. Inherits from
ActuatorPrimitive.
"""


from __future__ import division

import sympy as sp

from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Actuators.__ActuatorPrimitive import  ActuatorPrimitive
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes

import ActuatorGraph.Units.__si_units as si_units

from functools import reduce
import logging


class Spherical( ActuatorPrimitive ):
    def __init__( self, name=NameGenerator.next("Spherical"), actuated=False, coordinates = sp.Matrix( [0,0,0] ) ):
        self.name = name
        self.actuated = actuated
        self.coordinates = coordinates

        #self.invariants = [[],[],[]]
        #self.preconstraints = [[],[],[]]
        #self.postconstraints = [[],[],[]]

        self.preconstraints = []
        self.postconstraints = []
        self.invariants = []

        self.constraints = [ self.invariants, self.preconstraints, self.postconstraints ]

        self.thetaX = None 
        self.thetaY = None 
        self.thetaZ = None 
        self.minAngleX = None
        self.minAngleY = None
        self.minAngleZ = None
        self.maxAngleX = None
        self.maxAngleY = None
        self.maxAngleZ = None
        
        self.f = lambda x: (0, x)[ x!= None ]
        self.angle = [ self.thetaX, self.thetaY, self.thetaZ ]
        self.angleValues = [ self.f( angle ) for angle in self.angle ]
        self.type = ActuatorTypes.Spherical
        self.parent_orientation = sp.eye(4)
        self.positionMatrix = sp.eye(4)
        self.endeffector=False


        self.offsetAngle = sp.eye(4)

        self.parameter = []

        self.globalpositionmatrix = None

        self.log = logging.getLogger( __name__ )

        self.unit = si_units.radian
        self.dimension = si_units.dimensionless


    def __checkConstraints__( self, tx, ty, tz ):
        t = self.__getSymbols__()
        for constraint in self.constraints:
            if constraint != []:
                cond = None
                fx = sp.lambdify( t[0], list(filter( lambda i: t[0] in i.atoms(), constraint)))
                fy = sp.lambdify( t[1], list(filter( lambda i: t[1] in i.atoms(), constraint)))
                fz = sp.lambdify( t[2], list(filter( lambda i: t[2] in i.atoms(), constraint)))
                #f = sp.lambdify( t, constraint )

                condx = reduce( lambda x,y: x==y==True, fx(tx))
                condy = reduce( lambda x,y: x==y==True, fx(ty))
                condz = reduce( lambda x,y: x==y==True, fx(tz))
                #cond = reduce( lambda x,y: x==y==True, f(t[0], t[1], t[2]))

                if not condx or not condy or not condz:
                    return False
        return True

    def __getSymbols__( self ):
        tx = sp.symbols( str( self.name ) + "_roll", real=True)
        ty = sp.symbols( str( self.name ) + "_pitch", real=True)
        tz = sp.symbols( str( self.name ) + "_yaw", real=True)
        return (tx, ty, tz)

    def __setSymbols__( self, arg ):
        self.thetaX = arg[0]
        self.thetaY = arg[1]
        self.thetaZ = arg[2]
        self.angle = [ self.thetaX, self.thetaY, self.thetaZ ]
        self.angleValues = [ self.f( angle ) for angle in self.angle ]

    def __getSubstitution__( self ):
        tx, ty, tz = self.__getSymbols__()
        values = []
        if self.thetaX != None: values.append( (tx, sp.N( self.angleValues[0]) ))
        if self.thetaY != None: values.append( (ty, sp.N( self.angleValues[1]) ))
        if self.thetaZ != None: values.append( (tz, sp.N( self.angleValues[2]) ))
        return values

    def __setParentOrientation__( self, orientation ):
        self.parent_orientation = orientation


    def setAngle( self, thetaX=None, thetaY=None, thetaZ=None ):
        if self.__checkConstraints__( thetaX, thetaY, thetaZ ):
            if thetaX != None: self.thetaX = thetaX
            if thetaY != None: self.thetaY = thetaY
            if thetaZ != None: self.thetaZ = thetaZ
            self.angle = [ self.thetaX, self.thetaY, self.thetaZ ]
            self.angleValues = [ self.f( angle ) for angle in self.angle ]
            self.parameter = thetaZ
            return True
        else:
            return False

    def _setAngle( self, thetaX=None, thetaY=None, thetaZ=None ):
        self.thetaX = thetaX
        self.thetaY = thetaY
        self.thetaZ = thetaZ
        self.angle = [ self.thetaX, self.thetaY, self.thetaZ ]
        self.angleValues = [ self.f( angle ) for angle in self.angle ]
        self.parameter = thetaZ
    
    
    def setFixedAngle( self, thetaX=None, thetaY=None, thetaZ=None ):
        tx, ty, tz = self.__getSymbols__()
        if thetaX != None:
            self.thetaX = thetaX
            self.maxAngleX = thetaX
            self.minAngleX = thetaX
            self.invariants[0].append( tx<=thetaX )
            self.invariants[0].append( tx>=thetaX )
        if thetaY != None:
            self.thetaY = thetaY
            self.maxAngleY = thetaY
            self.minAngleY = thetaY
            self.invariants[1].append( ty<=thetaY )
            self.invariants[1].append( ty>=thetaY )
        if thetaZ != None:
            self.thetaZ = thetaZ
            self.minAngleZ = thetaZ
            self.maxAngleZ = thetaZ
            self.invariants[2].append( tz<=thetaZ )
            self.invariants[2].append( tz>=thetaZ )

        self.angle = [ self.thetaX, self.thetaY, self.thetaZ ]
        self.angleValues = [ self.f( angle ) for angle in self.angle ]
        self.parameter = thetaZ

    def getAngle( self ):
        return self.angle
    
    def setOrientation( self, orientation ):
        self.orientation = orientation

    def getConfigurationMatrix( self ):
        tx, ty, tz = self.__getSymbols__()
        Rx = sp.Matrix( [ [1,0,0,0], [0, sp.cos( tx ), -sp.sin( tx ),0], [0, sp.sin( tx ), sp.cos( tx ), 0 ], [ 0,0,0,1] ] )
        Ry = sp.Matrix( [ [sp.cos( ty ), 0, sp.sin( ty ), 0 ], [0,1,0,0], [-sp.sin( ty ), 0, sp.cos( ty ), 0], [0,0,0,1] ] )
        Rz = sp.Matrix( [ [sp.cos( tz ), -sp.sin( tz ), 0, 0 ], [sp.sin( tz ), sp.cos( tz ), 0, 0], [0,0,1,0], [0,0,0,1] ] )
        base = Rx.multiply( Ry ).multiply( Rz )
        S = self.__getSubstitution__()
        M = base.subs( S )
        if len(S) > 2:
            assert(tz == S[2][0])
        assert( M.shape == (4,4))
        assert( base.shape == (4,4))
        return base.subs(self.__getSubstitution__() )


    def getGlobalEndCoordinates( self ):
        if self.coordinates != None:
            x,y,z = self.coordinates
        else:
            x,y,z = self.__getSymbols__()
        M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        #return self.parent_orientation * self.getConfigurationMatrix() * M
        return self.parent_orientation * self.getGlobalPositionMatrix() *M

    def getCoordinates( self ):
        if self.coordinates != None:
            x,y,z = self.coordinates
        else:
            x,y,z = self.__getSymbols__() 
        #M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        M = sp.Matrix( [x,y,z ] )
        return M

    def getPositionMatrix( self ):
        tx, ty, tz = self.__getSymbols__()
        x,y,z = self.coordinates
        Rx = sp.Matrix( [ [1,0,0,x], [0, sp.cos( tx ), -sp.sin( tx ),y], [0, sp.sin( tx ), sp.cos( tx ), z ], [ 0,0,0,1] ] ).subs( tx, self.__getSymbols__()[0] )
        Ry = sp.Matrix( [ [sp.cos( ty ), 0, sp.sin( ty ), 0 ], [0,1,0,0], [-sp.sin( ty ), 0, sp.cos( ty ), 0], [0,0,0,1] ] ).subs( ty, self.__getSymbols__()[1] )
        Rz = sp.Matrix( [ [sp.cos( tz ), -sp.sin( tz ), 0, 0 ], [sp.sin( tz ), sp.cos( tz ), 0, 0], [0,0,1,0], [0,0,0,1] ] ).subs( tz, self.__getSymbols__()[2] )

        base = Rx.multiply( Ry ).multiply( Rz )
        substituted = base.subs(self.__getSubstitution__())
        assert( substituted.shape == (4,4))
        return substituted
        #return self.positionMatrix

    def setPositionMatrix( self, matrix ) :
        self.positionMatrix = matrix


    def getInputs( self ):
        if self.isActuated():
            tx, ty, tz = self.__getSymbols__()
            values = []
            if self.thetaX != None: values.append( tx )
            if self.thetaY != None: values.append( ty )
            if self.thetaZ != None: values.append( tz )
            return values
        else:
            return [];

    def __getParameters__( self ):
        if self.isActuated():
            return [];
        else:
            tx, ty, tz = self.__getSymbols__()
            values = []
            if self.thetaX != None: values.append( tx )
            if self.thetaY != None: values.append( ty )
            if self.thetaZ != None: values.append( tz )
            return values
    
    def constraintXRange( self, lowerBound, upperBound):
        t = self.__getSymbols__()[0]
        self.invariants.append( t >= lowerBound)
        self.invariants.append( t <= upperBound)
        self.minAngleX = lowerBound
        self.maxAngleX = upperBound

    def constraintYRange( self, lowerBound, upperBound):
        t = self.__getSymbols__()[1]
        self.invariants.append( t >= lowerBound)
        self.invariants.append( t <= upperBound)
        self.minAngleY = lowerBound
        self.maxAngleY = upperBound

    def constraintZRange( self, lowerBound, upperBound):
        t = self.__getSymbols__()[2]
        self.invariants.append( t >= lowerBound)
        self.invariants.append( t <= upperBound)
        self.minAngleZ = lowerBound
        self.maxAngleZ = upperBound
    

