"""
Screw Actuator, an ActuatorTree primitive. Inherits from
ActuatorPrimitive.
Lies flat in the xy plane, rotation direction is around the z-axis.
"""

import sympy as sp

from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Actuators.__ActuatorPrimitive import  ActuatorPrimitive
from ActuatorGraph.Actuators import Spherical
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes


class Screw( ActuatorPrimitive ):
    def __init__( self, name=NameGenerator.next("Screw"), actuated=False, coordinates = sp.Matrix( [0,0,0,0] ) ):
        self.name = name
        self.actuated = actuated
        self.coordinates = coordinates
        self.constraints = sp.sympify( True )
        
        self.minAngle = None
        self.maxAngle = None
        self.type = ActuatorTypes.Spherical
        self.spherical = Spherical.Spherical()
        self.spherical.setAngle( thetaX=0, thetaY=0 )
        
        
    def __checkConstraints__( self, angle ):
        return self.spherical.__checkConstraints__( self, thetaZ=angle )

    def __getSymbols__( self ):
        return self.spherical.__getSymbols__()

    def __getSubstitution__( self ):
        return self.spherical.__getSubstitution__()

    def __getConstraints__( self ):
        return self.spherical.__getConstraints__()
    
    def setAngle( self, angle=None ):
        self.spherical.setAngle( thetaZ=angle )
        self.angle = angle

    def getAngle( self ):
        return self.spherical.angle[2]

    def setOrientation( self, orientation ):
        self.spherical.setOrientation( orientation )

    def getMatrix( self ):
        return self.spherical.getMatrix()

    def getInputs( self ):
        return self.spherical.getInputs()

    def getParameters( self ):
        return self.spherical.getParameters() 

    def constraintRange( self, lowerBound, upperBound):
        self.spherical.minAngleZ = lowerBound
        self.spherical.maxAngleZ = upperBound

