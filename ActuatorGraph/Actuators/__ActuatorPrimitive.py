"""
Base class of an ActuatorPrimitive.

"""

from __future__ import division

from math import pi, sin, cos
from time import time
import sympy as sp

from ActuatorGraph.Utils.NameGenerator import NameGenerator

import ActuatorGraph.Actuators.ActuatorTypes as ActuatorTypes

from functools import reduce

import logging
import ActuatorGraph.Units.__si_units as si_units


class ActuatorPrimitive( object ):
    #class Decorators( object ):
    #    @classmethod
    #    def checkMutability( self, f ):
    #        assert( f.__name__ in self.mutableProperties )

    #class checkMutability( object ):
    #    def __init__( self, f ):
    #        self.f = f
    #    def __call__():
    #        assert( self.f.__name__ in self.mutableProperties )


    def __init__( self, name=NameGenerator.next( "ActuatorPrimitive" ), actuated=False, coordinates = sp.Matrix( [0,0,0,0] ) ):
        self.name = name
        self.actuated = actuated
        self.coordinates = coordinates

        self.invariants = []
        self.preconstraints = []
        self.postconstraints = []
        self.constraints = [ self.invariants, self.preconstraints, self.postconstraints ]

        self.weight = None
        self.force = None
        self.torque = None

        self.type = ActuatorTypes.ActuatorPrimitive
        self.parameter = None

        self.globalpositionmatrix = None
        self.log = logging.getLogger( __name__ )
        self.log.info( "Initialized %s Actuator %s" % (self.type, self.name))

        self.unit = si_units.dimensionless
        self.dimension = si_units.dimensionless


    def __getParameters__( self ):
        #if type( self.unit ) == list:
        #    res = [self.name]+self.unit
        #else:
        #    res = [self.name]+[self.unit]
        #return res
        unit = self.unit if type(self.unit) == list else [self.unit]
        name = [ e[0] for e in self.__getSubstitution__()]
        return name+ [e.quantity_name for e in unit if e is not None]

    def __str__( self ):
        return "ActuatorPrimitive %s angle: %s at %s" %( self.name, self.parameter, self.getCoordinates() )

    def __repr__( self ):
        return str(self.type)+":"+str(self.name)+":"+str(self.constraints)

    def __checkConstraints__( self, parameter ):
        l = self.__getSymbols__()
        for constraint in self.constraints:
            if constraint != []:
                f = sp.lambdify( l, constraint )
                cond = reduce( lambda x,y: x==y==True, f(parameter))
                self.log.debug( "%s> Constraints: %s" %( self.name, cond ))
                if not cond:
                    return False
        return True

    def __getSymbols__( self ):
        return "Not yet implemented"

    def __getSubstitution__( self ):
        return None

#    def __getSubstitution__( self ):
#        return "Not yet implemented"
    
    def getName( self ):
        return self.name


    def getGlobalPositionMatrix( self ):
        self.log.debug( "%s> Global Position Matrix" %( self.name ))
        return self.globalpositionmatrix


    def setGlobalPositionMatrix( self, matrix ):
        self.log.debug( "%s> Set Global Position Matrix to %s" %( self.name, matrix ))
        self.globalpositionmatrix = matrix

    def setCoordinates( self, coordinates ):
        self.log.debug( "%s> Set Coordinates to %s" %( self.name, coordinates ))
        self.coordinates = coordinates

    def getCoordinates( self ):
        return self.coordinates

    def setMatrix( self, matrix ):
        self.matrix = matrix

    def getType( self ):
        return self.type

    def getInputs( self ):
        return "Not yet implemented"

    def getParameters( self ):
        return "Not yet implemented"


    def addPreConstraint( self, constraint ):
        self.log.debug( "%s> Added pre constraint: %s" %( self.name, constraint))
        self.preconstraints.append( constraint )

    def getPreConstraints( self ):
        return self.preconstraints

    def addPostConstraint( self, constraint ):
        self.log.debug( "%s> Added post constraint: %s" %( self.name, constraint))
        self.postconstraints.append( constraint )

    def addInvariant( self, constraint ):
        self.log.debug( "%s> Added invariant: %s" %( self.name, constraint))
        self.invariants.append( constraint )

    def getInvariants( self ):
        return self.invariants

    def getPostConstraints( self ):
        return self.postconstraints

    def getConstraints( self ):
        return self.constraints

    def getMutableProperties( self ):
        return self.mutableProperties

    def setMutablePropery( self, property ):
        if self.mutableProperties.index( property ) != -1:
            self.mutableProperties.remove( property )
        else:
            self.mutableProperties.append( property )

    def isActuated( self ):
        #self.log.info( "%s> is actuated: %s" %( self.name, self.isActuated()))
        return self.actuated



