from __future__ import division

import sympy as sp

from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Actuators.__ActuatorPrimitive import  ActuatorPrimitive
from ActuatorGraph.Actuators import Spherical
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes
#from ActuatorGraph.Utils import drv8825
import logging
import ActuatorGraph.Units.__si_units as si_units


class Revolute( ActuatorPrimitive ):
    def __init__( self, name=NameGenerator.next("Revolute"), actuated=False, coordinates = sp.Matrix( [0,0,0] ) ):
        self.name = name
        self.actuated = actuated
        self.coordinates = coordinates

        self.type = ActuatorTypes.Revolute

        self.minAngle = 0
        self.maxAngle=360
        self.angle = None

        self.preconstraints = []
        self.postconstraints = []
        self.invariants = []
        self.constraints = [ self.invariants, self.preconstraints, self.postconstraints ]

        self.parent_orientation = sp.eye(4)

        self.endeffector=False
        self.globalpositionmatrix = None
        self.log = logging.getLogger( __name__ )

        self.connection = None

        self.offsetAngle = sp.eye(4)
        self.weight = 0
        self.torque = 0

        self.unit = si_units.radian
        self.dimension = si_units.dimensionless

    def __str__(self):
        return "Revolute %s angle: %s at %s" %( self.name, self.getAngle(), self.getCoordinates() )
        
    def __getSymbols__( self ):
        return sp.Symbol(self.name + "_yaw", real=True)

    def __setSymbols__(self, arg):
        self.angle = arg

    def __getSubstitution__( self ):
        values = []
        tz = self.__getSymbols__()
        if self.getAngle() != None:
            values.append( (tz, self.getAngle()) )
        else:
            values.append( (tz, tz))
        return values

    def __setParentOrientation__( self, orientation ):
        self.__setParentOrientation__( orientation )
        self.parent_orientation = orientation

    def setAngle( self, angle=0 ):
        self.angle = angle

    def setFixedAngle( self, angle ):
        tz = self.__getSymbols__()
        self.addInvariant( tz<=angle )
        self.addInvariant( tz>=angle )
        self.angle = angle
        self.spherical.setFixedAngle( thetaX=0, thetaY=0, thetaZ=angle )
        self.minAngle = angle
        self.maxAngle = angle

    def getAngle( self ):
        return self.angle

    def setOrientation( self, orientation ):
        self.parent_orientation = orientation

    def setCoordinates( self, coordinates ):
        self.coordinates =  coordinates

    def getCoordinates( self ):
        return self.coordinates

    def getGlobalEndCoordinates( self ):
        if self.coordinates != None:
            x,y,z = self.coordinates
        else:
            x,y,z = self.__getSymbols__()
        M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        return self.parent_orientation * self.getGlobalPositionMatrix() *M

    def setGlobalPositionMatrix( self, matrix ):
        self.globalpositionmatrix = matrix

    def getConfigurationMatrix( self ):
        tz = self.__getSymbols__()
        Rz = sp.Matrix( [ [sp.cos( tz ), -sp.sin( tz ), 0, 0 ], [sp.sin( tz ), sp.cos( tz ), 0, 0], [0,0,1,0], [0,0,0,1] ] )
        M = Rz.subs( self.__getSubstitution__() )
        return M

    def getPositionMatrix( self ):
        return self.globalpositionmatrix

#    def setPositionMatrix( self, matrix ):
#        self.spherical.setPositionMatrix( matrix )

#    def getInputs( self ):
#        return self.spherical.getInputs()

#    def getParameters( self ):
#        return self.spherical.getParameters()

    def constraintRange( self, lowerBound, upperBound):
        t = self.__getSymbols__()
        self.minAngleZ = lowerBound
        self.maxAngleZ = upperBound
        self.addInvariant( t >= lowerBound )
        self.addInvariant( t <= upperBound )

