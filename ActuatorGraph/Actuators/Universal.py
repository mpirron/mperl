'''
Universal joint

Passive, underactuated universal joint
merge bugs!
'''

import sympy as sp
import ActuatorGraph.Actuators.Revolute as Revolute
from ActuatorGraph import Tools
from ActuatorGraph.Actuators import Spherical, Prismatic

from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Actuators.__ActuatorPrimitive import  ActuatorPrimitive
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes

import ActuatorGraph.Units.__si_units as si_units
from ActuatorGraph.Utils import Pidetect

import logging


class Universal( ActuatorPrimitive ):
    def __init__( self, name=NameGenerator.next("Universal"), coordinates = sp.Matrix( [0,0,0] ), length=1):
        self.length = length
        self.name = name
        self.coordinates = coordinates
        self.weight = None

        self.segment_length = 1
        self.number_segments = 6
        self.segment_angle = [ [None,None,None] for i in range(self.number_segments) ]
        self.segment_matrix = [ [] for i in range(self.number_segments) ]
        self.modified = True
        self.local_config = self.__calculate_current_configuration__() if self.modified else self.globalpositionmatrix

        self.invariants = []
        self.preconstraints = []
        self.postconstraints = []
        self.constraints = [ self.invariants, self.preconstraints, self.postconstraints ]

        self.minAngleX = None
        self.minAngleY = None
        self.minAngleZ = None
        self.maxAngleX = None
        self.maxAngleY = None
        self.maxAngleZ = None

        self.orientation = None

        self.init = 0
        self.Rotate = None
        self.type = ActuatorTypes.Universal

        self.parent_orientation = sp.eye(4)
        self.actuatorModule = None

        self.endeffector=False

        self.globalpositionmatrix = None

        self.log = logging.getLogger( __name__ )
        self.offsetAngle = sp.eye(4)

        self.unit = si_units.radian
        self.dimension = self.unit.dimension_symbol

    def __str__( self ):
        return "Universal %s length: %s at %s" %( self.name, self.__getParameters__(), self.getCoordinates() )


    def __getSymbols__( self ):
        res = []
        res.append (sp.symbols( str( self.name )+"_length", real=True ))
        for i in range(self.number_segments):
            res.append(sp.symbols( str( self.name )+str(i) + "_roll", real=True))
            res.append(sp.symbols( str( self.name )+str(i) + "_pitch", real=True))
            res.append(sp.symbols( str( self.name )+str(i) + "_yaw", real=True))
        return res


    def __getSubstitution__(self ):
        res = []
        symbols = self.__getSymbols__()
        res.append( symbols[0] ) if self.segment_length == 0 else self.segment_length
        for i in range( 1, self.number_segments ):
            tmp = []
            for e in range(len(self.segment_angle[i])):
                if (symbols[i+e] == None):
                    tmp.append( symbols[i+e])
                else:
                    tmp.append( self.segment_angle[i][e])
            res.append( tmp )
        return res

    def __str__( self ):
        return "Universal %s length: %s at %s" %( self.name, self.getLength(), self.getCoordinates() )


    def __getSymbols__( self ):
        return (self.spherical1.__getSymbols__()+ self.prismatic.__getSymbols__() + self.spherical2.__getSymbols__() )


    def __getParameters__( self, arg ):
        return self.spherical1.__get


    def __setSymbols__( self, arg ):
        self.thetaX = arg[0]
        self.thetaY = arg[1]
        self.thetaZ = arg[2]
        self.length = arg[3]
        self.angle = [self.thetaX, self.thetaY, self.thetaZ]
        self.setLength( self.length )


    def __getSubstitution__(self ):
        return ( [(self.__getSymbols__(), self.length)] )


    def __setParentOrientation__( self, orientation ):
        self.parent_orientation = orientation


    def __calculate_current_configuration__( self ):
        P = sp.Matrix( [ [1,0,0, self.length], [0,1,0,0], [0,0,1,0],[0,0,0,1]] )
        res = sp.eye(4)


        for angle in self.segment_angle:
            matrix, symbols = Tools.getRotationMatrix( "XYZ" )
            M = matrix.subs( list( zip( symbols, angle)) ).multiply( P )
            res = res.multiply(M)
            #print( "->", sp.N(res))

        self.local_config = res
        self.modified = False
        return self.local_config

    def getName( self ):
        return self.name


    def setLength( self, length ):
        self.segment_length = length


    def setAngles( self, args ):
        assert (len(self.segment_angle) == len(args))
        self.segment_angle = args


    def constraintLength(self, minLen, maxLen ):
        pass

    def constraintXRange( self, lowerBound, upperBound):
        t = self.__getSymbols__()[1]
        self.prismatic.setLenght(length)


    def constraintLength(self, minLen, maxLen ):
        self.prismatic.constraintLength( minLen, maxLen )


    def constraintXRange( self, lowerBound, upperBound):
        t = self.__getSymbols__()[0]
        self.invariants.append( t >= lowerBound)
        self.invariants.append( t <= upperBound)
        self.minAngleX = lowerBound
        self.maxAngleX = upperBound

    def constraintYRange( self, lowerBound, upperBound):
        t = self.__getSymbols__()[1]
        self.invariants.append( t >= lowerBound)
        self.invariants.append( t <= upperBound)
        self.minAngleY = lowerBound
        self.maxAngleY = upperBound

    def constraintZRange( self, lowerBound, upperBound):

        #t = self.__getSymbols__()[1]
        t = self.__getSymbols__()[2]
        self.invariants.append( t >= lowerBound)
        self.invariants.append( t <= upperBound)
        self.minAngleZ = lowerBound
        self.maxAngleZ = upperBound


    def getLength( self ):
        return self.length


    def setOrientation( self, orientation ):
        self.orientation = orientation


    def setCoordinates( self, coordinates ):
        self.coordinates = coordinates


    def getCoordinates( self ):
        if self.coordinates != None:
            x,y,z = self.coordinates
        else:
            x,y,z = self.__getSymbols__() 
        #M = sp.Matrix( [ [1,0,0,x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        M = sp.Matrix( [x,y,z ] )
        return M


    def getGlobalEndCoordinates( self ):
        return self.getGlobalPositionMatrix()


    def getConfigurationMatrix( self ):
        if self.length == None:
            length = self.__getSymbols__()
        else:
            length = self.length

        M = sp.Matrix( [ [1,0,0,length],[0,1,0,0],[0,0,1,0],[0,0,0,1]] )

        assert( self.parent_orientation.shape == (4,4))
        if self.Rotate!=None:
            M = self.Rotate.getMatrix().multiply( M )
        if self.length != None:
            return M.subs( length, self.length )
        else:
            return M


    # prismatic: bug, because does not take into account the orientation of the parents ..
    # e.g., length+x only true if translation, for add. rot, use x+cos(t)*length and y+sin(t)*length or similar
    def getPositionMatrix( self ):
        length = self.__getSymbols__()
        x,y,z = self.coordinates
        M = sp.Matrix( [ [1,0,0,length+x],[0,1,0,y],[0,0,1,z],[0,0,0,1]] )
        if self.Rotate!=None:
            M = self.Rotate.getMatrix().multiply( M )
        if self.length != None:
            assert( M.shape == (4,4))
            return M.subs( length, self.length )
        else:
            assert( M.shape == (4,4))
            return M
        #return self.positionMatrix


    def setPositionMatrix( self, matrix ):
        self.positionMatrix = matrix


    def getType( self ):
        return self.type


    def rotate( self, thetaX=None, thetaY=None, thetaZ=None ):
        if thetaX != None: self.Rotate = Revolute.Revolute( thetaX=thetaX, thetaY=0, thetaZ=0 )
        elif thetaY != None: self.Rotate = Revolute.Revolute( thetaX=0, thetaY=thetaY, thetaZ=0 )
        elif thetaZ != None: self.Rotate = Revolute.Revolute( thetaX=0, thetaY=0, thetaZ=thetaZ )
            

    def isActuated( self ): 
        return False


    def getInputs( self ):
        return [];


    def getParameters( self ):
        return [];

