"""
Implementation of the actuator tree model                          

ActuatorTree are binary trees inducing an hierarchical structure,
which lends itself well to the properties of kinematic chains. To
cater to parallel structures, actuatortrees are arranged in a graph.

The current list of supported actuators, resp. types of actuators are
in ActuatorGraph.Actuators.ActuatorTypes.

The state of actuators are stored in matrix form (position &
orientation).

Edit 05/2018: use dicts, they preserve insertion order:
https://mail.python.org/pipermail/python-dev/2016-September/146327.html
"""


from functools import reduce
from itertools import chain

import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor

import numpy as np
import logging
import sympy as sp

from collections.abc import MutableMapping

class ActuatorTree( MutableMapping ):

    def __init__(self, *args, **kw):
        self._actuators = dict(*args, **kw)
        self.translationPos = sp.Matrix( [ [1,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]] )
        self.constraints = []
        self.dictionary = {}
        # log to the console
        self.debug=True
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(logging.INFO)
        ch = logging.StreamHandler()
        if self.debug:
            ch.setLevel(logging.DEBUG)
        else:
            ch.setLevel(logging.INFO)
        formatter = logging.Formatter('[%(name)s] %(levelname)s: %(message)s')
        ch.setFormatter(formatter)
        self.logger.addHandler(ch)

    # Next five are needed to inherit from MutableMapping

    def __setitem__(self, key, value):
        self._actuators[key] = value

    def __getitem__(self, key):
        try:
            return self._actuators[key]
        except:
            try:
                i = list( self._actuators.keys() )
                return self._actuators[ i[key] ]
            except:
                print("key error")


    def __delitem__(self, key):
        del self._actuators[key]

    def __iter__(self):
        return iter(self._actuators)

    def __len__(self):
        return len(self._actuators)

    def __str__( self ):
        """"""
        return str( self._actuators)

    def __repr__( self ):
        """ """
        return ""

    def __getSubstitutions__( self ):
        subst = []
        for node in self._actuators:
            subst.append( node.__getSubstitution__() )
        return subst



    def __len__( self ):
        """Return the number of nodes/actuators in the tree"""
        return len(self._actuators)

    #def getType( self ):
    #    """Return the compositional type of the node"""
    #    return self.type

    def __getListOfActuators__( self ):
        """Get a list of all actuators which are represented in the tree"""
        k = list(self._actuators.keys())
        v = list(self._actuators.values())
        #return list( set( k+v))
        return list(self._actuators.keys())

    def getListOfActuators( self, actuatorType=None, actuated=None, name=None ):
        """
        Return specific Actuators
        E.g. getListOfActuators( actuatorType="Revolute", actuated=True )
        """
        #l = self.Nodes
        l = self.__getListOfActuators__()
        return [a for a in l if (actuatorType == None or a.getType() == actuatorType) and
                                (actuated == None or a.isActuated() == actuated) and
                                (name == None or a.getName() == name)]



    def getNumberOfActuators( self ):
        """Returns number of Actuators in a tree"""
        return len( self.Nodes )


    def centerOfMass( self, tree ):
        """Calculate the center of mass of an ActuatorTree"""
        pass


# Constraint and constraint management of children and also of the tree itself.

    def __checkConstraints__( self, tree ):
        for actuator in tree.getListOfActuators():
            cond = actuator.__checkConstraints__( actuator.__getParameters__() )
            if cond != True:
                return False
        return True

    def addConstraint( self, constraint ):
        """ """
        self.constraints.append( constraint )


    def removeConstraint( self, tree, constraint ):
        """ """
        if self.constraints.index( constraint ):
            self.constraints.pop( self.constraints.index( constraint ) )
            return True
        else:
            return False

    def getPreConstraints( self ):
        """ """
        constraints = []
        for actuator in self.getListOfActuators():
            constraints.append( actuator.getPreConstraints()) if actuator.getPreConstraints() != None else []
        return constraints

    def getPostConstraints( self ):
        """ """
        constraints = []
        for actuator in self.getListOfActuators():
            constraints.append( actuator.getPostConstraints()) if actuator.getPreConstraints() != None else []
        return constraints

    def getInvariants( self ):
        """ """
        constraints = []
        for actuator in self.getListOfActuators():
            constraints.append( actuator.getInvariants()) if actuator.getPreConstraints() != None else []
        return constraints

    def getConstraints( self ):
        """ """

        return self.constraints+self.getPreConstraints()+self.getPostConstraints()+self.getInvariants()

# State updates / executions

    def getInputParameters( self ):
        d = {}
        for actuator in self.getListOfActuators():
            d[ actuator.getName() ] = actuator.__getSymbols__()
        return d

    def updateState( self, dictOfInputParameters ):
        updated = []
        try:
            for actuator in self.getListOfActuators():
                actuator.__setSymbols__( dictOfInputParameters[ actuator.getName() ] )
                self.logger.debug( "%s: set %s to %s " %( actuator.getName(), actuator.__getSymbols__(), dictOfInputParameters[ actuator.getName() ] ))
                updated.append(actuator)
        except:
            pass
        #print( "Updated:", updated)


    def executeState( self ):
        pass

    def getEndEffector( self ):
        for actuator in self.getListOfActuators():
            if actuator.endeffector:
                return actuator
        return self.getListOfActuators()[-1]

    def setEndEffector( self, actuator ):
        for a in self.getListOfActuators():
            a.endeffector=False
        actuator.endeffector = True


# rotate gears and calculate their angles ..
# 0, ratio

    def __rotateGears__( self, tree ):
        """ """
        aop_add = lambda v1, v2: v1+v2
        op_mul = lambda v1, v2: v1.multiply( v2 )
        op_mul_trans = self.op_mul_trans
        result = op_mul

        
        if tree.token is not "union":
            result = tree.token.getMatrix()
            return result

        if tree == None:
            return 1

        else:   
            matrix = sp.Matrix( [[1,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]] )
            if( tree.left is not None ) and ( tree.right is not None ):
                if tree.left.type == "Gear" and tree.right.type == "Gear":
                    ratio = tree.right.token.getTeeth() / tree.left.token.getTeeth()
                    tree.right.token.setAngle( tree.left.token.getAngle()*ratio)
                    #print("matrix", tree.right.token.getName(), tree.right.token.getMatrix() )
