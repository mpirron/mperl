#  MPERL : Hardware and Software Co-design for Robotic Manipulators

Building small custom robots is getting democratized thanks to affordable tools like 3D printers and microcontrollers.
However, it still requires expertise from a wide range of domains: from designing the mechanical parts to writing the code that controls the robot.
We present MPERL, a tool to help non-experts build custom robotic manipulators.

MPERL starts from an abstract description of the robot's kinematic structure which contains information about joints, actuators, and sensors.
The structure is refined with an easily manufactured geometry.
Furthermore, from the structure we also generate control code which can be used to move the robot to a target configuration.
We evaluate MPERL on a range of common robotic manipulator architectures, both serial and parallel.


![MPERL Example](mperl.mp4)

## Getting Started


These instructions will get you a copy of the project up and running on your local machine. 

### Prerequisites

To run the MPERL, you'll need Python3 (tested with Python 3.6.7, but
3.4 should also work), along with the python packages scipy
and sympy. In addition, you may want to have a working dreal4
installation in your path (tested with dreal v4.18.11.4).
You can find dreal at https://github.com/dreal/dreal4

### Installing

Clone the repository

```
$ git clone https://gitlab.mpi-sws.org/mpirron/mperl
```


## Running the tests

There are some examples provided in Examples/; to run them:

```
$ cd Examples/
$ python -m unittest test_*
```

The tilted_scara.py example can be run via

```
$ cd Examples/
$ python tilted_scara.py
```

For this example, the source of the sensor is set to an "artificial"
one, in order to run the test without actual hardware.

## Getting to know the components: A short tutorial

The examples are a good starting point to show you how to design
serial and parallel mechanisms. In the following, we'll guide you
through a minimal example to get you up and running:


Start by importing the primitive components like sensors or joints:

```
import ActuatorGraph.Actuators.Anchor as Anchor
import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
```

For inverse kinematic, you'll also want to include a solver:

```
import ActuatorGraph.SMT as SMT
```

And we also need to include some Tools for initializing the system:

```
import ActuatorGraph.ActuatorGraph as ActuatorGraph
import ActuatorGraph.Tools as Tools
import sympy 
```

We'll use dReal in this example, but we also provide a Least Square
Optimization and Cyclic Coordinate Descent.

Next, we'll put together the robotic system. We set the origin of the
system to origin of ordinates, and actuate the revolute joint and the
prismatic joint:

```
A0 = Anchor.Anchor( name="Anchor0", coordinates= [0,0,0] )
P0 = Prismatic.Prismatic( name="Prismatic0", actuated=True, minLen=0, maxLen=1 )
R0 = Revolute.Revolute( name="Revolute0", actuated=True )
```

And then we compose the system "pointer":

```
structure = { A0: [R0], R0: [P0] }
pointer = ActuatorGraph.ActuatorGraph( name="pointer" )
pointer.loadDictionary( structure )
pointer.setAnchors(A0)
pointer.setEndeffectors(P0)
pointer.partitionLinkages()
```

We can then initialize the system:

```
Tools.initChain( pointer )
```

Now, we are done and can work with the system, e.g. for parameter
synthesizing. We can also impose additional constraints, e.g. a  minimum
and maximum angular value:

```
R0.constraintRange( -sympy.pi, sympy.pi)
target = sympy.Matrix( [ sympy.sqrt(2)/2, sympy.sqrt(2)/2, 0 ] )
res = SMT.solveInverseKinematics( pointer, target, tol=0.001, solver="dreal")
for x,v in res.items():
    print(x, " = ", v)
```




## Authors
* Marcus Pirron 
* Damien Zufferey


