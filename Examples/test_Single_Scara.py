import unittest

import sys
sys.path.insert(0, '../')

import math
import sympy as sp

import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor
import ActuatorGraph.Sensors.Flex as Flex


import ActuatorGraph.Algorithms as Algorithms
import ActuatorGraph.Tools as Tools
import ActuatorGraph.SMT as SMT

class test_dreal( unittest.TestCase ):

    def setUp( self ):
        self.A0 = Anchor.Anchor( name="Anchor0", coordinates= sp.Matrix([0,0,0]) )
        self.A1 = Anchor.Anchor( name="Anchor1", coordinates= sp.Matrix([0,0,0]) )
        self.P0 = Prismatic.Prismatic( name="Prismatic0")
        self.P1 = Prismatic.Prismatic( name="Prismatic1")

        self.R0 = Revolute.Revolute( name="Revolute0" )
        self.R1 = Revolute.Revolute( name="Revolute1" )
        self.R2 = Revolute.Revolute( name="Revolute2" )


        self._P0 = Prismatic.Prismatic( name="P0_sensor" )
        self._P1 = Prismatic.Prismatic( name="P1_sensor" )
        self._P0.setLength(1)
        self._P1.setLength(1)
        self._F0 = Flex.Flex( name="Flex0_sensor" ) # default: loop device
        self.S0 = Tools.makeActuatorGraph( [self._P0, self._F0, self._P1] )
        self.S0.partitionLinkages()

        self.debug = True  # Print to stdout

        self.chain = Tools.makeActuatorGraph([self.A0, self.R0, self.P0, self.R1, self.P1, self.R2, self.A1])
        self.chain.partitionLinkages()
        Tools.initChain( self.chain )

        #self.chain_sensor = Tools.makeActuatorGraph([self.A0, self.R0, self.S0.getListOfActuators(), self.R1, self.P1, self.R2, self.A1])
        #self.chain_sensor.partitionLinkages()
        #Tools.initChain( self.chain_sensor )


    def test_00_forward_kinematics( self ):
        self.R0.setAngle( sp.pi/4 )
        self.R1.setAngle( sp.pi/4 )
        self.R2.setAngle( sp.pi/4 )
        self.P0.setLength(1)
        self.P1.setLength(1)
        Algorithms.calculateEndPosition( self.chain )
        if self.debug:
            print( self.chain.endeffectors[0].globalpositionmatrix )


    def test_01_with_constraints( self ):
        self.R0.constraintRange( -math.pi, math.pi)
        self.R1.constraintRange( -math.pi, math.pi)
        self.R2.constraintRange( -math.pi, math.pi)
        self.P0.setLength(1)
        self.P1.setLength(1)
        target = sp.Matrix( [ math.sqrt(2)/2, math.sqrt(2)/2, 0 ] )
        res = SMT.solveInverseKinematics( self.chain, target, tol=0.001, solver="dreal")
        if self.debug:
            for x,v in res.items():
                print(x, " = ", v)


    def test_adding_sensor_flatten( self ):
        """Time to add component (sensor) and init new chain"""
        self.chain_sensor = Tools.makeActuatorGraph(sp.flatten([self.A0, self.R0, self.S0.getListOfActuators(), self.R1, self.P1, self.R2, self.A1]))
        self.chain_sensor.partitionLinkages()
        self.R0.constraintRange( -math.pi/2, math.pi/2)
        self.R1.constraintRange( -math.pi/2, math.pi/2)
        self.R2.constraintRange( -math.pi/2, math.pi/2)
        self.P0.setLength(1)
        self.P1.setLength(1)
        Tools.initChain( self.chain_sensor )


    def test_adding_sensor( self ):
        """Time to add component (sensor) and init new chain"""
        self.chain_sensor = Tools.makeActuatorGraph([self.A0, self.R0, self._P0, self._F0, self._P1, self.R1, self.P1, self.R2, self.A1])
        self.chain_sensor.partitionLinkages()
        self.R0.constraintRange( -math.pi/2, math.pi/2)
        self.R1.constraintRange( -math.pi/2, math.pi/2)
        self.R2.constraintRange( -math.pi/2, math.pi/2)
        self.P0.setLength(1)
        self.P1.setLength(1)
        Tools.initChain( self.chain_sensor )


    def test_adding_new_component( self ):
        """Time to add component (sensor) and init new chain"""
        self.chain_sensor = Tools.makeActuatorGraph([self.A0, self.R0, self._P0, self._P1, self.R1, self.P1, self.R2, self.A1])
        self.chain_sensor.partitionLinkages()
        Tools.initChain( self.chain_sensor )


    def test_example_parameter_synthesis( self ):
        """Synthesize free parameters: length"""
        #self.R0.setAngle(sp.pi/4);
        #self.R1.setAngle(0)
        #self.R2.setAngle(0)
        self.R0.constraintRange( -math.pi, math.pi)
        self.R1.constraintRange( -math.pi, math.pi)
        self.R2.constraintRange( -math.pi, math.pi)
        self.P0.constraintLength( 1, 1 )
        self.P1.constraintLength( 1, 1 )
        target = sp.Matrix( [ math.sqrt(2)/2, math.sqrt(2)/2, 0 ] )
        res = SMT.solveInverseKinematics( self.chain, target, tol=0.001, solver="dreal")
        if self.debug:
            for x,v in res.items():
                print(x, " = ", v)
                self.assertTrue( abs(v) < 1e10 )


    def test_example_tilted_scara( self) :
        """90° tilted scara arm with sensor integration"""
        self.chain_sensor = Tools.makeActuatorGraph(sp.flatten([self.A0, self.R0, self.S0.getListOfActuators(), self.R1, self.P1, self.R2, self.A1]))
        self.chain_sensor.partitionLinkages()
        self.R0.constraintRange( -math.pi/2, math.pi/2)
        self.R1.constraintRange( -math.pi/2, math.pi/2)
        self.R2.constraintRange( -math.pi/2, math.pi/2)
        self.P0.setLength(1)
        self.P1.setLength(1)
        Tools.initChain( self.chain_sensor )

        self._F0.connection.__setValue__(1) # set some arbitrary "sensor value"
        target = sp.Matrix( [ math.sqrt(2), math.sqrt(2), 0 ] )
        res = SMT.solveInverseKinematics( self.chain_sensor, target, tol=0.001, solver="dreal")
        if self.debug:
            for x,v in res.items():
                print(x, " = ", v)
                self.assertTrue( abs(v) < 1e10 )



if __name__ == "__main__":
    unittest.main()
