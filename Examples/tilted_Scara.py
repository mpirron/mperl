import unittest

import sys
sys.path.insert(0, '../')

import sympy as sp

from time import sleep

import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Anchor as Anchor
import ActuatorGraph.Sensors.Flex as Flex


import ActuatorGraph.Algorithms as Algorithms
import ActuatorGraph.Tools as Tools
import ActuatorGraph.SMT as SMT
import ActuatorGraph.LeastSquare as LS

import ActuatorGraph.Sources.UART as UART

from time import time, sleep


# replace sleep with something more adequate. does not catch all out-of-time problems
def every( time_in_s=0.1 ):
    """
    Decorator to execute a function every n seconds.
    :param time_in_s:
    :return:
    """
    def every_decorator(func=None):
        def func_wrapper(wrapper=None):
            starttime = time()
            while True:
                func(wrapper)
                t = time_in_s - ((time() - starttime) % time_in_s)
                #print(t, time_in_s)
                if t > time_in_s:
                    raise StopAsyncIteration
                else:
                    sleep(time_in_s - ((time() - starttime) % time_in_s))
            return wrapper

        return func_wrapper
    return every_decorator



def setup( listOfActuators ):
    A0 = Anchor.Anchor( name="Anchor0", coordinates= sp.Matrix([0,0,0]) )
    A1 = Anchor.Anchor( name="Anchor1", coordinates= sp.Matrix([0,0,0]) )
    P0 = Prismatic.Prismatic( name="Prismatic0")
    P1 = Prismatic.Prismatic( name="Prismatic1")

    R0 = listOfActuators[0]
    R1 = listOfActuators[1]
    R2 = listOfActuators[2]


    _P0 = Prismatic.Prismatic( name="P0_sensor" )
    _P1 = Prismatic.Prismatic( name="P1_sensor" )
    _P0.setLength(0.5)
    _P1.setLength(0.5)
    _F0 = Flex.Flex( name="Flex0_sensor" ) # default: loop device
    #_F0.connection.__setValue__(1)  # set fixed value
    #F0.connection = UART.UART(port="/dev/ttyACM1")
    #_F0.connection.setRange(3400, 4000, -1, 1, 0) #wheatstone bridge returns 3400-4000 for +/- 1°
    #R0.connection = UART.UART(port="/dev/ttyUSB1")


    S0 = Tools.makeActuatorGraph( [_P0, _F0, _P1] )
    S0.partitionLinkages()

    R0.constraintRange( -sp.pi/2, sp.pi/2)
    R1.constraintRange( -sp.pi/2, sp.pi/2)
    R2.constraintRange( -sp.pi/2, sp.pi/2)
    P0.setLength(1)
    P1.setLength(1)

    chain_sensor = Tools.makeActuatorGraph(sp.flatten([A0, R0, S0.getListOfActuators(), R1, P1, R2, A1]))
    chain_sensor.setAnchors(A0)
    chain_sensor.setEndeffectors(R2)
    chain_sensor.partitionLinkages()
    Tools.initChain( chain_sensor )

    return chain_sensor


@every(1)
def main( chain_sensor ):
    res = SMT.solveInverseKinematics( chain_sensor, [1+sp.sqrt(2)/2, sp.sqrt(2)/2, 0], tol=0.001, solver="dreal")
    print(res)


if __name__ == "__main__":
    R0 = Revolute.Revolute( name="Revolute0" )
    R1 = Revolute.Revolute( name="Revolute1" )
    R2 = Revolute.Revolute( name="Revolute2" )


    chain_sensor = setup( [R0, R1, R2] )

    main( chain_sensor )