#!/usr/bin/env python3

"""
ActuatorGraph Representation of a dual scara mechanism
"""


import sys
sys.path.insert(0, '../')

import sympy as sp

import ActuatorGraph.Actuators.Revolute as Revolute
import ActuatorGraph.Actuators.Prismatic as Prismatic
import ActuatorGraph.Actuators.Spherical as Spherical
import ActuatorGraph.Actuators.Anchor as Anchor

import ActuatorGraph.Utils.Evaluation as Evaluation
import unittest

import ActuatorGraph.Algorithms as Algorithms
import ActuatorGraph.GeometricAlgorithms as GeometricAlgorithms
import ActuatorGraph.CCD as CCD
import ActuatorGraph.SMT as SMT
import ActuatorGraph.LeastSquare as LS

from ActuatorGraph.Utils.NameGenerator import NameGenerator
from ActuatorGraph.Actuators.ActuatorTypes import ActuatorTypes

import ActuatorGraph.Semantics as Semantics
import ActuatorGraph.Tools as Tools
import ActuatorGraph.ActuatorGraph as ActuatorGraph

import sympy as sp


class test_scara_arm( unittest.TestCase ):
    """Dual scara arm"""
    def setUp( self ):
        """Set up the robot"""
        self.A00 = Anchor.Anchor( name="Anchor0", coordinates= [0,0,0] )
        self.P00 = Prismatic.Prismatic( name="Prismatic0_left", actuated=False )
        self.P10 = Prismatic.Prismatic( name="Prismatic1_left", actuated=False )
        self.R00 = Revolute.Revolute( name="Revolute0_left", actuated=True )
        self.R10 = Revolute.Revolute( name="Revolute1_left", actuated=False )
        self.R20 = Revolute.Revolute( name="Revolute2_left", actuated=False )

        self.A01 = Anchor.Anchor( name="Anchor1", coordinates= [2,0,0] )
        self.P01 = Prismatic.Prismatic( name="Prismatic0_right", actuated=False )
        self.P11 = Prismatic.Prismatic( name="Prismatic1_right", actuated=False )
        self.R01 = Revolute.Revolute( name="Revolute0_right", actuated=True )
        self.R11 = Revolute.Revolute( name="Revolute1_right", actuated=True )
        self.R21 = Revolute.Revolute( name="Revolute2_right", actuated=False )

        self.P00.setLength(1)
        self.P10.setLength(1)
        self.P01.setLength(1)
        self.P11.setLength(1)

        self.R00.constraintRange( -sp.pi, sp.pi)
        self.R01.constraintRange( -sp.pi, sp.pi)
        self.R11.constraintRange( -sp.pi, sp.pi)
        self.R10.constraintRange( -sp.pi, sp.pi)
        self.R20.constraintRange( -sp.pi, sp.pi)
        self.R21.constraintRange( -sp.pi, sp.pi)


        self.structure = { self.A00: [self.R00],
                           self.R00: [self.P00],
                           self.P00: [self.R10],
                           self.R10: [self.P10],
                           self.P10: [self.R20],
                           self.R20: [self.P11],
                           self.P11: [self.R11],
                           self.R11: [self.P01],
                           self.P01: [self.R01],
                           self.R01: [self.A01]
                           }


        self.scaraArm = ActuatorGraph.ActuatorGraph( name="scaraArm" )
        self.scaraArm.loadDictionary( self.structure )
        self.scaraArm.setAnchors(self.A00)
        self.scaraArm.setEndeffectors(self.A01)
        self.scaraArm.partitionLinkages()

        for linkage in self.scaraArm.linkages:
            Tools.initChain( linkage )

        self.debug = True



    def testForwardKinematics( self ):
        """Test forward kinematic"""

        self.R00.setAngle( sp.pi/2 )
        self.R01.setAngle( -sp.pi/2)
        target = sp.Matrix( [sp.sqrt(2)/2,sp.sqrt(2)/2,0 ])

        res = SMT.solveInverseKinematics( self.scaraArm, target, 0.0001, "dreal") # arbitrary structures
        for x,v in res.items():
            print(x, " = ", v)


    def testInverseKinematicsDreal( self ):
        """Test inverse kinematic"""
        target = sp.Matrix( [2, 0, 0] )
        res = SMT.solveInverseKinematics( self.scaraArm, target, 0.0001, "dreal") # arbitrary structures
        for x,v in res.items():
            print(x, " = ", v)


    def testInverseKinematicsLS(self):
        target = sp.Matrix( [sp.sqrt(2)/2, sp.sqrt(2)/2, 0] )
        res3 = LS.solveChain( self.scaraArm, target, 0.0001) # currently only for chains
        for x,v in res3:
            print(x, " = ", v)


if __name__ == "__main__":
    unittest.main()
